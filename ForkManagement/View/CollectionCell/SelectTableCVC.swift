//
//  TableCVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 18/11/22.
//

import UIKit

class SelectTableCVC: UICollectionViewCell {
    
    @IBOutlet weak var lblTbNo: VCLanguageLabel!
    @IBOutlet weak var lblTable: VCLanguageLabel!
    @IBOutlet weak var lblNoPerson: UILabel!
    @IBOutlet weak var lblType: VCLanguageLabel!
    @IBOutlet weak var badgeImage: UIImageView!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.rounded(cornerRadius: 5)
        badgeImage.rounded(cornerRadius: 5)
        bgView.setBorder(borderColor: UIColor(named: "darkGrey") ?? .lightGray ,borderWidth: 0.2 )
    }
}
