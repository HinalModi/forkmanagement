//
//  FoodTypeCVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 23/11/22.
//

import UIKit

class FoodTypeCVC: UICollectionViewCell {
    
    @IBOutlet weak var dashImg: UIImageView!
    @IBOutlet weak var lblType: UILabel!
    var selCat : (()->())?
    var val = 0
    
    @IBAction func btnSel(_ sender: UIButton) {
        selCat?()
    }
    
}
