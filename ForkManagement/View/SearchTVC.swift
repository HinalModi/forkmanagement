//
//  SearchTVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 27/12/22.
//

import UIKit

class SearchTVC: UITableViewCell {
    @IBOutlet weak var lblRating: VCLanguageLabel!
    @IBOutlet weak var lblDist: VCLanguageLabel!
    @IBOutlet weak var lblWalkInPickTime: VCLanguageLabel!
    @IBOutlet weak var lblTime: VCLanguageLabel!
    @IBOutlet weak var restName: VCLanguageLabel!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.setBorder(borderColor: UIColor(named: "darkGrey") ?? .lightGray ,borderWidth: 0.2 )
        bgView.rounded(cornerRadius: 5)
        bgView.setShadow(shadowColor: .darkGray, shadowOpacity: 0.2, shadowRadius: 2, shadowOffset: CGSize(width: 0, height: 2))
        productImg.rounded(cornerRadius: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
