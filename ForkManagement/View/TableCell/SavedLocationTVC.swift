//
//  SavedLocationTVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 29/11/22.
//

import UIKit

class SavedLocationTVC: UITableViewCell {

    @IBOutlet weak var imgDone: UIImageView!
    @IBOutlet weak var btnMakeDefault: VCLanguageButton!
    @IBOutlet weak var lblRestName: VCLanguageLabel!
    @IBOutlet weak var lblType: VCLanguageLabel!
    @IBOutlet weak var bgView: UIView!
    var makeDefault : (()->())?
    var deleteLoc : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.rounded(cornerRadius: 10)
        bgView.setBorder(borderColor: UIColor(named: "borderGray")!, borderWidth: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    @IBAction func btnDelLoc(_ sender: UIButton) {
        deleteLoc?()
    }
    @IBAction func btnMakeDefault(_ sender: UIButton) {
        makeDefault?()
    }
}
