//
//  SelectTabFoodInCartTVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 24/11/22.
//

import UIKit

class SelectTabFoodInCartTVC: UITableViewCell {
    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblFoodToppings: VCLanguageLabel!
    @IBOutlet weak var txtQty: VCLanguageTextField!
    @IBOutlet weak var lblFoodPrice: VCLanguageLabel!
    @IBOutlet weak var lblFoodTypes: VCLanguageLabel!
    @IBOutlet weak var lblfoodName: VCLanguageLabel!
    var incrVal : (()->())?
    var decrVal : (()->())?
    var val = 1
    var delRow : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.setBorder(borderColor: UIColor(named: "darkGrey") ?? .lightGray ,borderWidth: 0.2 )
        bgView.rounded(cornerRadius: 5)
        foodImage.rounded(cornerRadius: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnDel(_ sender: UIButton) {
        delRow?()
    }
    @IBAction func btnDecr(_ sender: UIButton) {
        if txtQty.text != "1"{
            val -= 1
            txtQty.text = "\(val)"
            decrVal?()
        }
    }
    @IBAction func btnIncr(_ sender: VCLanguageButton) {
        val += 1
        txtQty.text = "\(val)"
        incrVal?()
    }
}
