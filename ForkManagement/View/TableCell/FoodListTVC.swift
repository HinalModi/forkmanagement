//
//  FoodListTVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 23/11/22.
//

import UIKit

class FoodListTVC: UITableViewCell {

    @IBOutlet weak var lblPrice: VCLanguageLabel!
    @IBOutlet weak var lblItemName: VCLanguageLabel!
    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.setBorder(borderColor: UIColor(named: "darkGrey") ?? .lightGray ,borderWidth: 0.2 )
        bgView.rounded(cornerRadius: 5)
        foodImage.rounded(cornerRadius: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
