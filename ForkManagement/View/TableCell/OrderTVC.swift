//
//  OrderTVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 28/11/22.
//

import UIKit
import HCSStarRatingView

class OrderTVC: UITableViewCell {
    
    @IBOutlet weak var lblRemHour: UILabel!
    @IBOutlet weak var lblRemMin: UILabel!
    @IBOutlet weak var expRateView: HCSStarRatingView!
    @IBOutlet weak var waiterRateView: HCSStarRatingView!
    @IBOutlet weak var foodRateView: HCSStarRatingView!
    @IBOutlet weak var lblHotelName: VCLanguageLabel!
    @IBOutlet weak var lblNoOfPersons: VCLanguageLabel!
    @IBOutlet weak var lblWalkInTime: VCLanguageLabel!
    @IBOutlet weak var lblOrderId: VCLanguageLabel!
    @IBOutlet weak var lblPrice: VCLanguageLabel!
    @IBOutlet weak var lblOpenTill: VCLanguageLabel!
    @IBOutlet weak var detailBgView: UIView!

    var doRate : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        detailBgView.setBorder(borderColor: UIColor(named: "borderGray")!, borderWidth: 0.5)
        detailBgView.rounded(cornerRadius: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    @IBAction func btnOpenRate(_ sender: UIButton) {
        doRate?()
    }
}
