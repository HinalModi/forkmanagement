//
//  WalletTVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 28/11/22.
//

import UIKit

class WalletTVC: UITableViewCell {

    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.rounded(cornerRadius: 10)
        timeView.roundedCorners(corners: [.topRight , .bottomRight], radius: 10)
        bgView.setBorder(borderColor: UIColor(named: "borderGray")!, borderWidth: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
