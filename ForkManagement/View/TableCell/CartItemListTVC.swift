//
//  CartItemListTVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 22/11/22.
//

import UIKit

class CartItemListTVC: UITableViewCell {
    
    @IBOutlet weak var lblTopPrice: VCLanguageLabel!
    @IBOutlet weak var btnDec: UIButton!
    @IBOutlet weak var btnInc: UIButton!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var lblFoodToppings: VCLanguageLabel!
    @IBOutlet weak var txtQty: VCLanguageTextField!
    @IBOutlet weak var lblFoodPrice: VCLanguageLabel!
    @IBOutlet weak var lblFoodTypes: VCLanguageLabel!
    @IBOutlet weak var lblfoodName: VCLanguageLabel!
    var incrVal : (()->())?
    var decrVal : (()->())?
    var val = 1
    var delRow : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.setBorder(borderColor: UIColor(named: "darkGrey") ?? .lightGray ,borderWidth: 0.2 )
        bgView.rounded(cornerRadius: 5)
        productImg.rounded(cornerRadius: 5)
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            btnDec.roundedCorners(corners: [.topLeft,.bottomLeft], radius: 5)
            btnInc.roundedCorners(corners: [.topRight,.bottomRight], radius: 5)
        }else{
            btnDec.roundedCorners(corners: [.topRight,.bottomRight], radius: 5)
            btnInc.roundedCorners(corners: [.topLeft,.bottomLeft], radius: 5)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func btnDel(_ sender: UIButton) {
        delRow?()
    }
    @IBAction func btndecrement(_ sender: UIButton) {
        if val > 1{
            val -= 1
            txtQty.text = "\(val)"
            decrVal?()
        }
    }
   
    @IBAction func btnIncrement(_ sender: UIButton) {
         val += 1
         txtQty.text = "\(val)"
         incrVal?()         
    }
}
