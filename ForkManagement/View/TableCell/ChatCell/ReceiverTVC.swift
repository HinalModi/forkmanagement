//
//  ReceiverTVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 01/12/22.
//

import UIKit

class ReceiverTVC: UITableViewCell {

    @IBOutlet weak var lblMsgRec: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var recImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        chatView.rounded(cornerRadius: 15)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
