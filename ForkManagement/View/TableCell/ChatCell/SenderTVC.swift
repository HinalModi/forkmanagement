//
//  SenderTVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 01/12/22.
//

import UIKit

class SenderTVC: UITableViewCell {

    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var lblMsgSent: UILabel!
    @IBOutlet weak var sentTime: UILabel!
    @IBOutlet weak var sendImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        chatView.rounded(cornerRadius: 15)
        lblMsgSent.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
