//
//  ToppingTVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 29/12/22.
//

import UIKit

class ToppingTVC: UITableViewCell {

    @IBOutlet weak var btnSel: UIButton!
    @IBOutlet weak var outImg: UIImageView!
    @IBOutlet weak var inImg: UIImageView!
    @IBOutlet weak var lblPrice: VCLanguageLabel!
    @IBOutlet weak var lblItemName: VCLanguageLabel!
    var selExt : (()->())?
    var val = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btnSelExtra(_ sender: UIButton) {
        selExt?()
    }
}
