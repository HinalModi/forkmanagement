//
//  ConfirmOrderListTVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 22/11/22.
//

import UIKit

class ConfirmOrderListTVC: UITableViewCell {

    @IBOutlet weak var lblRating: VCLanguageLabel!
    @IBOutlet weak var lblDist: VCLanguageLabel!
    @IBOutlet weak var lblHotel: VCLanguageLabel!
    @IBOutlet weak var lblOpenTill: VCLanguageLabel!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.setBorder(borderColor: UIColor(named: "darkGrey") ?? .lightGray ,borderWidth: 0.2 )
        bgView.rounded(cornerRadius: 5)
        productImg.rounded(cornerRadius: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
