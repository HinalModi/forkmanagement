//
//  DashBoardVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 16/11/22.
//

import UIKit
import HCSStarRatingView
import CoreLocation
import MapKit
import Network

class DashBoardVC: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var lblNoDataMsg: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var locBgView: UIImageView!
    @IBOutlet weak var searchLoc: VCLanguageTextField!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var restTV: UITableView!
    @IBOutlet weak var viewOffer: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var lblBookTab: UILabel!
    @IBOutlet weak var lblWalkIn: UILabel!
    @IBOutlet weak var lblPickUp: UILabel!
    @IBOutlet weak var imgSelectPickUp: UIImageView!
    @IBOutlet weak var imgSelectWalkIn: UIImageView!
    @IBOutlet weak var imgSelectBookTab: UIImageView!
    var isWalkin = Bool()
    var locManager = CLLocationManager()
    var currentLocation = CLLocation()
    var restList: [RestrauntList] = []
    var restListFilter : [RestrauntList] = []
    var savedLocation = CLLocationCoordinate2D()
    override func viewDidLoad() {
        super.viewDidLoad()
        monitorNetwork()
        searchView.rounded(cornerRadius: 20)
        viewOffer.rounded(cornerRadius: 30)
        viewOffer.setShadow(shadowColor: .gray, shadowOpacity: 0.2, shadowRadius: 2, shadowOffset: CGSize(width: 0, height: 2))
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            txtSearch.setLeftPaddingPoints(20)
        }else{
            txtSearch.setRightPaddingPoints(20)
        }
        if isWalkin{
            remove(asChildViewController: pickUpVC)
            remove(asChildViewController: bookTabVC)
            add(asChildViewController: walkInVC)
            lblPickUp.textColor = .black
            lblWalkIn.textColor = UIColor(named: "red")
            lblBookTab.textColor = .black
            imgSelectBookTab.isHidden = true
            imgSelectWalkIn.isHidden = false
            imgSelectPickUp.isHidden = true
        }else{
            remove(asChildViewController: walkInVC)
            remove(asChildViewController: pickUpVC)
            add(asChildViewController: bookTabVC)
        }
        locManager.requestWhenInUseAuthorization()
        if
            CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() ==  .authorizedAlways
        {
            currentLocation = locManager.location ?? CLLocation()
        }
        locationView.rounded(cornerRadius: 10)
        txtSearch.addTarget(self, action: #selector(self.textIsChanging(textField:)), for: .editingChanged)
        let gestureRecognizer = UITapGestureRecognizer(
            target: self, action:#selector(handleTap))
        gestureRecognizer.delegate = self
        mapView.addGestureRecognizer(gestureRecognizer)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        GlobalFunction.checkInternet(vc: self)
    }
    func monitorNetwork(){
        if #available(iOS 12.0, *) {
            let monitor = NWPathMonitor()
            monitor.pathUpdateHandler = { path in
                if path.status == .satisfied{
                    DispatchQueue.main.async {
                        self.view.makeToast("Internet Connected")
//                        GlobalFunction.checkInternet(vc: self, title: "Internet Connected", alertMessage: "", actionTitle: "Ok")
                    }
                }else{
                    DispatchQueue.main.async {
                        self.view.makeToast("No Internet Connection")
//                        GlobalFunction.checkInternet(vc: self, title: "No Internet Connection", alertMessage: "Please check your wifi or mobile data connection!", actionTitle: "Ok")
                    }
                }
            }
            let queue = DispatchQueue(label: "Network")
            monitor.start(queue: queue)
        }
        
    }
    
    @objc func handleTap(gestureRecognizer: UITapGestureRecognizer) {
        
        let location = gestureRecognizer.location(in: mapView)
        let coordinate = mapView.convert(location, toCoordinateFrom: mapView)
        
        // Add annotation:
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        print(coordinate)
        savedLocation = coordinate
        let allAnnotations = mapView.annotations
        mapView.removeAnnotations(allAnnotations)
        mapView.addAnnotation(annotation)
    }
    lazy var bookTabVC: BookTableVC = {
        let storyboard = UIStoryboard(name: "Guide", bundle: nil)
        var vc = storyboard.instantiateViewController(withIdentifier: "BookTableVC") as! BookTableVC
        vc.isSearch = txtSearch.text ?? ""
        self.add(asChildViewController: vc)
        return vc
    }()
    lazy var walkInVC: PickUpVC = {
        let storyboard = UIStoryboard(name: "Guide", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "PickUpVC") as! PickUpVC
        viewController.isFromWalkIn = true
        self.add(asChildViewController: viewController)
        return viewController
    }()
    lazy var pickUpVC: PickUpVC = {
        let storyboard = UIStoryboard(name: "Guide", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "PickUpVC") as! PickUpVC
        viewController.isFromWalkIn = false
        self.add(asChildViewController: viewController)
        return viewController
    }()
    func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
        bgView.addSubview(viewController.view)
        viewController.view.frame = bgView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }
    
    func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    @IBAction func btnOpenMap(_ sender: UIButton) {
        locBgView.isHidden = false
        btnClose.isHidden = false
        locationView.isHidden = false
    }
    
    @IBAction func closeLocView(_ sender: UIButton) {
        locBgView.isHidden = true
        btnClose.isHidden = true
        locationView.isHidden = true
    }
    @IBAction func btnSaveLocation(_ sender: UIButton) {
        getRestrauntList(searchTxt: "",latitude: "\(savedLocation.latitude)" ,longitude: "\(savedLocation.longitude)")
        locBgView.isHidden = true
        btnClose.isHidden = true
        locationView.isHidden = true        
    }
    @IBAction func btnOpenMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
    @IBAction func btnBookTable(_ sender: UIButton) {
        remove(asChildViewController: walkInVC)
        remove(asChildViewController: pickUpVC)
        add(asChildViewController: bookTabVC)
        lblPickUp.textColor = .black
        lblWalkIn.textColor = .black
        lblBookTab.textColor = UIColor(named: "red")
        imgSelectBookTab.isHidden = false
        imgSelectWalkIn.isHidden = true
        imgSelectPickUp.isHidden = true
    }
    @IBAction func btnPickUp(_ sender: UIButton) {
        remove(asChildViewController: walkInVC)
        remove(asChildViewController: bookTabVC)
        add(asChildViewController: pickUpVC)
        lblPickUp.textColor = UIColor(named: "red")
        lblWalkIn.textColor = .black
        lblBookTab.textColor = .black
        imgSelectBookTab.isHidden = true
        imgSelectWalkIn.isHidden = true
        imgSelectPickUp.isHidden = false
    }
    
    @IBAction func btnWalkIn(_ sender: UIButton) {
        remove(asChildViewController: pickUpVC)
        remove(asChildViewController: bookTabVC)
        add(asChildViewController: walkInVC)
        lblPickUp.textColor = .black
        lblWalkIn.textColor = UIColor(named: "red")
        lblBookTab.textColor = .black
        imgSelectBookTab.isHidden = true
        imgSelectWalkIn.isHidden = false
        imgSelectPickUp.isHidden = true
    }
    @objc func textIsChanging(textField: UITextField) {
        if txtSearch.text != "" {
            remove(asChildViewController: walkInVC)
            remove(asChildViewController: pickUpVC)
            remove(asChildViewController: bookTabVC)
            getRestrauntList(searchTxt: textField.text ?? "",latitude: "\(currentLocation.coordinate.latitude)" ,longitude: "\(currentLocation.coordinate.longitude)")
        }else{
            remove(asChildViewController: walkInVC)
            remove(asChildViewController: pickUpVC)
            add(asChildViewController: bookTabVC)
        }
    }
    //MARK: - Api Call
    func getRestrauntList(searchTxt : String , latitude : String ,longitude : String){
        let param : Parameters = [
            .service_id : "",
            .search : searchTxt,
            .latitude : latitude,
            .longitude : longitude,
        ]
        let request = APIManager.shared.createDataRequest(with: .post, params: param, api: .restaurants, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, restList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    print(restList)
                    self.lblNoDataMsg.isHidden = true
                    let lst = restList["data"] as? [String : Any]
                    let listData = lst?["data"] as! [[String : Any]]
                    self.restList.removeAll()
                    print(listData)
                    for i in listData{
                        let id = i["id"]
                        let rest_name = i["rest_name"]
                        let rest_branch = i["rest_branch"]
                        let no_of_staff = i["no_of_staff"]
                        let country_id = i["country_id"]
                        let state_id = i["state_id"]
                        let city = i["city"]
                        let parent_id = i["parent_id"]
                        let block = i["block"]
                        let street = i["street"]
                        let shop_number = i["shop_number"]
                        let address = i["address"]
                        let pincode = i["pincode"]
                        let contact = i["contact"]
                        let status_id = i["status_id"]
                        let distance = i["distance"]
                        let endtime = i["endtime"]
                        let restImage = i["image"]
                        self.restList.append(RestrauntList(id: id as? Int ?? 0, rest_name: rest_name as? String ?? "", rest_branch: rest_branch as? String ?? "", no_of_staff: no_of_staff as? String ?? "", country_id: country_id as? String ?? "", state_id: state_id as? String ?? "", city: city as? String ?? "", parent_id: parent_id as? String ?? "", block: block as? String ?? "", street: street as? String ?? "", shop_number: shop_number as? String ?? "", address: address as? String ?? "", pincode: pincode as? String ?? "", contact: contact as? String ?? "", status_id: status_id as? Int ?? 0,distance: distance as! Double,endtime: endtime as? String ?? "", restImage: restImage as? String ?? ""))
                        print("restruntList48",self.restList)
                    }
                    self.restTV.reloadData()
                }else{
                    print(message)
                    self.lblNoDataMsg.isHidden = false
                }
            }
        }
    }
}

extension DashBoardVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("restList",restList.count)
        return restList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTVC") as! SearchTVC
        cell.restName.text = restList[indexPath.row].rest_name
        let list = restList[indexPath.row]
//        if txtSearch.text != ""{
//            list = restListFilter[indexPath.row]
//        }else{
//            list = restList[indexPath.row]
//        }
        print(restList[indexPath.row].distance)
        let dist = String(format:"%.2f", list.distance)
        cell.lblDist.text = "\(dist)km"
        cell.lblTime.text = list.endtime
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "BookingTabDetailVC") as! BookingTabDetailVC
        let list = restList[indexPath.row]
        vc.restrauntId = list.id
        let dist = String(format:"%.2f", list.distance)
        vc.dist = "\(dist)km"
        vc.restName = list.rest_name
        vc.openTill = list.endtime
        navigationController?.pushViewController(vc, animated: true)
    }
}
