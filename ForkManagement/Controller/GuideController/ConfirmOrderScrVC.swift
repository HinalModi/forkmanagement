//
//  ConfirmOrderScrVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 22/11/22.
//

import UIKit

class ConfirmOrderScrVC: UIViewController {

    @IBOutlet weak var lblTotalPrice: VCLanguageLabel!
    @IBOutlet weak var lblDressCode: VCLanguageLabel!
    @IBOutlet weak var lblRules: VCLanguageLabel!
    @IBOutlet weak var lblHotelPrice: VCLanguageLabel!
    @IBOutlet weak var lblTime: VCLanguageLabel!
    @IBOutlet weak var lblSeats: VCLanguageLabel!
    @IBOutlet weak var lblDoorType: VCLanguageLabel!
    @IBOutlet weak var lblOrderId: VCLanguageLabel!
    @IBOutlet weak var lblAddress: VCLanguageLabel!
    @IBOutlet weak var lblPhone: VCLanguageLabel!
    @IBOutlet weak var lblName: VCLanguageLabel!
    @IBOutlet weak var backBtn: VCLanguageButton!
    @IBOutlet weak var btnInvoice: UIButton!
    @IBOutlet weak var btnPayment: UIButton!
    @IBOutlet weak var orderlistTV: UITableView!
    @IBOutlet weak var tabConstraint: NSLayoutConstraint!
    @IBOutlet weak var whiteBgView: UIView!
    var orderId = Int()
    var orderConfirmList = [OrdeConfirm]()
    override func viewDidLoad() {
        super.viewDidLoad()
        whiteBgView.rounded(cornerRadius: 20)
        btnPayment.rounded(cornerRadius: 10)
        btnInvoice.rounded(cornerRadius: 10)
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            backBtn.setImage(UIImage(named: "image 187"), for: .normal)
        }else{
            backBtn.setImage(UIImage(named: "rightArrow"), for: .normal)
        }
        lblName.text = userInfo.name
        lblPhone.text = userInfo.contact
        lblOrderId.text = "\(orderId)"
        print(orderId)
        getOrderDetail()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async {
            self.tabConstraint.constant = self.orderlistTV.contentSize.height
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnToReserveFood(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Order", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "OrderVC") as! OrderVC
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnContinue(_ sender: UIButton) {
        navigationController?.popToViewController(ofClass: DashBoardVC.self)
    }
    
    //MARK: - APi Call
    func searchTabInfo(restrauntId : String){
        let params : Parameters = [
            .restaurant_id : restrauntId,
        ]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .search_table, baseURL: .base, setToken: "Bearer " + userInfo.token)
        print(request)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, book in
            DispatchQueue.main.async { [self] in
                if isSuccess{
                    let data = book["data"] as! [String : Any]
                    let tabNo = data["table_no"] as? Int ?? 0
                    let bookData = data["data"] as! [[String : Any]]
                    print(bookData)
                    for i in bookData{
                        let noOfPerson = i["number_of_person"] as? Int ?? 0
                        let doorType = i["type"] as? String ?? ""
                        let tabPrice = i["price"] as? Int ?? 0
                        lblDoorType.text = doorType
                        lblHotelPrice.text = "Rs.\(tabPrice)"
                        lblSeats.text = "\(noOfPerson) Seats"
                    }
                }else{
                    print(message)
                    
                }
            }
        }
    }
    func getOrderDetail(){
        let params : Parameters = [
            .order_id : "\(orderId)",
        ]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .get_order_details, baseURL: .base, setToken: "Bearer " + userInfo.token)
        print(request)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, order in
            DispatchQueue.main.async { [self] in
                if isSuccess{
                    print(order)
                    let orderData = order["data"] as! [String : Any]
                    let order_item = orderData["order_item"] as! [[String : Any]]
                    let restaurant = orderData["restaurant"] as! [String : Any]
                    let restName = restaurant["rest_name"] as? String ?? ""
                    let restaurant_id = orderData["restaurant_id"] as? Int ?? 0
                    searchTabInfo(restrauntId: "\(restaurant_id)")
                    for i in order_item{
                        let order_item_id = i["id"]
                        let order_item_details = i["order_item_details"] as! [String : Any]
                        let order_item_name = order_item_details["name"] as? String ?? ""
                        let order_item_image = order_item_details["image"] as? String ?? ""
                        let order_item_price = order_item_details["price"] as? Int ?? 0
                        self.orderConfirmList.append(OrdeConfirm(order_item_name: restName, order_item_image: order_item_image, order_item_price: order_item_price))
                    }
                    
                    self.orderlistTV.reloadData()
                    let total = orderData["total"] as? String ?? ""
                    lblTotalPrice.text = "Rs.\(total)"
                    let timing = orderData["timing"]
                    let bookingTab = orderData["booking_table"] as! [[String : Any]]
                    for i in bookingTab{
                       let id = i["id"] as? Int ?? 0
                       let rules = i["rules"] as? String ?? ""
                       let dresscode = i["dresscode"] as? String ?? ""
                       let occasion = i["occasion"] as? String ?? ""
                       let date = i["date"] as? String ?? ""
                        lblRules.text = rules
                        lblDressCode.text = dresscode
                        lblTime.text = date
                    }
                }else{
                    print(message)
                }
            }
        }
    }
}
extension ConfirmOrderScrVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderConfirmList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmOrderListTVC") as! ConfirmOrderListTVC
        cell.lblHotel.text = orderConfirmList[indexPath.row].order_item_name
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
}
