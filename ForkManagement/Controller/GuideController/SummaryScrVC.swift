//
//  SummaryScrVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 21/11/22.
//

import UIKit

class SummaryScrVC: UIViewController {

    @IBOutlet weak var doorView: UIView!
    @IBOutlet weak var confirmBgView: UIImageView!
    @IBOutlet weak var confirmScrView: UIView!
    @IBOutlet weak var lbltotalPrice: VCLanguageLabel!
    @IBOutlet weak var lblPrice: VCLanguageLabel!
    @IBOutlet weak var lblTime: VCLanguageLabel!
    @IBOutlet weak var lblDoor: VCLanguageLabel!
    @IBOutlet weak var lblDoorType: VCLanguageLabel!
    @IBOutlet weak var lblPhone: VCLanguageLabel!
    @IBOutlet weak var lblName: VCLanguageLabel!
    @IBOutlet weak var lblHotel: VCLanguageLabel!
    @IBOutlet weak var backBtn: VCLanguageButton!
    @IBOutlet weak var btnPayment: UIButton!
    @IBOutlet weak var txtCoupon: UITextField!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var summaryTV: UITableView!
    @IBOutlet weak var tabConstraint: NSLayoutConstraint!
    @IBOutlet weak var topRect: UIView!
    @IBOutlet weak var whiteBgView: UIView!
    var isFromWalkIn = Bool()
    var isFromPickUp = Bool()
    var hotelName = String()
    var doorType = String()
    var doorNo = String()
    var time = String()
    var cartItemList = [Cart_item]()
    var tablePrice = String()
    var totalPrice = String()
    var restrauntId = Int()
    var orderType = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        whiteBgView.rounded(cornerRadius: 20)
        topRect.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        topRect.layer.cornerRadius = 20
        btnApply.rounded(cornerRadius: 10)
        btnApply.setShadow(shadowColor: .gray, shadowOpacity: 0.2, shadowRadius: 2, shadowOffset: CGSize(width: 0, height: 2))
        txtCoupon.setLeftPaddingPoints(20)
        txtCoupon.rounded(cornerRadius: 5)
        btnPayment.rounded(cornerRadius: 10)
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            backBtn.setImage(UIImage(named: "image 187"), for: .normal)
        }else{
            backBtn.setImage(UIImage(named: "rightArrow"), for: .normal)
        }
        lblTime.text = time
        lblName.text = userInfo.name
        lblPhone.text = userInfo.contact
        lblHotel.text = hotelName
        lblDoorType.text = doorType
        lblDoor.text = doorNo
        lblPrice.text = tablePrice
        if totalPrice != ""{
            lbltotalPrice.text = "Rs.\(totalPrice)"
        }
        confirmScrView.rounded(cornerRadius: 10)
        getCartDetails()
        if isFromPickUp{
            doorView.isHidden = true
        }else{
            doorView.isHidden = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.updateViewConstraints()
    }
    
    func setTableHeight(tableview : UITableView , heightConstraint : NSLayoutConstraint){
        DispatchQueue.main.async {
            heightConstraint.constant = tableview.contentSize.height
            self.view.layoutIfNeeded()
            heightConstraint.constant = tableview.contentSize.height
        }
    }
    
    //MARK: - APi Manager
    func getCartDetails(){
        let params : Parameters = [:]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .get_cart_details, baseURL: .base, setToken: "Bearer " + userInfo.token)//createDataRequest(with: .post, params: param, api: .get_cart_details, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, cartList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    print(cartList)
                    let cartData = cartList["data"] as! [String : Any]
                    let cart_item = cartData["cart_item"] as! [[String : Any]]
                    let total_price = cartData["total"]
                    self.totalPrice = total_price as? String ?? ""
                    self.lbltotalPrice.text = "Rs.\(total_price ?? "")"
                    print(cart_item)
                    self.cartItemList.removeAll()
                    for i in cart_item{
                        let cart_Item_Detail = i["cart_item_details"] as! [String : Any]
                        let cart_id = i["id"]
                        let cart_item_id = cart_Item_Detail["id"]
                        let cart_item_name = cart_Item_Detail["name"]
                        let cart_item_price = cart_Item_Detail["price"]
                        let cart_item_image = cart_Item_Detail["image"]
                        let category_Id = cart_Item_Detail["category_id"]
                        let qty = i["qty"]
                        let ext_item_Detail = i["extra_item_details"] as? [[String : Any]] ?? [[String : Any]]()
                        self.cartItemList.append(Cart_item(cart_item_id: cart_item_id as? Int ?? 0, category_id: category_Id as? Int ?? 0, cart_item_name: cart_item_name as? String ?? "", cart_item_price: cart_item_price as? Int ?? 0, cart_item_image: cart_item_image as? String ?? "", extra_item_details: ext_item_Detail, qty: qty as? Int ?? 0, cart_Id: cart_id as? Int ?? 0))
                    }
                    self.setTableHeight(tableview: self.summaryTV,heightConstraint: self.tabConstraint)
                    self.summaryTV.reloadData()
                }else{
                    print(message)
                    
                }
            }
        }
    }
    func updateValue(cart_item_id : String , qty : String){
        let params : Parameters = [
            .cart_item_id : cart_item_id,
            .qty : qty
        ]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .update_qty, baseURL: .base, setToken: "Bearer " + userInfo.token)//createDataRequest(with: .post, params: param, api: .update_qty, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, cartList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    self.getCartDetails()
                }else{
                    print(message)
                }
            }
        }
    }
    func deleteCartitem(cart_item_id : String){
        let params : Parameters = [
            .cart_item_id : cart_item_id,
        ]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .remove_item_cart, baseURL: .base, setToken: "Bearer " + userInfo.token)//createDataRequest(with: .post, params: param, api: .remove_item_cart, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, cartList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    self.getCartDetails()
                }else{
                    print(message)
                }
            }
        }
    }
    func createOrder(){
        let params : Parameters = [
            .restaurant_id : "\(restrauntId)",
            .type : orderType,
            .queue_id : "",
            .booking_table_id : UserDefaults.standard.string(forKey: "BookingTableId") ?? ""
        ]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .create_order, baseURL: .base, setToken: "Bearer " + userInfo.token)
        print(request)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, order in
            DispatchQueue.main.async { [self] in
                if isSuccess{
                    print(message)
                    let orderId = order["data"]
                    print(orderId)
                    let storyBoard = UIStoryboard(name: "Order", bundle: nil)
                    confirmBgView.isHidden = false
                    confirmScrView.isHidden = false
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        let vc = storyBoard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                        vc.orderId = "\(orderId ?? "")"
                        vc.totalPrice = "Rs.\(self.totalPrice)"
                        vc.flowType = "order"
                        if self.isFromPickUp{
                            vc.isfromPickUp = true
                        }else{
                            vc.isfromPickUp = false
                        }
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }else{
                    print(message)
                    
                }
            }
        }
    }
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnToPaymentScr(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "WalkInPickUp", bundle: nil)
        if isFromWalkIn{
            let vc = storyBoard.instantiateViewController(withIdentifier: "WalkInConfirmVC") as! WalkInConfirmVC
            navigationController?.pushViewController(vc, animated: true)
        }else if isFromPickUp{
            createOrder()
//            let vc = storyBoard.instantiateViewController(withIdentifier: "PickUpConfirmVC") as! PickUpConfirmVC
//            navigationController?.pushViewController(vc, animated: true)
        } else{
            createOrder()
        }
    }
}
extension SummaryScrVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartItemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartItemListTVC") as! CartItemListTVC
        let list = cartItemList[indexPath.row]
        cell.lblfoodName.text = list.cart_item_name
        cell.lblFoodPrice.text = "Rs.\(list.cart_item_price)"
        cell.productImg.sd_setImage(with: URL(string: list.cart_item_image), placeholderImage: UIImage(named: "default_1"))
        cell.txtQty.text = "\(list.qty)"
        cell.lblFoodPrice.text = "Rs.\(list.cart_item_price)"
        var extName = [String]()
        for j in list.extra_item_details {
            let ext_Name = j["name"] as? String ?? ""
            let price = j["price"] as? String ?? ""
            extName.append(ext_Name)
            let extPrice = Int(price)
            cell.lblFoodToppings.text = extName.joined(separator: ",")
            cell.lblTopPrice.text = "Rs.\(extPrice ?? 0)"
        }
        cell.delRow = {
            self.deleteCartitem(cart_item_id: "\(list.cart_Id)")
            self.cartItemList.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
            self.setTableHeight(tableview: self.summaryTV,heightConstraint: self.tabConstraint)
        }
        cell.incrVal = {
            self.updateValue(cart_item_id: "\(list.cart_Id)", qty: cell.txtQty.text ?? "")
        }
        cell.decrVal = {
            if cell.txtQty.text != "0"{
                self.updateValue(cart_item_id: "\(list.cart_Id)", qty: cell.txtQty.text ?? "")
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
}
