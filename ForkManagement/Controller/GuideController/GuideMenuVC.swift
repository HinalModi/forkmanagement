//
//  GuideMenuVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 15/11/22.
//

import UIKit
import LGSideMenuController
class GuideMenuVC: UIViewController {

    @IBOutlet weak var imgThumbnail: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
    
        imgThumbnail.rounded(cornerRadius: 10)
    }
    @IBAction func btnToDashBoard(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnOpenMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
    
}
