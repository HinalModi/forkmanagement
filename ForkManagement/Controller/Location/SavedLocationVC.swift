//
//  LocationVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 29/11/22.
//

import UIKit
import LGSideMenuController
import MapKit
import CoreLocation

class SavedLocationVC: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var locationViewBg: UIImageView!
    @IBOutlet weak var hotelSelImg: UIImageView!
    @IBOutlet weak var workSelImg: UIImageView!
    @IBOutlet weak var homeSelImg: UIImageView!
    @IBOutlet weak var lblHotelDefault: UILabel!
    @IBOutlet weak var lblHomeDefault: UILabel!
    @IBOutlet weak var lblWorkDefault: UILabel!
    @IBOutlet weak var hotelDoneImg: UIImageView!
    @IBOutlet weak var workDoneImg: UIImageView!
    @IBOutlet weak var homeDoneImg: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var btnSaveLoc: UIButton!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var locTV: UITableView!
    
    let locmanager = CLLocationManager()
    var location : CLLocation?
    var isUpdatingLocation = false
    var locationList : [Location] = []
    var lat = String()
    var long = String()
    var savedLocation = CLLocationCoordinate2D()
    let geocoder = CLGeocoder()
    var locality = ""
    var administrativeArea = ""
    var country = ""
    var locName = ""
    var locType = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        btnSaveLoc.rounded(cornerRadius: 10)
        locationView.rounded(cornerRadius: 20)
        mapView.rounded(cornerRadius: 20)
        let authorizationstatus = CLLocationManager.authorizationStatus()
        if authorizationstatus == .notDetermined{
            locmanager.requestWhenInUseAuthorization()
            return
        }
    
       //If Permission Denied
        if authorizationstatus == .denied || authorizationstatus == .restricted{
            reportLocationServiceDeniedError()
            return
        }
        //Start and stop locationmanager
        if isUpdatingLocation{
            stopLocationManager()
        }else{
            location = nil
            startLocationManager()
        }
        getAllLocation()
        let gestureRecognizer = UITapGestureRecognizer(
            target: self, action:#selector(handleTap))
        gestureRecognizer.delegate = self
        mapView.addGestureRecognizer(gestureRecognizer)
    }
    @objc func handleTap(gestureRecognizer: UITapGestureRecognizer) {
        
        let locationMap = gestureRecognizer.location(in: mapView)
        let coordinate = mapView.convert(locationMap, toCoordinateFrom: mapView)
        
        // Add annotation:
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        print(coordinate)
        savedLocation = coordinate
        let allAnnotations = mapView.annotations
        mapView.removeAnnotations(allAnnotations)
        mapView.addAnnotation(annotation)
        geocoder.reverseGeocodeLocation(location ?? CLLocation(), completionHandler: {(placemarks, error) in
            if (error != nil) {
                print("Error in reverseGeocode")
            }
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count > 0 {
                let placemark = placemarks![0]
                self.locality = placemark.locality!
                self.administrativeArea = placemark.administrativeArea!
                self.country = placemark.country!
                self.locName = "\(self.locality)" + ",\(self.administrativeArea)" + ",\(self.country)"
            }
        })
    }
    func stopLocationManager(){
        if isUpdatingLocation{
            locmanager.stopUpdatingLocation()
            locmanager.delegate = nil
            isUpdatingLocation = false
        }
    }
    func startLocationManager(){
        if CLLocationManager.locationServicesEnabled(){
            locmanager.delegate = self
            locmanager.desiredAccuracy = kCLLocationAccuracyBest
            locmanager.requestLocation()
            locmanager.startUpdatingLocation()
            isUpdatingLocation = true
        }
    }
    
    func  reportLocationServiceDeniedError(){
        let alert = UIAlertController(title: "Location service not enabled", message: "Please go to Setting > Privacy to enable the location for this app", preferredStyle:.alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: {(cAlertAction) in
            //Redirect to Settings app
     UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        })
        alert.addAction(action)
        present(alert,animated:true,completion: nil)
         
    }
    func setTypeSelection(type1OutImage:Bool,type2OutImage:Bool,type3OutImage:Bool){
        UIView.animate(withDuration: 0.1) {
            self.homeSelImg.isHidden = type1OutImage
            self.workSelImg.isHidden = type2OutImage
            self.hotelSelImg.isHidden = type3OutImage
        }
    }
   
    //MARK: - Api Call
    func storeLocation(){
        let params : Parameters = [
            .latitude : savedLocation.latitude,
            .longitude : savedLocation.latitude,
            .location : locName,
            .type : locType,
            .is_default : "1",
        ]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .location_store, baseURL: .base, setToken: "Bearer " + userInfo.token)
        print(request)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, location in
            DispatchQueue.main.async { [self] in
                if isSuccess{
                    getAllLocation()
                    UIView.animate(withDuration: 0.1) {
                        self.locationView.isHidden = true
                        self.locationViewBg.isHidden = true
                        self.btnClose.isHidden = true
                    }
                }else{
                    print(message)
                }
            }
        }
    }
    func getAllLocation(){
        let params : Parameters = [:]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .location_list, baseURL: .base, setToken: "Bearer " + userInfo.token)
        print(request)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, location in
            DispatchQueue.main.async { [self] in
                if isSuccess{
                    print(message)
                    let locationdata = location["data"] as! [[String : Any]]
                    print(locationdata)
                    self.locationList.removeAll()
                    for i in locationdata{
                        let lat = i["latitude"]
                        let long = i["longitude"]
                        let type = i["type"]
                        let location = i["location"]
                        let is_default = i["is_default"]
                        let id = i["id"]
                        self.locationList.append(Location(lat: lat as? String ?? "", long: long as? String ?? "", type: type as? String ?? "", location: location as? String ?? "", is_default: is_default as? Int ?? 0, id: id as? Int ?? 0))
                    }
                    self.locTV.reloadData()
                }else{
                    print(message)
                }
            }
        }
    }
    func setDefault(locationId : Int){
        let params : Parameters = [
            .location_id : "\(locationId)",
        ]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .setdefault, baseURL: .base, setToken: "Bearer " + userInfo.token)
        print(request)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, location in
            DispatchQueue.main.async { [self] in
                if isSuccess{
                    print(location)
                    getAllLocation()
                }else{
                    print(message)
                }
            }
        }
    }
    func deleteLoc(locationId : Int){
        let params : Parameters = [
            .location_id : "\(locationId)",
        ]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .location_delete, baseURL: .base, setToken: "Bearer " + userInfo.token)
        print(request)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, location in
            DispatchQueue.main.async { [self] in
                if isSuccess{
                    print(message)
                    getAllLocation()
                }else{
                    print(message)
                }
            }
        }
    }
    //MARK: - Action
   
    @IBAction func btnSaveLoc(_ sender: UIButton) {
        storeLocation()
    }
    @IBAction func btnSelHome(_ sender: UIButton) {
        locType = "home"
        setTypeSelection(type1OutImage: false, type2OutImage: true, type3OutImage: true)
    }
    
    @IBAction func btnSelWork(_ sender: UIButton) {
        locType = "office"
        setTypeSelection(type1OutImage: true, type2OutImage: false, type3OutImage: true)
    }
    
    @IBAction func btnSelHotel(_ sender: UIButton) {
        locType = "hotel"
        setTypeSelection(type1OutImage: true, type2OutImage: true, type3OutImage: false)
    }
    @IBAction func btnMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
    
    @IBAction func btnOpenFilter(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1) {
            self.locationView.isHidden = false
            self.locationViewBg.isHidden = false
            self.btnClose.isHidden = false
        }
    }
    @IBAction func btnCloseLocView(_ sender: UIButton) {
        getAllLocation()
        UIView.animate(withDuration: 0.1) {
            self.locationView.isHidden = true
            self.locationViewBg.isHidden = true
            self.btnClose.isHidden = true
        }
    }
}
extension SavedLocationVC : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager didfailwithError : \(error) ")
        if (error as NSError).code == CLError.locationUnknown.rawValue {
            return
        }
        stopLocationManager()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = locations.first!
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location?.coordinate ?? CLLocationCoordinate2D(), span: span)
        mapView.setRegion(region, animated: true)
        stopLocationManager()
    }
    
}

extension SavedLocationVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SavedLocationTVC") as! SavedLocationTVC
        cell.lblType.text = locationList[indexPath.row].type
        cell.lblRestName.text = locationList[indexPath.row].location
        if locationList[indexPath.row].is_default == 0{
            cell.btnMakeDefault.isHidden = false
            cell.imgDone.isHidden = true
        }else{
            cell.btnMakeDefault.isHidden = true
            cell.imgDone.isHidden = false
        }
        if locationList[indexPath.row].type == "office" && locationList[indexPath.row].is_default == 0{
            lblWorkDefault.isHidden = true
            workDoneImg.isHidden = true
        }
        if locationList[indexPath.row].type == "office" && locationList[indexPath.row].is_default == 1{
            lblWorkDefault.isHidden = false
            workDoneImg.isHidden = false
        }
        if locationList[indexPath.row].type == "home" && locationList[indexPath.row].is_default == 0{
            homeDoneImg.isHidden = true
            lblHomeDefault.isHidden = true
        }
        if locationList[indexPath.row].type == "home" && locationList[indexPath.row].is_default == 1{
            homeDoneImg.isHidden = false
            lblHomeDefault.isHidden = false
        }
        if locationList[indexPath.row].type == "hotel" && locationList[indexPath.row].is_default == 0{
            lblHotelDefault.isHidden = true
            hotelDoneImg.isHidden = true
        }
        if locationList[indexPath.row].type == "hotel" && locationList[indexPath.row].is_default == 1{
            lblHotelDefault.isHidden = false
            hotelDoneImg.isHidden = false
        }
        let loc_Id = Int(self.locationList[indexPath.row].id)
        cell.makeDefault = {
            self.setDefault(locationId: loc_Id)
        }
        cell.deleteLoc = {
            tableView.beginUpdates()
            self.deleteLoc(locationId: loc_Id)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            self.locationList.remove(at: indexPath.row)
            tableView.endUpdates()
            
        }
        return cell
    }
    
    
}
