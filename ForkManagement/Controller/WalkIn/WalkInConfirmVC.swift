//
//  WalkInConfirmVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 28/11/22.
//

import UIKit

class WalkInConfirmVC: UIViewController {
    
    
    @IBOutlet weak var backBtn: VCLanguageButton!
    @IBOutlet weak var btnInvoice: UIButton!
    @IBOutlet weak var btnGetDirection: UIButton!
    @IBOutlet weak var orderlistTV: UITableView!
    @IBOutlet weak var tabConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        btnGetDirection.rounded(cornerRadius: 10)
        btnInvoice.rounded(cornerRadius: 10)
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            backBtn.setImage(UIImage(named: "image 187"), for: .normal)
        }else{
            backBtn.setImage(UIImage(named: "rightArrow"), for: .normal)
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async {
            self.tabConstraint.constant = self.orderlistTV.contentSize.height
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension WalkInConfirmVC : UITableViewDelegate , UITableViewDataSource {
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
return 1
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmOrderListTVC") as! ConfirmOrderListTVC
return cell
}

func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
self.viewWillLayoutSubviews()
}
}
