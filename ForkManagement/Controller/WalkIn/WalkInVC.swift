//
//  WalkInVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 25/11/22.
//

import UIKit
import CoreLocation
import DropDown

class WalkInVC: UIViewController {
    
    @IBOutlet weak var lblSeats: VCLanguageLabel!
    @IBOutlet weak var lblTime: VCLanguageLabel!
    @IBOutlet weak var restName: VCLanguageLabel!
    @IBOutlet weak var lblCustomerQueueNo: VCLanguageLabel!
    @IBOutlet weak var walkInQueueView: UIView!
    @IBOutlet weak var activityInc: UIActivityIndicatorView!
    @IBOutlet weak var tabHeight: NSLayoutConstraint!
    @IBOutlet weak var lblNoOfPr: UILabel!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var lblQueueNo: UILabel!
    @IBOutlet weak var walkInTV: UITableView!
    @IBOutlet weak var viewQueue: UIView!
    @IBOutlet weak var viewPlace: UIView!
    @IBOutlet weak var viewOccasion: UIView!
    @IBOutlet weak var viewPerson: UIView!
    @IBOutlet weak var getInQueueView: UIView!
    var locManager = CLLocationManager()
    var currentLocation = CLLocation()
    var restruntList : [RestrauntList] = []
    var restrauntId = Int()
    let dropDown = DropDown()
    var personList = ["Person 1","Person 2","Person 3","Person 4","Person 5","Person 6","Person 7","Person 8"]
    var action = String()
    var queueNo = Int()
    var restrauntName = String()
    var orderId = Int()
    var queueId = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        getInQueueView.rounded(cornerRadius: 5)
        viewQueue.rounded(cornerRadius: 5)
        viewPlace.rounded(cornerRadius: 5)
        viewOccasion.rounded(cornerRadius: 5)
        viewPerson.rounded(cornerRadius: 5)
        getQueueList()
        restName.text = restrauntName
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        locManager.requestWhenInUseAuthorization()
        if
            CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() ==  .authorizedAlways
        {
            currentLocation = locManager.location ?? CLLocation()
        }
        if currentLocation.coordinate.latitude != 0.0 || currentLocation.coordinate.longitude != 0.0{
            getRestrauntList()
        }
        lblSeats.text = "For \(lblNoOfPr.text ?? "") Persons"
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setTableHeight(tableview: walkInTV, heightConstraint: tabHeight)
    }
    func setTableHeight(tableview : UITableView , heightConstraint : NSLayoutConstraint){
        DispatchQueue.main.async {
            heightConstraint.constant = tableview.contentSize.height
            self.view.layoutIfNeeded()
            heightConstraint.constant = tableview.contentSize.height
        }
    }
    
    @IBAction func btnToWalkInQueue(_ sender: UIButton) {
        getInQueue()
    }
    @IBAction func btnWalkInQueue(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "WalkInPickUp", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "WalkInQueueVC") as! WalkInQueueVC
        vc.queueId = queueId
        vc.orderId = orderId
        vc.restrauntId = restrauntId
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: - DropDown
    
    @IBAction func btnOpenPerson(_ sender: UIButton) {
        dropDown.dataSource = personList
        dropDown.show()
        dropDown.anchorView = dropDownView
        dropDown.selectionAction = { [self] (index: Int, item: String) in
            if let number = Int.parse(from: item) {
                lblNoOfPr.text = "\(number)"
            }
            getRestrauntList()
        }
    }
    //MARK: - Api Call
    func getRestrauntList(){
        activityInc.startAnimating()
        let param : Parameters = [
            .service_id : "2",
            .latitude : "\(currentLocation.coordinate.latitude)",
            .longitude : "\(currentLocation.coordinate.longitude)",
        ]
        let request = APIManager.shared.createDataRequest(with: .post, params: param, api: .restaurants, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, restList in
            DispatchQueue.main.async {
                self.activityInc.stopAnimating()
                if isSuccess{
                    print(message)
                    //                print(restList)
                    let lst = restList["data"] as? [String : Any]
                    let listData = lst?["data"] as! [[String : Any]]
                    self.restruntList.removeAll()
                    print(listData)
                    for i in listData{
                        let id = i["id"]
                        let rest_name = i["rest_name"]
                        let rest_branch = i["rest_branch"]
                        let no_of_staff = i["no_of_staff"]
                        let country_id = i["country_id"]
                        let state_id = i["state_id"]
                        let city = i["city"]
                        let parent_id = i["parent_id"]
                        let block = i["block"]
                        let street = i["street"]
                        let shop_number = i["shop_number"]
                        let address = i["address"]
                        let pincode = i["pincode"]
                        let contact = i["contact"]
                        let status_id = i["status_id"]
                        let distance = i["distance"]
                        let endtime = i["endtime"]
                        let restImage = i["image"]
                        self.restruntList.append(RestrauntList(id: id as? Int ?? 0, rest_name: rest_name as? String ?? "", rest_branch: rest_branch as? String ?? "", no_of_staff: no_of_staff as? String ?? "", country_id: country_id as? String ?? "", state_id: state_id as? String ?? "", city: city as? String ?? "", parent_id: parent_id as? String ?? "", block: block as? String ?? "", street: street as? String ?? "", shop_number: shop_number as? String ?? "", address: address as? String ?? "", pincode: pincode as? String ?? "", contact: contact as? String ?? "", status_id: status_id as? Int ?? 0,distance: distance as! Double,endtime: endtime as? String ?? "", restImage: restImage as? String ?? ""))
                        print("restruntList48",self.restruntList)
                    }
                    self.setTableHeight(tableview: self.walkInTV, heightConstraint: self.tabHeight)
                    self.walkInTV.reloadData()
                }else{
                    print(message)
                    self.activityInc.stopAnimating()
                }
            }
        }
    }
    func queueConfirmAction(){
        let param : Parameters = [
            .restaurant_id : "\(restrauntId)",
            .person : lblNoOfPr.text ?? "",
            .occasion : "birthday",
            .area : "inside",
            .action : action
        ]
        var request : URLRequest!
        if userInfo == nil{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .queue_confirmation_action, baseURL: .base, setToken: "")
        }else{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .queue_confirmation_action, baseURL: .base, setToken: "Bearer " + userInfo.token)
        }
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, restList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    print(restList)
                    let infodata = restList["data"] as! [String : Any]
                    print(infodata)
                    self.getQueueList()
                    self.view.makeToast(message)
                }else{
                    print(message)
                    self.view.makeToast(message)
                }
            }
        }
    }
    func getInQueue(){
        let param : Parameters = [
            .restaurant_id : "\(restrauntId)",
            .person : lblNoOfPr.text ?? "",
            .occasion : "birthday",
            .area : "inside"
        ]
        var request : URLRequest!
        if userInfo == nil{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .get_in_queue, baseURL: .base, setToken: "")
        }else{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .get_in_queue, baseURL: .base, setToken: "Bearer " + userInfo.token)
        }
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, restList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    self.view.makeToast(message)
                    self.getQueueList()

                }else{
                    print(message)
                    self.view.makeToast(message)
                }
            }
        }
    }
    func getQueueList(){
       
        let param : Parameters = [
            .restaurant_id : "\(restrauntId)",
        ]
        var request : URLRequest!
        if userInfo == nil{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .queue_list, baseURL: .base, setToken: "")
        }else{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .queue_list, baseURL: .base, setToken: "Bearer " + userInfo.token)
        }
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, queueList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    print(queueList)
                    let queueId = queueList["data"] as? Int ?? 0
                    self.lblQueueNo.text = "\(queueId)"
                    self.getCustomerQueue()
                }else{
                    print(message)
                    self.getCustomerQueue()
                }
            }
        }
    }
    func getCustomerQueue(){
        let param : Parameters = [
            .restaurant_id : "\(restrauntId)",
        ]
        var request : URLRequest!
        if userInfo == nil{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .customer_queue_list, baseURL: .base, setToken: "")
        }else{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .customer_queue_list, baseURL: .base, setToken: "Bearer " + userInfo.token)
        }
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, queueList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    let queueData = queueList["data"] as! [String : Any]
                    print(queueData)
                    let queueNo = queueData["queue"] as? Int ?? 0
                    let queueId = queueData["queue_id"] as? Int ?? 0
                    let status = queueData["status"] as? String ?? ""
                    self.orderId = queueId
                    self.lblQueueNo.text = "\(queueNo)"
                    self.queueId = queueId
                    print(queueNo)
                    print(status)
                    if status == "open" && queueNo == 0 {
                        UIView.animate(withDuration: 0.1) {
                            self.walkInQueueView.isHidden = false
                            self.lblCustomerQueueNo.text = "\(queueNo)"
                        }
                    }
                }else{
                    print(message)
                }
            }
        }
    }
}
extension WalkInVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("restruntList108",restruntList.count)
        return restruntList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalkInTVC") as! WalkInTVC
        cell.restName.text = restruntList[indexPath.row].rest_name
        let list = restruntList[indexPath.row]
        print(restruntList[indexPath.row].distance)
        let dist = String(format:"%.2f", list.distance)
        cell.lblDist.text = "\(dist)km"
        cell.lblTime.text = "Open Till \(list.endtime)"
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "BookingTabDetailVC") as! BookingTabDetailVC
//        navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
}
