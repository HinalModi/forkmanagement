//
//  WalkInQueueVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 28/11/22.
//

import UIKit

class WalkInQueueVC: UIViewController {

    @IBOutlet weak var viewExit: UIView!
    @IBOutlet weak var viewCancel: UIView!
    @IBOutlet weak var viewPass: UIView!
    @IBOutlet weak var lblPhone: VCLanguageLabel!
    @IBOutlet weak var lblName: VCLanguageLabel!
    @IBOutlet weak var lblOrderId: VCLanguageLabel!
    @IBOutlet weak var lblQueueId: VCLanguageLabel!
    @IBOutlet weak var backBtn: VCLanguageButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnViewMenu: UIButton!
    var queueId = Int()
    var orderId = Int()
    var restrauntId = Int()
    override func viewDidLoad() {
        super.viewDidLoad()

        btnShare.rounded(cornerRadius: 10)
        btnViewMenu.rounded(cornerRadius: 10)
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            backBtn.setImage(UIImage(named: "image 187"), for: .normal)
        }else{
            backBtn.setImage(UIImage(named: "rightArrow"), for: .normal)
        }
        print(queueId)
        lblQueueId.text = "\(queueId)"
        lblOrderId.text = "\(orderId)"
        if userInfo != nil{
            lblName.text = userInfo.name
            lblPhone.text = userInfo.contact
        }
    }
    
    @IBAction func btnExitAction(_ sender: UIButton) {
        queueSelectAction(action: "exit")
    }
    @IBAction func btnCancelAction(_ sender: UIButton) {
        queueSelectAction(action: "cancel")
    }
    func queueSelectAction(action : String){
        let params : Parameters = [
            .action_type : action,
            .queue_id : "\(queueId)",
            .restaurant_id : "\(restrauntId)",
        ]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .action, baseURL: .base, setToken: "Bearer " + userInfo.token)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, cancelqueue in
            DispatchQueue.main.async { [self] in
                if isSuccess{
                    print(cancelqueue)
                }else{
                    print(message)
                }
            }
        }
        
    }
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPass(_ sender: UIButton) {
        queueSelectAction(action: "pass")

    }
}
