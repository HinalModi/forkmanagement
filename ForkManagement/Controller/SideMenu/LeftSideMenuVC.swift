//
//  SideMenuVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 23/11/22.
//

import UIKit
import LGSideMenuController
import FirebaseAuth

class LeftSideMenuVC: UIViewController {

    @IBOutlet weak var supportView: UIView!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var walletView: UIView!
    @IBOutlet weak var orderView: UIView!
    @IBOutlet weak var btnArabic: UIButton!
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var switchOnOff: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()

        switchOnOff.setBorder(borderColor: .gray, borderWidth: 0.5)
        switchOnOff.setShadow(shadowColor: .gray, shadowOpacity: 0.2, shadowRadius: 2, shadowOffset: CGSize(width: 0, height: 2))
        switchOnOff.rounded(cornerRadius: 15)
        if VCLangauageManager.sharedInstance.isLanguageEnglish() {
            btnArabic.backgroundColor = UIColor(named: "btnGrey")
            btnEnglish.backgroundColor = UIColor(named: "red")
        }else{
            btnArabic.backgroundColor = UIColor(named: "red")
            btnEnglish.backgroundColor = UIColor(named: "btnGrey")
        }
        if userInfo == nil{
            supportView.isHidden = true
            locationView.isHidden = true
            walletView.isHidden = true
            orderView.isHidden = true
        }else{
            supportView.isHidden = false
            locationView.isHidden = false
            walletView.isHidden = false
            orderView.isHidden = false
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnEnglish.roundedCorners(corners: [.topLeft , .bottomLeft], radius: 20)
        btnArabic.roundedCorners(corners: [.topRight , .bottomRight], radius: 20)
    }
    
    @IBAction func btnCloseMenu(_ sender: UIButton) {
        sideMenuController?.hideLeftView()
    }
    @IBAction func btnSelectEnglish(_ sender: UIButton) {
        btnArabic.backgroundColor = UIColor(named: "btnGrey")
        btnEnglish.backgroundColor = UIColor(named: "red")
        if VCLangauageManager.sharedInstance.isLanguageArabic() {
            VCLangauageManager.sharedInstance.changeAppLanguage(To: .english)
            Constant.APPDELEGATE.setRootVC()
        }
    }
    @IBAction func btnSelectArabic(_ sender: UIButton) {
        btnArabic.backgroundColor = UIColor(named: "red")
        btnEnglish.backgroundColor = UIColor(named: "btnGrey")
        if VCLangauageManager.sharedInstance.isLanguageEnglish() {
            VCLangauageManager.sharedInstance.changeAppLanguage(To: .arabic)
            Constant.APPDELEGATE.setRootVC()
        }
    }
    
    @IBAction func btnToScan(_ sender: UIButton) {
    }
    
    @IBAction func btnWalkIn(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Guide", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
        vc?.isWalkin = true
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideLeftView()
    }
    
    @IBAction func btnLogReg(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Do you want to Logout?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ToLoginVC") as? ToLoginVC
            if let viewController = vc {
                (self.sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
            }
            do {
                
                try Auth.auth().signOut()
                if userInfo != nil{
                    userInfo = nil
                    UserInfoManager.removeUserInfo()
                    if let bundleID = Bundle.main.bundleIdentifier {
                        UserDefaults.standard.removePersistentDomain(forName: bundleID)
                    }
                }
            }catch{
                print(error.localizedDescription)
            }
            self.sideMenuController?.hideLeftView()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: { (action: UIAlertAction!) in
          print("Handle Cancel Logic here")
          }))
        present(alert, animated: true)
    }
    
    @IBAction func btnHome(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Guide", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
        
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideLeftView()
        
    }
    
    @IBAction func btnFood(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Order", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "OrderVC") as? OrderVC
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideLeftView()

    }
    
    @IBAction func btnWallet(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Order", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "WalletVC") as? WalletVC
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideLeftView()
    }
    
    @IBAction func btnLocation(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Order", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SavedLocationVC") as? SavedLocationVC
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideLeftView()
    }
    
    @IBAction func btnSupport(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Order", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideLeftView()
    }
    
    @IBAction func btnContact(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Order", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DashBoardContactVC") as? DashBoardContactVC
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideLeftView()
    }
    
    
}
