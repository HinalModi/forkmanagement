//
//  LGSideMenuViewController.swift
//  ForkManagement
//
//  Created by Ankit Dave on 23/11/22.
//

import UIKit
import LGSideMenuController

class LGSideMenu : LGSideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        leftViewPresentationStyle = .slideBelow
        leftViewWidth = view.frame.width / 1.2
        rootViewCoverBlurEffect = UIBlurEffect(style: .dark)
        isLeftViewSwipeGestureEnabled = false
    }
    

}
