//
//  RightSideMenuVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 05/12/22.
//

import UIKit

class RightSideMenuVC: UIViewController {

    @IBOutlet weak var btnArabic: UIButton!
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var switchOnOff: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()

        switchOnOff.setBorder(borderColor: .gray, borderWidth: 0.5)
        switchOnOff.setShadow(shadowColor: .gray, shadowOpacity: 0.2, shadowRadius: 2, shadowOffset: CGSize(width: 0, height: 2))
        switchOnOff.rounded(cornerRadius: 15)
        if VCLangauageManager.sharedInstance.isLanguageEnglish() {
            btnArabic.backgroundColor = UIColor(named: "btnGrey")
            btnEnglish.backgroundColor = UIColor(named: "red")
        }else{
            btnArabic.backgroundColor = UIColor(named: "red")
            btnEnglish.backgroundColor = UIColor(named: "btnGrey")
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnEnglish.roundedCorners(corners: [.topLeft , .bottomLeft], radius: 20)
        btnArabic.roundedCorners(corners: [.topRight , .bottomRight], radius: 20)
    }
    
    @IBAction func btnCloseMenu(_ sender: UIButton) {
        sideMenuController?.hideRightView()
    }
    @IBAction func btnSelectEnglish(_ sender: UIButton) {
        btnArabic.backgroundColor = UIColor(named: "btnGrey")
        btnEnglish.backgroundColor = UIColor(named: "red")
        if VCLangauageManager.sharedInstance.isLanguageArabic() {
            VCLangauageManager.sharedInstance.changeAppLanguage(To: .english)
            Constant.APPDELEGATE.setRootVC()
        }
        
    }
    @IBAction func btnSelectArabic(_ sender: UIButton) {
        btnArabic.backgroundColor = UIColor(named: "red")
        btnEnglish.backgroundColor = UIColor(named: "btnGrey")
        if VCLangauageManager.sharedInstance.isLanguageEnglish() {
            VCLangauageManager.sharedInstance.changeAppLanguage(To: .arabic)
            Constant.APPDELEGATE.setRootVC()
        }
    }
    
    @IBAction func btnToScan(_ sender: UIButton) {
    }
    
    @IBAction func btnWalkIn(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Guide", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
        vc?.isWalkin = true
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideRightView()
    }
    
    @IBAction func btnLogReg(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideRightView()
    }
    
    @IBAction func btnHome(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Guide", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
        
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideRightView()
        
    }
    
    @IBAction func btnFood(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Guide", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "BookingTabScrVC") as? BookingTabScrVC
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideRightView()

    }
    
    @IBAction func btnWallet(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Order", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "WalletVC") as? WalletVC
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideRightView()
    }
    
    @IBAction func btnLocation(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Order", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SavedLocationVC") as? SavedLocationVC
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideRightView()
    }
    
    @IBAction func btnSupport(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Order", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideRightView()
    }
    
    @IBAction func btnContact(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Order", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DashBoardContactVC") as? DashBoardContactVC
        if let viewController = vc {
            (sideMenuController!.rootViewController as? UINavigationController)?.pushViewController(viewController, animated: false)
        }
        self.sideMenuController?.hideRightView()
    }
    
}
