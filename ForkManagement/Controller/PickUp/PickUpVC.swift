//
//  PickUpVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 25/11/22.
//

import UIKit
import CoreLocation
class PickUpVC: UIViewController {

    @IBOutlet weak var activityInc: UIActivityIndicatorView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var pickUpTV: UITableView!
    var locManager = CLLocationManager()
    var currentLocation = CLLocation()
    var restruntList : [RestrauntList] = []
    var restPickUpList : [RestrauntList] = []
    var isFromWalkIn = Bool()
    var restrauntId = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        locManager.requestWhenInUseAuthorization()
        if
            CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() ==  .authorizedAlways
        {
            currentLocation = locManager.location ?? CLLocation()
        }
        if currentLocation.coordinate.latitude != 0.0 || currentLocation.coordinate.longitude != 0.0{
            if isFromWalkIn {
                getRestrauntList()
            }else{
                getRestrauntPickUpList()
            }
        }
    }
    func getRestrauntList(){
        activityInc.startAnimating()
        let param : Parameters = [
            .service_id : "2",
            .latitude : "\(currentLocation.coordinate.latitude)",
            .longitude : "\(currentLocation.coordinate.longitude)",
        ]
        let request = APIManager.shared.createDataRequest(with: .post, params: param, api: .restaurants, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, restList in
            DispatchQueue.main.async {
                self.activityInc.stopAnimating()
                if isSuccess{
                    print(message)
                    //                print(restList)
                    let lst = restList["data"] as? [String : Any]
                    let listData = lst?["data"] as! [[String : Any]]
                    self.restruntList.removeAll()
                    print(listData)
                    for i in listData{
                        let id = i["id"]
                        let rest_name = i["rest_name"]
                        let rest_branch = i["rest_branch"]
                        let no_of_staff = i["no_of_staff"]
                        let country_id = i["country_id"]
                        let state_id = i["state_id"]
                        let city = i["city"]
                        let parent_id = i["parent_id"]
                        let block = i["block"]
                        let street = i["street"]
                        let shop_number = i["shop_number"]
                        let address = i["address"]
                        let pincode = i["pincode"]
                        let contact = i["contact"]
                        let status_id = i["status_id"]
                        let distance = i["distance"]
                        let endtime = i["endtime"]
                        let restImage = i["image"]
                        self.restruntList.append(RestrauntList(id: id as? Int ?? 0, rest_name: rest_name as? String ?? "", rest_branch: rest_branch as? String ?? "", no_of_staff: no_of_staff as? String ?? "", country_id: country_id as? String ?? "", state_id: state_id as? String ?? "", city: city as? String ?? "", parent_id: parent_id as? String ?? "", block: block as? String ?? "", street: street as? String ?? "", shop_number: shop_number as? String ?? "", address: address as? String ?? "", pincode: pincode as? String ?? "", contact: contact as? String ?? "", status_id: status_id as? Int ?? 0,distance: distance as! Double,endtime: endtime as? String ?? "", restImage: restImage as? String ?? ""))
                        print("restruntList48",self.restruntList)
                    }
                    self.pickUpTV.reloadData()
                }else{
                    print(message)
                    self.activityInc.stopAnimating()
                }
            }
        }
    }
    func getRestrauntPickUpList(){
        activityInc.startAnimating()
        let param : Parameters = [
            .service_id : "3",
            .latitude : "\(currentLocation.coordinate.latitude)",
            .longitude : "\(currentLocation.coordinate.longitude)",
        ]
        let request = APIManager.shared.createDataRequest(with: .post, params: param, api: .restaurants, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, restList in
            DispatchQueue.main.async {
                self.activityInc.stopAnimating()
                if isSuccess{
                    print(message)
                    //                print(restList)
                    let lst = restList["data"] as? [String : Any]
                    let listData = lst?["data"] as! [[String : Any]]
                    self.restPickUpList.removeAll()
                    print(listData)
                    for i in listData{
                        let id = i["id"]
                        let rest_name = i["rest_name"]
                        let rest_branch = i["rest_branch"]
                        let no_of_staff = i["no_of_staff"]
                        let country_id = i["country_id"]
                        let state_id = i["state_id"]
                        let city = i["city"]
                        let parent_id = i["parent_id"]
                        let block = i["block"]
                        let street = i["street"]
                        let shop_number = i["shop_number"]
                        let address = i["address"]
                        let pincode = i["pincode"]
                        let contact = i["contact"]
                        let status_id = i["status_id"]
                        let distance = i["distance"]
                        let endtime = i["endtime"]
                        let restImage = i["image"]
                        self.restPickUpList.append(RestrauntList(id: id as? Int ?? 0, rest_name: rest_name as? String ?? "", rest_branch: rest_branch as? String ?? "", no_of_staff: no_of_staff as? String ?? "", country_id: country_id as? String ?? "", state_id: state_id as? String ?? "", city: city as? String ?? "", parent_id: parent_id as? String ?? "", block: block as? String ?? "", street: street as? String ?? "", shop_number: shop_number as? String ?? "", address: address as? String ?? "", pincode: pincode as? String ?? "", contact: contact as? String ?? "", status_id: status_id as? Int ?? 0,distance: distance as! Double,endtime: endtime as? String ?? "", restImage: restImage as? String ?? ""))
                        print("restruntList123",self.restPickUpList)
                    }
                    self.pickUpTV.reloadData()
                }else{
                    print(message)
                    self.activityInc.stopAnimating()
                }
            }
        }
    }
}
extension PickUpVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFromWalkIn {
            return restruntList.count
        }else{
            return restPickUpList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalkInTVC") as! WalkInTVC
        if isFromWalkIn {
            cell.restName.text = restruntList[indexPath.row].rest_name
            let list = restruntList[indexPath.row]
            print(restruntList[indexPath.row].distance)
            let dist = String(format:"%.2f", list.distance)
            cell.lblDist.text = "\(dist)km"
            cell.lblTime.text = "Open Till \(list.endtime)"
        }else{
            cell.restName.text = restPickUpList[indexPath.row].rest_name
            let list = restPickUpList[indexPath.row]
            print(restPickUpList[indexPath.row].distance)
            let dist = String(format:"%.2f", list.distance)
            cell.lblDist.text = "\(dist)km"
            cell.lblTime.text = "Open Till \(list.endtime)"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFromWalkIn {
            let vc = storyboard?.instantiateViewController(withIdentifier: "WalkInVC") as! WalkInVC
            vc.restrauntId = restruntList[indexPath.row].id
            vc.restrauntName = restruntList[indexPath.row].rest_name
            navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "BookingTabScrVC") as! BookingTabScrVC
            vc.restrauntId = restPickUpList[indexPath.row].id
            vc.isFromPickUp = true
            vc.restName = restPickUpList[indexPath.row].rest_name
            vc.cartType = "book_table"
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
