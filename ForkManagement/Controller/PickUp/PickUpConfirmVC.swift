//
//  WalkInQueueVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 25/11/22.
//

import UIKit
import LGSideMenuController

class PickUpConfirmVC: UIViewController {

    @IBOutlet weak var lblTotalPrice: VCLanguageLabel!
    @IBOutlet weak var lblPhn: VCLanguageLabel!
    @IBOutlet weak var lblName: VCLanguageLabel!
    @IBOutlet weak var lblTotalTime: VCLanguageLabel!
    @IBOutlet weak var lblOrderId: VCLanguageLabel!
    @IBOutlet weak var backBtn: VCLanguageButton!
    @IBOutlet weak var btnInvoice: UIButton!
    @IBOutlet weak var btnGetDirection: UIButton!
    @IBOutlet weak var orderlistTV: UITableView!
    @IBOutlet weak var tabConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    var orderId = Int()
    var orderConfirmList = [OrdeConfirm]()
    override func viewDidLoad() {
        super.viewDidLoad()
        btnInvoice.rounded(cornerRadius: 10)
        btnGetDirection.rounded(cornerRadius: 10)
        bgView.rounded(cornerRadius: 20)
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            backBtn.setImage(UIImage(named: "image 187"), for: .normal)
        }else{
            backBtn.setImage(UIImage(named: "rightArrow"), for: .normal)
        }
        lblName.text = userInfo.name
        lblPhn.text = userInfo.contact
        lblOrderId.text = "\(orderId)"
        getOrderDetail()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async {
            self.tabConstraint.constant = self.orderlistTV.contentSize.height
            self.view.layoutIfNeeded()
        }
    }
    //MARK: - Action
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: - Api Call
    func getOrderDetail(){
        let params : Parameters = [
            .order_id : "\(orderId)",
        ]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .get_order_details, baseURL: .base, setToken: "Bearer " + userInfo.token)
        print(request)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, order in
            DispatchQueue.main.async { [self] in
                if isSuccess{
                    print(order)
                    let orderData = order["data"] as! [String : Any]
                    let order_item = orderData["order_item"] as! [[String : Any]]
                    let restaurant = orderData["restaurant"] as! [String : Any]
                    let restName = restaurant["rest_name"] as? String ?? ""
                    for i in order_item{
                        let order_item_details = i["order_item_details"] as! [String : Any]
                        let order_item_image = order_item_details["image"] as? String ?? ""
                        let order_item_price = order_item_details["price"] as? Int ?? 0
                        self.orderConfirmList.append(OrdeConfirm(order_item_name: restName, order_item_image: order_item_image, order_item_price: order_item_price))
                    }
                    self.orderlistTV.reloadData()
                    let total = orderData["total"] as? String ?? ""
                    lblTotalPrice.text = "Rs.\(total)"
                }else{
                    print(message)
                }
            }
        }
    }
}

extension PickUpConfirmVC : UITableViewDelegate , UITableViewDataSource {
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return orderConfirmList.count
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmOrderListTVC") as! ConfirmOrderListTVC
    cell.lblHotel.text = orderConfirmList[indexPath.row].order_item_name
    return cell
}

func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    self.viewWillLayoutSubviews()
}
}
