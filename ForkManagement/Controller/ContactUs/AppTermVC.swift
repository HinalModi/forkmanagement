//
//  AppTermVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 30/11/22.
//

import UIKit

class AppTermVC: UIViewController {

    @IBOutlet weak var txtAppterm: VCLanguageTextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        apiCall()
    }
    
    func apiCall(){
        let params : Parameters = [
            .type : "app-terms"
        ]
        var request : URLRequest!
        if userInfo == nil{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .cmspage, baseURL: .base, setToken: "")
        }else{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .cmspage, baseURL: .base, setToken: "Bearer " + userInfo.token)
        }
        print(request)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, appterm in
            DispatchQueue.main.async {                
                if isSuccess{
                    print(message)
                    print(appterm)
                    let data  = appterm["data"] as? [String:Any] ?? [String:Any]()
                    let apptemData = data["data"] as? [[String:Any]] ?? [[String:Any]]()
                    print(apptemData)
                    for i in apptemData {
                        let content = i["content"] as? String ?? ""
                        self.txtAppterm.text = content
                    }
                }else{
                    print(message)
                }
            }
        }
    }
}
