//
//  DashBoardContactVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 30/11/22.
//

import UIKit

class DashBoardContactVC: UIViewController {

    @IBOutlet weak var lineAppTermImg: UIImageView!
    @IBOutlet weak var lineContactImg: UIImageView!
    @IBOutlet weak var lblAppTerm: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var bgView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        remove(asChildViewController: appTerm)
        add(asChildViewController: contactUs)
    }
    lazy var contactUs: ContactUsVC = {
        let storyboard = UIStoryboard(name: "Order", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
        self.add(asChildViewController: viewController)
        return viewController
    }()
    lazy var appTerm: AppTermVC = {
        let storyboard = UIStoryboard(name: "Order", bundle: nil)
        var viewController = storyboard.instantiateViewController(withIdentifier: "AppTermVC") as! AppTermVC
        self.add(asChildViewController: viewController)
        return viewController
    }()
    @IBAction func btnOpenMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
    @IBAction func btnContact(_ sender: UIButton) {
        remove(asChildViewController: appTerm)
        add(asChildViewController: contactUs)
        lblAppTerm.textColor = .black
        lblContact.textColor = UIColor(named: "red")
        lineAppTermImg.isHidden = true
        lineContactImg.isHidden = false
    }
    @IBAction func btnAppTerm(_ sender: UIButton) {
        remove(asChildViewController: contactUs)
        add(asChildViewController: appTerm)
        lblAppTerm.textColor = UIColor(named: "red")
        lblContact.textColor = .black
        lineAppTermImg.isHidden = false
        lineContactImg.isHidden = true
    }
    func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
        bgView.addSubview(viewController.view)
        viewController.view.frame = bgView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParent: self)
    }
    
    func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
}
