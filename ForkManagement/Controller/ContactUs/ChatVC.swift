//
//  ChatVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 01/12/22.
//

import UIKit
import LGSideMenuController
class ChatVC: UIViewController {

    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var txtSend: UITextField!
    var msgArry = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        txtSend.setBorder(borderColor: UIColor(named: "btnGrey")! , borderWidth: 0.5)
        txtSend.rounded(cornerRadius: 5)
        txtSend.setLeftPaddingPoints(40)
        txtSend.setRightPaddingPoints(60)
    }
  
    @IBAction func btnOpenMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
    
    @IBAction func btnMsgSend(_ sender: UIButton) {
        msgArry.append(txtSend.text ?? "")
        txtSend.text = ""
        scrollToBottom()
        tblChat.reloadData()
    }
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.msgArry.count-1, section: 0)
            self.tblChat.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
}
extension ChatVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return msgArry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "SenderTVC") as! SenderTVC
        cell1.lblMsgSent.text = msgArry[indexPath.row]
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "ReceiverTVC") as! ReceiverTVC
        cell2.lblMsgRec.text = msgArry[indexPath.row]
            return cell1
    }
    
    
}
