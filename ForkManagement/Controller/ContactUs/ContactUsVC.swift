//
//  ContactUsVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 30/11/22.
//

import UIKit

class ContactUsVC: UIViewController {

    @IBOutlet weak var txtSubmit: UIButton!
    @IBOutlet weak var txtMsg: UITextView!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    
        txtSubmit.rounded(cornerRadius: 10)
        txtMsg.rounded(cornerRadius: 5)
        txtPhone.rounded(cornerRadius: 5)
        txtAddress.rounded(cornerRadius: 5)
        txtName.rounded(cornerRadius: 5)
        txtName.setLeftPaddingPoints(10)
        txtPhone.setLeftPaddingPoints(10)
        txtAddress.setLeftPaddingPoints(10)
    }

    func contactApi(){
        let params : Parameters = [
            .name : txtName.text ?? "",
            .email : txtAddress.text ?? "",
            .phone : txtPhone.text ?? "",
            .message : txtMsg.text ?? "",
        ]
        var request : URLRequest!
        if userInfo == nil{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .contact, baseURL: .base, setToken: "")
        }else{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .contact, baseURL: .base, setToken: "Bearer " + userInfo.token)
        }
//        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .contact, baseURL: .base, setToken: "Bearer " + userInfo.token)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, restList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    self.view.makeToast(message)
                }else{
                    print(message)
                }
            }
        }
    }
    
    @IBAction func btnSubmitInfo(_ sender: UIButton) {
        contactApi()
    }
}
