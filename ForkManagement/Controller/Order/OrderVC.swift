//
//  OrderVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 28/11/22.
//

import UIKit
import LGSideMenuController
import HCSStarRatingView

class OrderVC: UIViewController {

    @IBOutlet weak var txtFeedBack: UITextView!
    @IBOutlet weak var expRatingView: HCSStarRatingView!
    @IBOutlet weak var waiterRatingView: HCSStarRatingView!
    @IBOutlet weak var foodRatingView: HCSStarRatingView!
    @IBOutlet weak var ratingBgView: UIImageView!
    @IBOutlet weak var btnSubmit: VCLanguageButton!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var activityInc: UIActivityIndicatorView!
    @IBOutlet weak var orderTableTV: UITableView!
    @IBOutlet weak var imgSelectPickUp: UIImageView!
    @IBOutlet weak var imgSelectWalkIn: UIImageView!
    @IBOutlet weak var imgSelectBookTab: UIImageView!
    @IBOutlet weak var lblPickUp: UILabel!
    @IBOutlet weak var lblWalkIn: UILabel!
    @IBOutlet weak var lblBookTab: UILabel!
    var orderList = [OrderList]()
    var orderReview = [Review]()
    var reviewData = [String : Any]()
    var restaurant_id = Int()
    var order_id = Int()
    var waiter = Int()
    var food = Int()
    var experience = Int()
    var reviewId = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        getOrders()
        ratingView.rounded(cornerRadius: 20)
        btnSubmit.rounded(cornerRadius: 10)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

    }
    //MARK: - APi Call
    func getOrders(){
        activityInc.startAnimating()
        let params : Parameters = [:]
        var request : URLRequest!
        if userInfo == nil{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .get_order, baseURL: .base, setToken: "")
        }else{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .get_order, baseURL: .base, setToken: "Bearer " + userInfo.token)
        }
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, orderList in
            DispatchQueue.main.async {
                self.activityInc.stopAnimating()
                if isSuccess{
                    print(message)
                    let orders = orderList["data"] as? [[String  : Any]] ?? [[String  : Any]]()
                    print(orders)
                    self.orderList.removeAll()
                    for i in orders{
                        let orderid = i["id"]
                        let userId = i["user_id"]
                        let totalOrder = i["total"]
                        let payment_status = i["payment_status"]
                        let order_status = i["order_status"]
                        let restraunt = i["restaurant"]
                        let review = i["review"]
                        let timing = i["timing"] as? [String:Any]
                        let opendays = timing?["opendays"] as? String ?? ""
                        let orderDate = i["created_at"] as? String ?? ""
                        print(review)
                        self.orderList.append(OrderList(orderid: orderid as? Int ?? 0, review: review as? [String : Any] ?? [String : Any](), userId: userId as? Int ?? 0, totalOrder: totalOrder as? String ?? "", payment_status: payment_status as? String ?? "", order_status: order_status as? String ?? "", timing: timing ?? [String:Any]() , restaurant: restraunt as? [String : Any] ?? [String : Any](), opendays: opendays, orderDate: orderDate ))
                    }
                    self.orderTableTV.reloadData()
                }else{
                    print(message)
                }
            }
        }
    }
    func addReview(){
        self.waiter = Int(waiterRatingView.value)
        self.food = Int(foodRatingView.value)
        self.experience = Int(expRatingView.value)
        activityInc.startAnimating()
        let params : Parameters = [
            .restaurant_id : "\(restaurant_id)",
            .order_id : "\(order_id)",
            .waiter : "\(waiter)",
            .food : "\(food)",
            .experience : "\(experience)",
            .id : reviewId,
            .review : "\(txtFeedBack.text ?? "")"
        ]
        print(params)
        var request : URLRequest!
        if userInfo == nil{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .add_review, baseURL: .base, setToken: "")
        }else{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .add_review, baseURL: .base, setToken: "Bearer " + userInfo.token)
        }
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, review in
            DispatchQueue.main.async {
                if isSuccess{
                    print(review)
                    print(message)
                    self.getOrders()
                    UIView.animate(withDuration: 0.1) {
                        self.ratingBgView.isHidden = true
                        self.ratingView.isHidden = true
                    }
                    self.activityInc.stopAnimating()
                }else{
                    self.activityInc.stopAnimating()
                    print(message)
                }
            }
        }
    }
    //MARK: - Actions
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func btnCloseView(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1) {
            self.ratingBgView.isHidden = true
            self.ratingView.isHidden = true
        }
    }
    @IBAction func btnSubmitRating(_ sender: UIButton) {
        addReview()
    }
}
extension OrderVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTVC") as! OrderTVC
        let list = orderList[indexPath.row]
        cell.lblPrice.text = list.totalOrder
        cell.lblOrderId.text = "\(list.orderid)"
        let restName = list.restaurant["rest_name"] as? String ?? ""
        cell.lblHotelName.text = restName
        cell.foodRateView.value = list.review["food"] as? CGFloat ?? 0
        cell.expRateView.value = list.review["experience"] as? CGFloat ?? 0
        cell.waiterRateView.value = list.review["waiter"] as? CGFloat ?? 0
    
        if list.orderDate != ""{
            let newdate = GlobalFunction.convertDateString(dateString: list.orderDate , fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ", toFormat: "hh:mm a")

            let dateformatter = DateFormatter()
            dateformatter.dateFormat = "hh:mm a"
            let currentTime = dateformatter.string(from: Date())
            print(currentTime)
            print(newdate)
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            let date1 = formatter.date(from: currentTime)!
            let date2 = formatter.date(from: newdate)!
            let elapsedTime = date2.timeIntervalSince(date1)
            
            let hours = floor(elapsedTime / 60 / 60)
            let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)
            print("\(Int(hours)) hr and \(Int(minutes)) min")
            if currentTime < newdate{                
                cell.lblRemMin.text = "\(Int(minutes))"
                cell.lblRemHour.text = "\(Int(hours))"
            }
        }
        cell.doRate = {
            self.reviewId = "\(list.review["id"] ?? "")"
            UIView.animate(withDuration: 0.1) {
                self.ratingBgView.isHidden = false
                self.ratingView.isHidden = false
            }
            let restId = list.restaurant["id"] as? Int ?? 0
            self.restaurant_id = restId
            self.order_id = list.orderid
           
        }
        let data = list.opendays.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [[String : Any]]
            {
                jsonArray.map { time in
                    let timeslot = time["timeslot"] as? String ?? ""
                    let day = time["day"] as? String ?? ""
                    let close = time["close"]
                    let date = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "EEEE"
                    let dayOfTheWeekString = dateFormatter.string(from: date)
                    if day == dayOfTheWeekString{
                        let dataSlot = timeslot.data(using: .utf8)!
                        do {
                            if let jsonArray = try JSONSerialization.jsonObject(with: dataSlot, options : .allowFragments) as? [[String : Any]]
                            {
                                jsonArray.map { slottime in
                                    let end_time = slottime["end_time"] as? String ?? ""
                                    let start_time = slottime["start_time"]
                                    cell.lblOpenTill.text = "Open Till \(end_time)"
                                }
                            }
                        }catch{
                        }
                    }
                }
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(error)
        }
        return cell
    }
    
    @IBAction func btnBookTable(_ sender: UIButton) {
        lblPickUp.textColor = .black
        lblWalkIn.textColor = .black
        lblBookTab.textColor = UIColor(named: "red")
        imgSelectBookTab.isHidden = false
        imgSelectWalkIn.isHidden = true
        imgSelectPickUp.isHidden = true
    }
    @IBAction func btnPickUp(_ sender: UIButton) {
        lblPickUp.textColor = UIColor(named: "red")
        lblWalkIn.textColor = .black
        lblBookTab.textColor = .black
        imgSelectBookTab.isHidden = true
        imgSelectWalkIn.isHidden = true
        imgSelectPickUp.isHidden = false
    }
    
    @IBAction func btnWalkIn(_ sender: UIButton) {
        lblPickUp.textColor = .black
        lblWalkIn.textColor = UIColor(named: "red")
        lblBookTab.textColor = .black
        imgSelectBookTab.isHidden = true
        imgSelectWalkIn.isHidden = false
        imgSelectPickUp.isHidden = true
    }
}
