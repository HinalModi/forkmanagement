//
//  BookingTabScrVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 23/11/22.
//

import UIKit
import LGSideMenuController
import SDWebImage
class BookingTabScrVC: UIViewController {

    @IBOutlet weak var lblRemMins: UILabel!
    @IBOutlet weak var lblRemhour: UILabel!
    @IBOutlet weak var reservedView: UIView!
    @IBOutlet weak var activityInc: UIActivityIndicatorView!
    @IBOutlet weak var lblDoorType: VCLanguageLabel!
    @IBOutlet weak var lblBookingPrice: VCLanguageLabel!
    @IBOutlet weak var lblPhone: VCLanguageLabel!
    @IBOutlet weak var lblTime: VCLanguageLabel!
    @IBOutlet weak var lblType: VCLanguageLabel!
    @IBOutlet weak var lblName: VCLanguageLabel!
    @IBOutlet weak var lblHotel: VCLanguageLabel!
    @IBOutlet weak var toppingHeightCons: NSLayoutConstraint!
    @IBOutlet weak var toppingTV: UITableView!
    @IBOutlet weak var lblPrice: VCLanguageLabel!
    @IBOutlet weak var lblReserveTime: VCLanguageLabel!
    @IBOutlet weak var lblSeats: VCLanguageLabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblDist: UILabel!
    @IBOutlet weak var lblWalkInTime: UILabel!
    @IBOutlet weak var lblOpenTill: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var subCatTV: UITableView!
    @IBOutlet weak var categoryCV: UICollectionView!
    @IBOutlet weak var btnMenuBack: UIButton!
    @IBOutlet weak var txtQty: UITextField!
    @IBOutlet weak var type3OutImage: UIImageView!
    @IBOutlet weak var type3BgImage: UIImageView!
    @IBOutlet weak var type1OutImage: UIImageView!
    @IBOutlet weak var type2BgImage: UIImageView!
    @IBOutlet weak var type2OutImage: UIImageView!
    @IBOutlet weak var type1BgImage: UIImageView!
    @IBOutlet weak var btnPayfoodTab: UIButton!
    @IBOutlet weak var foodCartView: UIView!
    @IBOutlet weak var foodCartTV: UITableView!
    @IBOutlet weak var viewCartBg: UIView!
    @IBOutlet weak var cartFoodImage: UIImageView!
    @IBOutlet weak var imageHotel: UIImageView!
    @IBOutlet weak var viewHotelInfo: UIView!
    @IBOutlet weak var foodStackView: UIStackView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var typeTopView: UIView!
    @IBOutlet weak var btnDecr: UIButton!
    @IBOutlet weak var btnIncr: UIButton!
    @IBOutlet weak var btnReserve: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var alertBgView: UIImageView!
    @IBOutlet weak var txtsearchFood: UITextField!
    @IBOutlet weak var btnViewCart: UIButton!
    @IBOutlet weak var tabHeight: NSLayoutConstraint!
    var isselected = Bool()
    var val = 0
    var isfromBooking = Bool()
    var restName = String()
    var dist = String()
    var openTill = String()
    var restrauntId = Int()
    var itemNameList = [FoodCategory]()
    var foodList = [FoodList]()
    var categoryId = String()
    var itemList = [ItemList]()
    var item_Id = String()
    var booking_table_Id = UserDefaults.standard.string(forKey: "BookingTableId")
    var itemExt_Id = [String]()
    var paramValue = [String]()
    var cartItemList = [Cart_item]()
    var time = String()
    var doorType = String()
    var door = String()
    var tablePrice = String()
    var totalPrice = String()
    var noOfPerson = Int()
    var isFromPickUp = false
    var cartType = ""
    var bookingDate = UserDefaults.standard.value(forKey: "BookingTableDate")
    override func viewDidLoad() {
        super.viewDidLoad()

        txtsearchFood.setBorder(borderColor: UIColor(named: "darkGrey") ?? .lightGray ,borderWidth: 0.2 )
        viewHotelInfo.setBorder(borderColor: UIColor(named: "darkGrey") ?? .lightGray ,borderWidth: 0.2 )
        imageHotel.rounded(cornerRadius: 5)
        viewCartBg.rounded(cornerRadius: 5)
        cartFoodImage.rounded(cornerRadius: 5)
        viewHotelInfo.rounded(cornerRadius: 5)
        btnViewCart.rounded(cornerRadius: 10)
        btnPayfoodTab.rounded(cornerRadius: 10)
        txtsearchFood.rounded(cornerRadius: 5)
        btnAdd.rounded(cornerRadius: 10)
        btnReserve.rounded(cornerRadius: 10)
        txtsearchFood.setLeftPaddingPoints(20)
        lblOpenTill.text = "Open Till \(openTill)"
        lblDist.text = "\(dist)"
        lblProductName.text = restName
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            btnMenuBack.setImage(UIImage(named: "image 187"), for: .normal)
            btnDecr.roundedCorners(corners: [.topLeft,.bottomLeft], radius: 5)
            btnIncr.roundedCorners(corners: [.topRight,.bottomRight], radius: 5)
        }else{
            btnMenuBack.setImage(UIImage(named: "rightArrow"), for: .normal)
            btnDecr.roundedCorners(corners: [.topRight,.bottomRight], radius: 5)
            btnIncr.roundedCorners(corners: [.topLeft,.bottomLeft], radius: 5)
        }
        
        typeTopView.rounded(cornerRadius: 20)
        foodCartView.rounded(cornerRadius: 20)
        if isfromBooking || isFromPickUp{
            btnMenuBack.setImage(UIImage(named: "image 187"), for: .normal)
        }else{
            btnMenuBack.setImage(UIImage(named: "Menu"), for: .normal)
        }
        if isFromPickUp{
            reservedView.isHidden = true
        }else{
            reservedView.isHidden = false
        }
        getFoodApiCall()
//        getSubCategory(categoryId: "")
        lblHotel.text = restName
        if userInfo != nil{
            lblName.text = userInfo.name
            lblPhone.text = userInfo.contact
        }
        lblTime.text = time
        lblDoorType.text = "\(doorType):"
        lblType.text = door
        lblBookingPrice.text = tablePrice
        lblPrice.text = tablePrice
        lblSeats.text = "\(noOfPerson) Seats"
        txtsearchFood.addTarget(self, action: #selector(self.textIsChanging(textField:)), for: .editingChanged)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setTableHeight(tableview: subCatTV,heightConstraint: tabHeight)
        setTableHeight(tableview: toppingTV, heightConstraint: toppingHeightCons)
        if bookingDate as? String ?? "" != ""{
            let newdate = GlobalFunction.convertDateString(dateString: bookingDate as? String ?? "", fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "hh:mm a")
            
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = "hh:mm a"
            let currentTime = dateformatter.string(from: Date())
            print(currentTime)
            print(newdate)
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            let date1 = formatter.date(from: currentTime)!
            let date2 = formatter.date(from: newdate)!
            let elapsedTime = date2.timeIntervalSince(date1)
            let hours = floor(elapsedTime / 60 / 60)
            let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)
            print("\(Int(hours)) hr and \(Int(minutes)) min")
            lblRemhour.text = "\(Int(hours))"
            lblRemMins.text = "\(Int(minutes))"
        }
    }
    func setTableHeight(tableview : UITableView , heightConstraint : NSLayoutConstraint){
        DispatchQueue.main.async {
            heightConstraint.constant = tableview.contentSize.height
            self.view.layoutIfNeeded()
            heightConstraint.constant = tableview.contentSize.height
        }
    }
//MARK: - Api CAll
    func getFoodApiCall(){
        activityInc.startAnimating()
        let param : Parameters = [
            .branch_id : "\(restrauntId)",
        ]
        let request = APIManager.shared.createDataRequest(with: .post, params: param, api: .food_list, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, foodList in
            DispatchQueue.main.async {
                if isSuccess{
                    self.activityInc.stopAnimating()
                    let lst = foodList["data"] as? [String : Any]
                    let listData = lst?["data"] as! [[String : Any]]
                    self.itemNameList.removeAll()
                    print("listData119",listData)
                    for i in listData{
                        let name = i["name"]
                        let id = i["id"]
                        let branch_id = i["branch_id"]
                        let slug = i["slug"]
                        let status_id = i["status_id"]
                        let defaultVal = i["default"]
                        self.getSubCategory(categoryId: "\(id as? Int ?? 0)")
                        self.itemNameList.append(FoodCategory(name: name as? String ?? "", id: id as? Int ?? 0, branch_id: branch_id as? String ?? "", slug: slug as? String ?? "", status_id: status_id as? Int ?? 0, defaultVal: defaultVal as? Int ?? 0))
                        print("itemNameList127",self.itemNameList)
                    }
                    self.categoryCV.reloadData()
                }else{
                    print(message)
                    self.activityInc.stopAnimating()
                }
            }
        }
    }
    func getSubCategory(categoryId : String){
        let param : Parameters = [
            .category_id : categoryId ,
        ]
        let request = APIManager.shared.createDataRequest(with: .post, params: param, api: .cat_item_list, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, foodList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    let listData = foodList["data"] as! [[String : Any]]
                    print(listData)
                    self.foodList.removeAll()
                    for i in listData{
                        let id = i["id"]
                        let category_id = i["category_id"]
                        let name = i["name"]
                        let price = i["price"]
                        let image = i["image"]
                        self.foodList.append(FoodList(name: name as? String ?? "", price: price as? Int ?? 0, image: image as? String ?? "", id: id as? Int ?? 0, category_id: category_id as? Int ?? 0))
                        print("foodList154",self.itemNameList)
                    }
                    self.setTableHeight(tableview: self.subCatTV,heightConstraint: self.tabHeight)
                    self.subCatTV.reloadData()
//                    self.categoryCV.reloadData()
                }else{
                    print(message)
                    self.setTableHeight(tableview: self.subCatTV,heightConstraint: self.tabHeight)
                }
            }
        }
    }
    @objc func textIsChanging(textField: UITextField) {
        getRestrauntList(categoryId: categoryId, searchTxt: textField.text ?? "")
    }
    func getRestrauntList(categoryId : String,searchTxt : String){
        let param : Parameters = [
            .search_item : searchTxt,
            .category_id : categoryId,
        ]
        print(param)
        let request = APIManager.shared.createDataRequest(with: .post, params: param, api: .cat_item_list, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, foodList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    let listData = foodList["data"] as! [[String : Any]]
                    print(listData)
                    self.foodList.removeAll()
                    for i in listData{
                        let id = i["id"]
                        let category_id = i["category_id"]
                        let name = i["name"]
                        let price = i["price"]
                        let image = i["image"]
                        self.foodList.append(FoodList(name: name as? String ?? "", price: price as? Int ?? 0, image: image as? String ?? "", id: id as? Int ?? 0, category_id: category_id as? Int ?? 0))
                        print("foodList154",self.itemNameList)
                    }
                    self.setTableHeight(tableview: self.subCatTV,heightConstraint: self.tabHeight)
                    self.subCatTV.reloadData()
//                    self.categoryCV.reloadData()
                }else{
                    print(message)
                }
            }
        }
    }
    func getItemList(itemId : String){
        let param : Parameters = [
            .item_id : itemId
        ]
        print(param)
        let request = APIManager.shared.createDataRequest(with: .post, params: param, api: .item_list, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, foodList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    let foodData = foodList["data"]  as! [String : Any]
                    let extra = foodData["extra"] as! [[String : Any]]
                    self.itemList.removeAll()
                    for i in extra{
                        let id = i["id"]
                        let item_Id = i["item_id"]
                        let name = i["name"]
                        let price = i["price"]
                        let description = i["description"]
                        let status_id = i["status_id"]
                        let image = i["image"]
                        self.itemList.append(ItemList(id: id as? Int ?? 0, item_Id: item_Id as? Int ?? 0, name: name as? String ?? "", price: price as? String ?? "", description: description as? String ?? "", status_id: status_id as? Int ?? 0, image: image as? String ?? ""))
                    }
                    self.setTableHeight(tableview: self.toppingTV, heightConstraint: self.toppingHeightCons)
                    self.toppingTV.reloadData()
                }else{
                    print(message)
                }
            }
        }
    }
    func addItemInCart(){
        let itemExtArray = (itemExt_Id.map{String($0)}.joined(separator: ","))
        let param : Parameters = [
            .item_id : item_Id,
            .qty : "\(txtQty.text ?? "")",
            .booking_table_id : "\(booking_table_Id ?? "")",
            .item_extra : itemExtArray,
            .identifier : UserDefaults.standard.string(forKey: "identifier") ?? "",
            .type : cartType
        ]
        print(param)
        var request : URLRequest!
        if userInfo == nil {
            request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .add_item_cart, baseURL: .base, setToken: "")
        }else{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: param, api: .add_item_cart, baseURL: .base, setToken: "Bearer " + userInfo.token)
        }
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, foodList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    self.getCartDetails()
                    self.view.makeToast(message)
                }else{
                    print(message)
                    
                }
            }
        }
    }
    
    func getCartDetails(){
        let params : Parameters = [:]
        var request : URLRequest!
        if userInfo == nil {
            request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .get_cart_details, baseURL: .base, setToken: "")
        }else{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .get_cart_details, baseURL: .base, setToken: "Bearer " + userInfo.token)
        }
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, cartList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    print(cartList)
                    let cartData = cartList["data"] as! [String : Any]
                    let cart_item = cartData["cart_item"] as! [[String : Any]]
                    let total_price = cartData["total"]
                    self.totalPrice = total_price as? String ?? ""
                    print(self.totalPrice)
                    self.cartItemList.removeAll()
                    for i in cart_item{
                        let cart_Item_Detail = i["cart_item_details"] as! [String : Any]
                        let cart_id = i["id"]
                        let cart_item_id = cart_Item_Detail["id"]
                        let cart_item_name = cart_Item_Detail["name"]
                        let cart_item_price = cart_Item_Detail["price"]
                        let cart_item_image = cart_Item_Detail["image"]
                        let category_Id = cart_Item_Detail["category_id"]
                        let qty = i["qty"]
                        let ext_item_Detail = i["extra_item_details"] as? [[String : Any]] ?? [[String : Any]]()
                        self.cartItemList.append(Cart_item(cart_item_id: cart_item_id as? Int ?? 0, category_id: category_Id as? Int ?? 0, cart_item_name: cart_item_name as? String ?? "", cart_item_price: cart_item_price as? Int ?? 0, cart_item_image: cart_item_image as? String ?? "", extra_item_details: ext_item_Detail, qty: qty as? Int ?? 0, cart_Id: cart_id as? Int ?? 0))
                    }
                    self.foodCartTV.reloadData()
                    self.alertBgView.isHidden = false
                    self.typeTopView.isHidden = true
                    self.btnClose.isHidden = false
                    self.foodCartView.isHidden = false
                    self.foodStackView.isHidden = false

                }else{
                    print(message)
                    self.view.makeToast("No item found in the cart!")
                }
            }
        }
    }
    func updateValue(cart_item_id : String , qty : String){
        let params : Parameters = [
            .cart_item_id : cart_item_id,
            .qty : qty
        ]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .update_qty, baseURL: .base, setToken: "Bearer " + userInfo.token)//createDataRequest(with: .post, params: param, api: .update_qty, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, cartList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                }else{
                    print(message)
                }
            }
        }
    }
    func deleteCartitem(cart_item_id : String){
        let params : Parameters = [
            .cart_item_id : cart_item_id,
        ]
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .remove_item_cart, baseURL: .base, setToken: "Bearer " + userInfo.token)//createDataRequest(with: .post, params: param, api: .remove_item_cart, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, cartList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                }else{
                    print(message)
                }
            }
        }
    }
//MARK: - Actions
    @IBAction func btnBack(_ sender: UIButton) {
        if isfromBooking || isFromPickUp{
            navigationController?.popViewController(animated: true)
        }else{
            sideMenuController?.showLeftView()
            
        }
    }
    @IBAction func btnAddToCart(_ sender: UIButton) {
        addItemInCart()
    }
    @IBAction func btnCloseFoodView(_ sender: UIButton) {
        self.alertBgView.isHidden = true
        self.typeTopView.isHidden = true
        self.btnClose.isHidden = true
        self.foodCartView.isHidden = true
        self.foodStackView.isHidden = true
    }
    @IBAction func btnToViewCart(_ sender: UIButton) {
        
        getCartDetails()
    }
    
    @IBAction func btnToPayForFoodTable(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SummaryScrVC") as! SummaryScrVC
        vc.hotelName = lblHotel.text ?? ""
        vc.doorNo = lblType.text ?? ""
        vc.doorType = lblDoorType.text ?? ""
        vc.time = lblTime.text ?? ""
        vc.tablePrice = tablePrice
        vc.totalPrice = totalPrice
        vc.restrauntId = restrauntId
        
        if isFromPickUp{
            vc.isFromPickUp = isFromPickUp
            vc.orderType = "pickup"
        }else{
            vc.orderType = "book_table"
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func setTypeSelection(type1OutImage:Bool,type2OutImage:Bool,type3OutImage:Bool){
        UIView.animate(withDuration: 0.1) {
            self.type1OutImage.isHidden = type1OutImage
            self.type2OutImage.isHidden = type2OutImage
            self.type3OutImage.isHidden = type3OutImage
        }
    }
    
    @IBAction func btnReserveFood(_ sender: UIButton) {
        getCartDetails()
    }
    @IBAction func btnSelType1(_ sender: UIButton) {
        setTypeSelection(type1OutImage: false, type2OutImage: true, type3OutImage: true)
    }
    
    @IBAction func btnSelType2(_ sender: UIButton) {
        setTypeSelection(type1OutImage: true, type2OutImage: false, type3OutImage: true)
    }
    
    @IBAction func btnSelType3(_ sender: UIButton) {
        setTypeSelection(type1OutImage: true, type2OutImage: true, type3OutImage: false)
    }
    
    @IBAction func btnDecrement(_ sender: UIButton) {
        val -= 1
        if val > 0{
            txtQty.text = "\(val)"
        }
    }
    
    @IBAction func btnIncrement(_ sender: UIButton) {
        if val < 0{            
            val = 1
        }
        val += 1
        if val > 0{
            txtQty.text = "\(val)"
        }
        
    }
    var showImageIndex : Int?
}
extension BookingTabScrVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("foodList",foodList.count)
        if tableView == toppingTV{
            return itemList.count
        }else if tableView == foodCartTV{
            return cartItemList.count
        }else{
            return foodList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == foodCartTV{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectTabFoodInCartTVC") as! SelectTabFoodInCartTVC
            let list = cartItemList[indexPath.row]
            cell.lblfoodName.text = list.cart_item_name
            cell.lblFoodPrice.text = "Rs.\(list.cart_item_price)"
            cell.foodImage.sd_setImage(with: URL(string: list.cart_item_image), placeholderImage: UIImage(named: "default_1"))
            cell.txtQty.text = "\(list.qty)"
            cell.lblFoodPrice.text = "Rs.\(list.cart_item_price)"
            var extName = [String]()
            for j in list.extra_item_details {
                let ext_Name = j["name"]
                let price = j["price"]
                extName.append(ext_Name as? String ?? "")
                cell.lblFoodToppings.text = extName.joined(separator: ",")
            }
            cell.delRow = {
                tableView.beginUpdates()
                self.deleteCartitem(cart_item_id: "\(list.cart_Id)")
                tableView.deleteRows(at: [indexPath], with: .automatic)
                self.cartItemList.remove(at: indexPath.row)
                tableView.endUpdates()
            }
            cell.incrVal = {
                self.updateValue(cart_item_id: "\(list.cart_Id)", qty: cell.txtQty.text ?? "")
            }
            cell.decrVal = {
                if cell.txtQty.text != "0"{
                    self.updateValue(cart_item_id: "\(list.cart_Id)", qty: cell.txtQty.text ?? "")
                }
            }
            return cell
        }else if tableView == toppingTV{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ToppingTVC") as! ToppingTVC
            cell.lblItemName.text = itemList[indexPath.row].name
            cell.lblPrice.text = "Rs.\(itemList[indexPath.row].price)"
            let ext = "item_extra"
            cell.selExt = {
                if cell.val == 0 {
                    cell.inImg.isHidden = false
                    self.item_Id = "\(self.itemList[indexPath.row].item_Id)"
                    self.itemExt_Id.append("\(self.itemList[indexPath.row].id)")
                    self.paramValue.append(ext)
                    cell.val = 1
                }else{
                    cell.inImg.isHidden = true
                    cell.val = 0
                    if self.itemExt_Id.indices.contains(indexPath.row) {
                        self.itemExt_Id.remove(at: indexPath.row)
                    }
                    if self.paramValue.indices.contains(indexPath.row) {
                        self.paramValue.remove(at: indexPath.row)
                    }
                }
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FoodListTVC") as! FoodListTVC
            cell.lblItemName.text = foodList[indexPath.row].name
            cell.lblPrice.text = "Rs.\(foodList[indexPath.row].price)"
            cell.foodImage.sd_setImage(with: URL(string: "https://staging.greatly-done.com/fork-mgmt/fork-management/api/v1/customer/" + foodList[indexPath.row].image) , placeholderImage: UIImage(named: "default_1"))
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == subCatTV{
            getItemList(itemId: "\(foodList[indexPath.row].id)")
            item_Id = "\(foodList[indexPath.row].id)"
            self.alertBgView.isHidden = false
            self.typeTopView.isHidden = false
            self.btnClose.isHidden = false
            self.foodStackView.isHidden = false
            self.foodCartView.isHidden = true
        }else if tableView == toppingTV{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ToppingTVC") as! ToppingTVC
            cell.inImg.isHidden = true
            cell.val = 1
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
   
}
extension BookingTabScrVC : UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("itemNameList",foodList.count)
        return itemNameList.count
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodTypeCVC", for: indexPath) as! FoodTypeCVC

        cell.lblType.text = itemNameList[indexPath.row].name
        cell.lblType.sizeToFit()
        if self.itemNameList[indexPath.row].defaultVal == 1 {
            cell.dashImg.isHidden = false
        }
     
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: itemNameList[indexPath.item].name.size(withAttributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17)]).width + 25, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        isselected = true
        getSubCategory(categoryId: "\(itemNameList[indexPath.row].id)")
        if "\(itemNameList[indexPath.row].id)" == ""{
            categoryId = "1"
        }else{
            categoryId = "\(itemNameList[indexPath.row].id)"
        }
        showImageIndex = indexPath.row
        
        collectionView.reloadData()
    }
   
}
extension String {
    var stripped: String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-=[]().!_")
        return self.filter {okayChars.contains($0) }
    }
}
