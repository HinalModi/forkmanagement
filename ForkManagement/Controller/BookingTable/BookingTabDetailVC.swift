//
//  BookingTabDetailVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 17/11/22.
//

import UIKit
import DropDown

class BookingTabDetailVC: UIViewController {
    
    @IBOutlet weak var hotelImage: UIImageView!
    @IBOutlet weak var lblTabPrice: VCLanguageLabel!
    @IBOutlet weak var lblSelTabDoor: VCLanguageLabel!
    @IBOutlet weak var lblFloorName: VCLanguageLabel!
    @IBOutlet weak var lblNoOfSeats: VCLanguageLabel!
    @IBOutlet weak var lblPhone: VCLanguageLabel!
    @IBOutlet weak var lblCartTime: VCLanguageLabel!
    @IBOutlet weak var lblDoorType: VCLanguageLabel!
    @IBOutlet weak var lblName: VCLanguageLabel!
    @IBOutlet weak var lblHotel: VCLanguageLabel!
    @IBOutlet weak var txtOccasion: VCLanguageTextView!
    @IBOutlet weak var txtDressCode: VCLanguageTextView!
    @IBOutlet weak var txtRules: VCLanguageTextView!
    @IBOutlet weak var lblPrice: VCLanguageLabel!
    @IBOutlet weak var txtDescription: VCLanguageTextView!
    @IBOutlet weak var lblNoTab: VCLanguageLabel!
    @IBOutlet weak var lblBranchName: VCLanguageLabel!
    @IBOutlet weak var lblTime: VCLanguageLabel!
    @IBOutlet weak var lblDate: VCLanguageLabel!
    @IBOutlet weak var lblPerson: VCLanguageLabel!
    @IBOutlet weak var lblNum: VCLanguageLabel!
    @IBOutlet weak var dropDownWhr: DropDown!
    @IBOutlet weak var dropNum: DropDown!
    @IBOutlet weak var lblWhr: VCLanguageLabel!
    @IBOutlet weak var lblDist: UILabel!
    @IBOutlet weak var lblOpenTill: UILabel!
    @IBOutlet weak var lblRestName: UILabel!
    @IBOutlet weak var dropBranchName: DropDown!
    @IBOutlet weak var dropNoPerson: DropDown!
    @IBOutlet weak var dropBranch: DropDown!
    @IBOutlet weak var backBtn: VCLanguageButton!
    @IBOutlet weak var btnSelectFood: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnConfirmPay: UIButton!
    @IBOutlet weak var stackBookCrt: UIStackView!
    @IBOutlet weak var bgView: UIImageView!
    @IBOutlet weak var itemCartView: UIView!
    @IBOutlet weak var bookTabView: UIView!
    @IBOutlet weak var btnSelNext: UIButton!
    @IBOutlet weak var floorView: UIView!
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var btnFloor: UIButton!
    @IBOutlet weak var btnList: UIButton!
    @IBOutlet weak var selectTabCV: UICollectionView!
    @IBOutlet weak var btnPayConfirm: UIButton!
    var restName = String()
    var openTill = String()
    var dist = String()
    var restrauntId = Int()
    var restCollList = [RestDetail]()
    var childRestList = [ChildRest]()
    var selectList = Bool()
    var whrList = ["Inside" , "Outside"]
    var numList = ["0.1" , "0.2", "0.3", "0.4", "0.5", "0.6", "0.7"]
    var personList = ["Person 1","Person 2","Person 3","Person 4","Person 5","Person 6","Person 7","Person 8"]
    var childRest = [String]()
    let dropDown = DropDown()
    var tabId = String()
    var noOfPerson = Int()
    var floorList = [String]()
    var identifierName = UserDefaults.standard.string(forKey: "identifier")
    var restImage = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        btnPayConfirm.rounded(cornerRadius: 10)
        hotelImage.sd_setImage(with: URL(string: restImage), placeholderImage: UIImage(named: "cutlery"))
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            backBtn.setImage(UIImage(named: "image 187"), for: .normal)
            btnFloor.roundedCorners(corners: [.topLeft , .bottomLeft], radius: 20)
            btnList.roundedCorners(corners: [.topRight , .bottomRight], radius: 20)
        }else{
            backBtn.setImage(UIImage(named: "rightArrow"), for: .normal)
            btnFloor.roundedCorners(corners: [.topRight , .bottomRight], radius: 20)
            btnList.roundedCorners(corners: [.topLeft , .bottomLeft], radius: 20)
        }
        lblRestName.text = restName
        lblDist.text = dist
        lblOpenTill.text = openTill
        if userInfo != nil{
            lblName.text = userInfo.name
            lblPhone.text = userInfo.contact
        }
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let someDateTime = formatter.string(from: date)
        lblDate.text = someDateTime
        let time = Date()
        formatter.dateFormat = "HH:mm:ss"
        lblTime.text = formatter.string(from: time)
        btnSelNext.rounded(cornerRadius: 10)
        bookTabView.rounded(cornerRadius: 20)
        itemCartView.rounded(cornerRadius: 20)
        btnConfirmPay.rounded(cornerRadius: 10)
        btnCancel.rounded(cornerRadius: 10)
        btnSelectFood.rounded(cornerRadius: 10)
        getRestrauntDetail()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    //MARK: - Api Call
    func getRestrauntDetail(){
        let param : Parameters = [
            .restaurant_id : restrauntId
        ]
        let request = APIManager.shared.createDataRequest(with: .post, params: param, api: .restaurant_details, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, restList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    let lst = restList["data"] as? [String : Any]
                    let listData = lst?["data"] as! [[String : Any]]
                    for i in listData{
                        let chidRest = i["child_restaurant"] as! [[String : Any]]
                        for childRes in chidRest{
                            let id =  childRes["id"]
                            let rest_name =  childRes["rest_name"]
                            let rest_branch =  childRes["rest_branch"]
                            let no_of_staff =  childRes["no_of_staff"]
                            let country_id =  childRes["country_id"]
                            let state_id =  childRes["state_id"]
                            let city =  childRes["city"]
                            let parent_id =  childRes["parent_id"]
                            let block =  childRes["block"]
                            let street =  childRes["street"]
                            let shop_number =  childRes["shop_number"]
                            let address =  childRes["address"]
                            let pincode =  childRes["pincode"]
                            let contact =  childRes["contact"]
                            let rest_long =  childRes["rest_long"]
                            let rest_lat =  childRes["rest_lat"]
                            let status_id =  childRes["status_id"]
                            let deleted_at =  childRes["deleted_at"]
                            let created_at =  childRes["created_at"]
                            let updated_at =  childRes["updated_at"]
                            self.childRestList.append(ChildRest(id: id as? Int ?? 0, rest_name: rest_name as? String ?? "", rest_branch: rest_branch as? String ?? "", no_of_staff: no_of_staff as? String ?? "", country_id: country_id as? String ?? "", state_id: state_id as? String ?? "", city: city as? String ?? "", parent_id: parent_id as? String ?? "", block: block as? String ?? "", street: street as? String ?? "", shop_number: shop_number as? String ?? "", address: address as? String ?? "", pincode: pincode as? String ?? "", contact: contact as? String ?? "", rest_long: rest_long as? String ?? "", rest_lat: rest_lat as? String ?? "", status_id: status_id as? Int ?? 0, deleted_at: deleted_at as? String ?? "", created_at: created_at as? String ?? "", updated_at: updated_at as? String ?? ""))
                            self.childRest.append(rest_branch as? String ?? "")
                        }
                        let listTab = i["table"] as! [[String : Any]]
                        for j in listTab {
                            let id = j["id"]
                            let restaurant_id = j["restaurant_id"]
                            let table_no = j["table_no"]
                            let no_of_staff = j["no_of_staff"]
                            let number_of_person = j["number_of_person"]
                            let status_id = j["status_id"]
                            let type = j["type"]
                            let dress_code = j["dress_code"]
                            let describtion = j["describtion"]
                            let floor_id = j["floor_id"]
                            let shop_number = j["area_id"]
                            let deleted_at = j["deleted_at"]
                            let created_at = j["created_at"]
                            let updated_at = j["updated_at"]
                            let area_id = j["area_id"]
                            let price = j["price"]
                            self.restCollList.append(RestDetail(area_id: area_id as? Int ?? 0, created_at: created_at as? String ?? "", deleted_at: deleted_at as? String ?? "", describtion: describtion as? String ?? "", dress_code: dress_code as? String ?? "", floor_id: floor_id as? Int ?? 0, id: id as? Int ?? 0, number_of_person: number_of_person as? Int ?? 0, restaurant_id: restaurant_id as? Int ?? 0, status_id: status_id as? Int ?? 0, table_no: table_no as? Int ?? 0, type: type as? String ?? "", updated_at: updated_at as? String ?? "",no_of_staff:  no_of_staff as? String ?? "",shop_number: shop_number as? String ?? "", price: price as? Int ?? 0))
                        }
                        let listFloor = i["floor"] as! [[String : Any]]
                        for j in listFloor{
                            let floorName = j["name"] as? String ?? ""
                            self.floorList.append(floorName)
                        }
                        self.selectTabCV.reloadData()
                    }
                }else{
                    print(message)
                    
                }
            }
            
        }
    }
    func bookTable(){
        
        let params : Parameters = [
            .restaurant_id : restrauntId,
            .table_id : tabId,
            .rules : txtRules.text ?? "",
            .dresscode : txtDressCode.text ?? "",
            .occasion : txtOccasion.text ?? "",
            .date : "\(lblDate.text ?? "") \(lblTime.text ?? "")",//"2022-12-31 09:00:00"
            .identifier : identifierName ?? ""
        ]
        print(params)
        let request : URLRequest!
        if userInfo == nil {
            request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .booking_table, baseURL: .base, setToken: "")
        }else{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .booking_table, baseURL: .base, setToken: "Bearer " + userInfo.token)
        }
//        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .booking_table, baseURL: .base, setToken: "Bearer " + userInfo.token)//"Bearer " + userInfo.token)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, booktab in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    self.view.makeToast(message)
//                    let bookDetail = booktab
                    let listData = booktab["data"] as? [String : Any]
                    let id = listData?["id"]
                    let table_id = listData?["table_id"]
                    let payment_status = listData?["payment_status"]
                    let customer_id = listData?["customer_id"]
                    let date = listData?["date"]
                    let restaurant_id = listData?["restaurant_id"]
                    print(booktab)
//                    if userInfo != nil{
                        UserDefaults.standard.removeObject(forKey: "BookingTableId")
                        UserDefaults.standard.set(id, forKey: "BookingTableId")
                        UserDefaults.standard.set(date, forKey: "BookingTableDate")
                        let alert = UIAlertController(title: "Alert", message: "Do you wish to pay for the table?", preferredStyle:.alert)
                        let yesAction = UIAlertAction(title: "Yes", style: .default, handler: {(alertAction) in
                            let storyBoard = UIStoryboard(name: "Order", bundle: nil)
                            let vc = storyBoard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                            let bookingTabId = id
                            vc.book_table_id = "\(bookingTabId ?? "")"
                            vc.flowType = "table"
                            vc.totalPrice = self.lblPrice.text ?? ""
                            self.navigationController?.pushViewController(vc, animated: true)
                        })
                        let noAction = UIAlertAction(title: "No", style: .default, handler: {(alertAction) in
                            
                        })
                        alert.addAction(yesAction)
                        alert.addAction(noAction)
                        self.present(alert,animated:true,completion: nil)
//                    }else{
//                        self.view.makeToast("Please Login to Book Table!")
//                    }
                    
                }else{
                    
                    print(message)
                    if userInfo == nil{
                        self.view.makeToast("Please Login to Book Table!")
                    }
                }
            }
        }
    }
    //MARK: - Action
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnBookingConfirm(_ sender: UIButton) {
        
    }
    
    @IBAction func btnEditFromCart(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1) {
            self.bgView.isHidden = true
            self.stackBookCrt.isHidden = true
            self.btnClose.isHidden = true
        }
    }
    @IBAction func btCloseView(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1) {
            self.bgView.isHidden = true
            self.stackBookCrt.isHidden = true
            self.btnClose.isHidden = true
        }
    }
    
    @IBAction func btnToSelectFood(_ sender: UIButton) {
        if UserDefaults.standard.string(forKey: "BookingTableId") != nil{
            let vc = storyboard?.instantiateViewController(withIdentifier: "BookingTabScrVC") as! BookingTabScrVC
            vc.isfromBooking = true
            vc.restName = restName
            vc.dist = dist
            vc.openTill = openTill
            vc.restrauntId = restrauntId
            vc.time = lblCartTime.text ?? ""
            vc.doorType = lblDoorType.text ?? ""
            vc.door = lblNoOfSeats.text ?? ""
            vc.tablePrice = lblPrice.text ?? ""
            vc.noOfPerson = noOfPerson
            vc.cartType = "book_table"
            navigationController?.pushViewController(vc, animated: true)
        }else{
            self.view.makeToast("Please book the table first!")
        }
    }
    @IBAction func btnCancelView(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1) {
            self.bgView.isHidden = true
            self.stackBookCrt.isHidden = true
            self.btnClose.isHidden = true
        }
    }
    
    @IBAction func btnSelectNext(_ sender: UIButton) {
        self.bookTabView.isHidden = true
        self.itemCartView.isHidden = false
        lblNoOfSeats.text = "\(noOfPerson) Seats"
    }
    @IBAction func btnSelectList(_ sender: UIButton) {
        btnFloor.backgroundColor = UIColor(named: "btnGrey")
        btnList.backgroundColor = UIColor(named: "red")
        listView.isHidden = false
        floorView.isHidden = true
        selectList = true
    }
    @IBAction func btnSelectFloor(_ sender: UIButton) {
        btnFloor.backgroundColor = UIColor(named: "red")
        btnList.backgroundColor = UIColor(named: "btnGrey")
        listView.isHidden = true
        floorView.isHidden = false
    }
    
    @IBAction func btnConfirmAndPay(_ sender: UIButton) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "SummaryScrVC") as! SummaryScrVC
//        navigationController?.pushViewController(vc, animated: true)
        bookTable()
    }
    
    @IBAction func btnDoorType(_ sender: UIButton) {
        dropDown.dataSource = whrList
        dropDown.show()
        dropDown.anchorView = dropDownWhr
        dropDown.selectionAction = { [self] (index: Int, item: String) in
            lblWhr.text = item
        }
    }
    
    @IBAction func btnNumb(_ sender: UIButton) {
        dropDown.dataSource = numList
        dropDown.show()
        dropDown.anchorView = dropNum
        dropDown.selectionAction = { [self] (index: Int, item: String) in
            lblNum.text = item
        }
    }
    
    @IBAction func btnOpenCal(_ sender: UIButton) {
        RPicker.selectDate(title: "Select Date", cancelText: "Cancel", datePickerMode: .date, minDate: Date(), maxDate: Date().dateByAddingYears(5), didSelectDate: {[weak self] (selectedDate) in
            // TODO: Your implementation for date
            self?.lblDate.text = selectedDate.dateString("yyyy-MM-dd")
        })
    }
    @IBAction func btnOpenTime(_ sender: UIButton) {
        RPicker.selectDate(title: "Select Time", cancelText: "Cancel", datePickerMode: .time, didSelectDate: { [weak self](selectedDate) in
            // TODO: Your implementation for date
            self?.lblTime.text = selectedDate.dateString("hh:mm:ss")
        })
    }
    
    @IBAction func btnSelPerson(_ sender: UIButton) {
        dropDown.dataSource = personList
        dropDown.show()
        dropDown.anchorView = dropNoPerson
        dropDown.selectionAction = { [self] (index: Int, item: String) in
            if let number = Int.parse(from: item) {
                lblPerson.text = "\(number)"
            }
        }
    }
    
    @IBAction func btnSelBranch(_ sender: UIButton) {
        dropDown.dataSource = childRest
        dropDown.show()
        dropDown.anchorView = dropBranch
        dropDown.selectionAction = { [self] (index: Int, item: String) in
            lblBranchName.text = item
        }
    }
    @IBAction func btnSelBranchType(_ sender: UIButton) {
        dropDown.dataSource = floorList
        dropDown.show()
        dropDown.anchorView = dropBranchName
        dropDown.selectionAction = { [self] (index: Int, item: String) in
            lblFloorName.text = item
        }
    }
    
    
}
extension BookingTabDetailVC : UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return restCollList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectTableCVC", for: indexPath) as! SelectTableCVC
        cell.lblTbNo.text = "\(restCollList[indexPath.row].table_no)"
        cell.lblNoPerson.text = "\(restCollList[indexPath.row].number_of_person)"
        cell.lblType.text = restCollList[indexPath.row].type
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.bgView.isHidden = false
        self.stackBookCrt.isHidden = false
        self.btnClose.isHidden = false
        self.bookTabView.isHidden = false
        self.itemCartView.isHidden = true
        lblCartTime.text = lblTime.text
        lblNoTab.text = "\(restCollList[indexPath.row].table_no)"
        lblPrice.text = "Rs.\(restCollList[indexPath.row].price)"
        txtDescription.text = restCollList[indexPath.row].describtion
        lblDoorType.text = restCollList[indexPath.row].type
        lblHotel.text = restName
        restrauntId = restCollList[indexPath.row].restaurant_id
        noOfPerson = restCollList[indexPath.row].number_of_person
        tabId = "\(restCollList[indexPath.row].id)"
        lblSelTabDoor.text = "\(restCollList[indexPath.row].type) \(restCollList[indexPath.row].number_of_person) Seats"
        lblTabPrice.text = "Rs.\(restCollList[indexPath.row].price)"
        
        
    }
    
}
struct RestDetail{
    init(area_id: Int, created_at: String, deleted_at: String, describtion: String, dress_code: String, floor_id: Int, id: Int, number_of_person: Int, restaurant_id: Int, status_id: Int, table_no: Int, type: String, updated_at: String,no_of_staff : String,shop_number : String ,price : Int) {
        self.area_id = area_id
        self.created_at = created_at
        self.deleted_at = deleted_at
        self.describtion = describtion
        self.dress_code = dress_code
        self.floor_id = floor_id
        self.id = id
        self.number_of_person = number_of_person
        self.restaurant_id = restaurant_id
        self.status_id = status_id
        self.table_no = table_no
        self.type = type
        self.updated_at = updated_at
        self.no_of_staff = no_of_staff
        self.shop_number = shop_number
        self.price = price
    }
    
    var area_id : Int
    var created_at : String
    var deleted_at : String
    var describtion : String
    var dress_code : String
    var floor_id : Int
    var id : Int
    var number_of_person : Int
    var restaurant_id : Int
    var status_id : Int
    var table_no : Int
    var type : String
    var updated_at : String
    var no_of_staff : String
    var shop_number : String
    var price : Int
}
struct ChildRest {
     init(id: Int, rest_name: String, rest_branch: String, no_of_staff: String, country_id: String, state_id: String, city: String, parent_id: String, block: String, street: String, shop_number: String, address: String, pincode: String, contact: String, rest_long: String, rest_lat: String, status_id: Int, deleted_at: String, created_at: String, updated_at: String) {
        self.id = id
        self.rest_name = rest_name
        self.rest_branch = rest_branch
        self.no_of_staff = no_of_staff
        self.country_id = country_id
        self.state_id = state_id
        self.city = city
        self.parent_id = parent_id
        self.block = block
        self.street = street
        self.shop_number = shop_number
        self.address = address
        self.pincode = pincode
        self.contact = contact
        self.rest_long = rest_long
        self.rest_lat = rest_lat
        self.status_id = status_id
        self.deleted_at = deleted_at
        self.created_at = created_at
        self.updated_at = updated_at
    }
    
    var id : Int
    var rest_name : String
    var rest_branch : String
    var no_of_staff : String
    var country_id : String
    var state_id : String
    var city : String
    var parent_id : String
    var block : String
    var street : String
    var shop_number : String
    var address : String
    var pincode : String
    var contact : String
    var rest_long : String
    var rest_lat : String
    var status_id : Int
    var deleted_at : String
    var created_at : String
    var updated_at : String
}
