//
//  ConfirmWalkInOrderVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 25/11/22.
//

import UIKit

class ConfirmWalkInOrderVC: UIViewController {

    @IBOutlet weak var btnEmailInvoice: UIButton!
    @IBOutlet weak var btnGetDir: UIButton!
    @IBOutlet weak var orderlistTV: UITableView!
    @IBOutlet weak var tabConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnEmailInvoice.rounded(cornerRadius: 10)
        btnGetDir.rounded(cornerRadius: 10)

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async {
            self.tabConstraint.constant = self.orderlistTV.contentSize.height
            self.view.layoutIfNeeded()
        }
    }
   
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}
extension ConfirmWalkInOrderVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmOrderListTVC") as! ConfirmOrderListTVC
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
}
