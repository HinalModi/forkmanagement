//
//  BookTableVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 25/11/22.
//

import UIKit
import DropDown
import CoreLocation

class BookTableVC: UIViewController {

    @IBOutlet weak var lblNoDataMsg: UILabel!
    @IBOutlet weak var activityInc: UIActivityIndicatorView!
    @IBOutlet weak var dropDownView: DropDown!
    @IBOutlet weak var lblNoOfPr: VCLanguageLabel!
    @IBOutlet weak var lblTime: VCLanguageLabel!
    @IBOutlet weak var lblDate: VCLanguageLabel!
    @IBOutlet weak var restTV: UITableView!
    var restruntList : [RestrauntList] = []
    var restListFilter : [RestrauntList] = []
    var datePicker: UIDatePicker!
    let timePicker = UIDatePicker()
    var iselectedDate:String = ""
    var selectedDate: String = String()
    let dropDown = DropDown()
    var personList = ["Person 1","Person 2","Person 3","Person 4","Person 5","Person 6","Person 7","Person 8"]
    var locManager = CLLocationManager()
    var currentLocation = CLLocation()
    var isSearch = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let someDateTime = formatter.string(from: date)
        lblDate.text = someDateTime
        let time = Date()
        formatter.dateFormat = "HH:mm a"
        lblTime.text = formatter.string(from: time)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        locManager.requestWhenInUseAuthorization()
        if
            CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() ==  .authorizedAlways
        {
            currentLocation = locManager.location ?? CLLocation()
        }
        if isSearch == ""{
            if currentLocation.coordinate.latitude != 0.0 || currentLocation.coordinate.longitude != 0.0{
                getRestrauntList()
            }
        }
    }
    //MARK: - DropDown
    
    @IBAction func btnOpenPerson(_ sender: UIButton) {
        dropDown.dataSource = personList
        dropDown.show()
        dropDown.anchorView = dropDownView
        dropDown.selectionAction = { [self] (index: Int, item: String) in
            
            if let number = Int.parse(from: item) {
                lblNoOfPr.text = "\(number)"
            }
            getRestrauntList()
        }
    }
    
    //MARK: - Calender
   
    @IBAction func btnOpenCalender(_ sender: Any) {

        RPicker.selectDate(title: "Select Date", cancelText: "Cancel", datePickerMode: .date, minDate: Date(), maxDate: Date().dateByAddingYears(5), didSelectDate: {[weak self] (selectedDate) in
            // TODO: Your implementation for date
            self?.lblDate.text = selectedDate.dateString()
            self?.getRestrauntList()
        })
    }
    //MARK: - Pick Time
    @IBAction func btnOpenTime(_ sender: UIButton) {
        RPicker.selectDate(title: "Select Time", cancelText: "Cancel", datePickerMode: .time, didSelectDate: { [weak self](selectedDate) in
            // TODO: Your implementation for date
            self?.lblTime.text = selectedDate.dateString("hh:mm a")
            self?.getRestrauntList()
        })
    }
    //MARK: Api Call
    func getRestrauntList(){
        activityInc.startAnimating()
        let param : Parameters = [
            .person : lblNoOfPr.text ?? "",
            .date : "\(lblDate.text ?? "")" + " " + "\(lblTime.text ?? "")",
            .latitude : "\(currentLocation.coordinate.latitude)",
            .longitude : "\(currentLocation.coordinate.longitude)",
        ]
        let request = APIManager.shared.createDataRequest(with: .post, params: param, api: .restaurants, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, restList in
            DispatchQueue.main.async {
                if isSuccess{
                    print(message)
                    self.activityInc.stopAnimating()
                    let lst = restList["data"] as? [String : Any]
                    let listData = lst?["data"] as! [[String : Any]]
                    self.restruntList.removeAll()
                    print(listData)
                    for i in listData{
                        let id = i["id"]
                        let rest_name = i["rest_name"]
                        let rest_branch = i["rest_branch"]
                        let no_of_staff = i["no_of_staff"]
                        let country_id = i["country_id"]
                        let state_id = i["state_id"]
                        let city = i["city"]
                        let parent_id = i["parent_id"]
                        let block = i["block"]
                        let street = i["street"]
                        let shop_number = i["shop_number"]
                        let address = i["address"]
                        let pincode = i["pincode"]
                        let contact = i["contact"]
                        let status_id = i["status_id"]
                        let distance = i["distance"]
                        let endtime = i["endtime"]
                        let restImage = i["image"]
                        self.restruntList.append(RestrauntList(id: id as? Int ?? 0, rest_name: rest_name as? String ?? "", rest_branch: rest_branch as? String ?? "", no_of_staff: no_of_staff as? String ?? "", country_id: country_id as? String ?? "", state_id: state_id as? String ?? "", city: city as? String ?? "", parent_id: parent_id as? String ?? "", block: block as? String ?? "", street: street as? String ?? "", shop_number: shop_number as? String ?? "", address: address as? String ?? "", pincode: pincode as? String ?? "", contact: contact as? String ?? "", status_id: status_id as? Int ?? 0,distance: distance as! Double,endtime: endtime as? String ?? "", restImage: restImage as? String ?? ""))
                        print("restruntList48",self.restruntList)
                    }
                    self.restTV.reloadData()
                    
                }else{
                    print(message)
                    self.activityInc.stopAnimating()
                }
            }
        }
    }
}
extension BookTableVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restruntList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookTabTVC") as! BookTabTVC
        cell.restName.text = restruntList[indexPath.row].rest_name
        let list = restruntList[indexPath.row]
        print(restruntList[indexPath.row].distance)
        let dist = String(format:"%.2f", list.distance)
        cell.lblDist.text = "\(dist)km"
        cell.productImg.sd_setImage(with: URL(string: list.restImage), placeholderImage: UIImage(named: "cutlery"))
        cell.lblTime.text = "Open Till \(list.endtime)"
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "BookingTabDetailVC") as! BookingTabDetailVC
        let list = restruntList[indexPath.row]
        vc.restrauntId = list.id
        let dist = String(format:"%.2f", list.distance)
        vc.dist = "\(dist)km"
        vc.restName = list.rest_name
        vc.openTill = list.endtime
        vc.restImage = list.restImage
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
struct RestrauntList {
     init(id: Int, rest_name: String, rest_branch: String, no_of_staff: String, country_id: String, state_id: String, city: String, parent_id: String, block: String, street: String, shop_number: String, address: String, pincode: String, contact: String, status_id: Int ,distance : Double,endtime : String, restImage : String) {
         self.id = id
         self.rest_name = rest_name
         self.rest_branch = rest_branch
         self.no_of_staff = no_of_staff
         self.country_id = country_id
         self.state_id = state_id
         self.city = city
         self.parent_id = parent_id
         self.block = block
         self.street = street
         self.shop_number = shop_number
         self.address = address
         self.pincode = pincode
         self.contact = contact
         self.status_id = status_id
         self.distance = distance
         self.endtime = endtime
         self.restImage = restImage
     }
    
    var id : Int
    var rest_name : String
    var rest_branch : String
    var no_of_staff : String
    var country_id : String
    var state_id : String
    var city : String
    var parent_id : String
    var block : String
    var street : String
    var shop_number : String
    var address : String
    var pincode : String
    var contact : String
    var status_id : Int
    var distance : Double
    var endtime : String
    var restImage : String
}
extension Date {
    func dateString(_ format: String = "dd-MM-yyyy") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    func dateByAddingYears(_ dYears: Int) -> Date {
        var dateComponents = DateComponents()
        dateComponents.year = dYears
        return Calendar.current.date(byAdding: dateComponents, to: self)!
    }
}
extension Int {
    static func parse(from string: String) -> Int? {
        return Int(string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())
    }
}
