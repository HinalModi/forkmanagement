//
//  ForgotPswdVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 15/11/22.
//

import UIKit
import FirebaseAuth

class ForgotPswdVC: UIViewController {
   
    @IBOutlet weak var activityInc: UIActivityIndicatorView!
    @IBOutlet weak var btnDone: VCLanguageButton!
    @IBOutlet weak var btnSubmit: VCLanguageButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var otpTextFieldView: OTPFieldView!
    @IBOutlet weak var bgOtpImg: UIImageView!
    @IBOutlet weak var bgView: UIImageView!
    @IBOutlet weak var successView: UIView!
    @IBOutlet weak var enterOTPView: UIView!
    @IBOutlet weak var stackOpenOTP: UIStackView!
    @IBOutlet weak var viewRenterPswd: UIView!
    @IBOutlet weak var viewEnterPswd: UIView!
    @IBOutlet weak var txtContact: VCLanguageTextField!
    @IBOutlet weak var backBtn: VCLanguageButton!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var txtRenterPswd: FloatLabelTextField!
    @IBOutlet weak var txtNewPswd: FloatLabelTextField!
    var otpCode = String()
    var tokenPswd = String()
    var phone_Code = "+91"
    var currentVerificationId = ""
    var idVerifyToken = ""
    var varifyId = ""
    var resettoken = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        btnReset.rounded(cornerRadius: 10)
        txtRenterPswd.cornerRadius = 2
        txtNewPswd.cornerRadius = 2
        btnSubmit.rounded(cornerRadius: 10)
        btnDone.rounded(cornerRadius: 10)
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            backBtn.setImage(UIImage(named: "image 187"), for: .normal)
            txtRenterPswd.setLeftPaddingPoints(10)
            txtRenterPswd.setRightPaddingPoints(60)
            txtNewPswd.setLeftPaddingPoints(10)
            txtContact.setLeftPaddingPoints(10)
            txtContact.setRightPaddingPoints(60)
            txtNewPswd.setRightPaddingPoints(60)
        }else{
            backBtn.setImage(UIImage(named: "rightArrow"), for: .normal)
            txtRenterPswd.setLeftPaddingPoints(60)
            txtNewPswd.setLeftPaddingPoints(60)
            txtRenterPswd.setRightPaddingPoints(10)
            txtNewPswd.setRightPaddingPoints(10)
            txtContact.setLeftPaddingPoints(60)
            txtContact.setRightPaddingPoints(10)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidLayoutSubviews() {
        
    }
    func setupOtpView(){
        self.otpTextFieldView.fieldsCount = 6
        self.otpTextFieldView.fieldBorderWidth = 0.5
        self.otpTextFieldView.filledBorderColor = UIColor.darkGray
        self.otpTextFieldView.cursorColor = UIColor.darkGray
        self.otpTextFieldView.displayType = .square
        self.otpTextFieldView.fieldSize = 40
        self.otpTextFieldView.separatorSpace = 15
        self.otpTextFieldView.shouldAllowIntermediateEditing = false
        self.otpTextFieldView.delegate = self
        self.otpTextFieldView.initializeUI()
        }
    
    //MARK: -Firebase Auth login
    func getOtp(){
        Auth.auth().languageCode = "en"
        let phoneNumber = "+91" + (txtContact.text ?? "")
        
        // Step 4: Request SMS
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            self.varifyId = verificationID ?? ""
            
            // Either received APNs or user has passed the reCAPTCHA
            // Step 5: Verification ID is saved for later use for verifying OTP with phone number
            self.currentVerificationId = verificationID!
        }
    }
    func loginThroughPhone(){
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: varifyId, verificationCode: self.otpCode)

            Auth.auth().signIn(with: credential) { (authResult, error) in
                if let error = error {
                    let authError = error as NSError
                    print(authError.description)
                    return
                }
                
                // User has signed in successfully and currentUser object is valid
                let currentUserInstance = Auth.auth().currentUser
                currentUserInstance?.getIDTokenForcingRefresh(true) { idToken, error in
                    if let error = error {
                        // Handle error
                        return;
                    }
                    self.idVerifyToken = idToken ?? ""
                    self.forgotPassword()
                }
            }
        }
    //MARK: -Api Call
    func forgotPassword(){
        activityInc.startAnimating()
        let parameters : Parameters = [
            .contact : txtContact.text ?? "",
            .token : idVerifyToken
        ]
        print(parameters)
        let request = APIManager.shared.createDataRequest(with: .post, params: parameters, api: .forget_password, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSucess, message,forgotData  in
            DispatchQueue.main.async {
                if isSucess{
                    self.activityInc.stopAnimating()
                    let data = forgotData["data"] as? [String : Any]
                    print(data)
                    self.resettoken = data?["token"] as? String ?? ""
                    self.view.makeToast(message)
                    self.successView.isHidden = false
                    self.enterOTPView.isHidden = true

                }else{
                    print(message)
                }
            }
        }
    }
    func resetPassword(){
        activityInc.startAnimating()
        let parameters : Parameters = [
            .contact : txtContact.text ?? "",
            .password : txtNewPswd.text ?? "",
            .password_confirmation : txtRenterPswd.text ?? "",
            .token : resettoken,
        ]
        print(parameters)
        let request = APIManager.shared.createDataRequest(with: .post, params: parameters, api: .reset_password, baseURL: .base)
        URLSession.shared.makeApiCall(with: request) { isSucess, message,data  in
            if isSucess{
                
                DispatchQueue.main.async {
                    self.activityInc.stopAnimating()
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                print(message)
                self.activityInc.stopAnimating()
            }
        }
    }
   
    //MARK: - Action
    
    @IBAction func btnSubmitOtp(_ sender: UIButton) {
        loginThroughPhone()
        
        
    }
    
    @IBAction func btnCloseView(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1) {
            self.bgOtpImg.isHidden = true
            self.bgView.isHidden = true
            self.stackOpenOTP.isHidden = true
            self.enterOTPView.isHidden = true
            self.btnClose.isHidden = true
            self.successView.isHidden = true
        }
    }
    
    @IBAction func btnEnterPswd(_ sender: UIButton) {
        setupOtpView()
        getOtp()
        UIView.animate(withDuration: 0.1) {
            self.enterOTPView.isHidden = false
            self.bgOtpImg.isHidden = false
            self.bgView.isHidden = false
            self.stackOpenOTP.isHidden = false
            self.btnClose.isHidden = false
        }
    }
    @IBAction func btnResetPswd(_ sender: UIButton) {
        resetPassword()
        UIView.animate(withDuration: 0.1) {
            self.enterOTPView.isHidden = false
        }
    }
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSuccess(_ sender: UIButton) {
        UIView.animate(withDuration: 0.1) {
            self.bgOtpImg.isHidden = true
            self.bgView.isHidden = true
            self.stackOpenOTP.isHidden = true
            self.enterOTPView.isHidden = true
            self.btnClose.isHidden = true
            self.successView.isHidden = true
            self.viewRenterPswd.isHidden = false
            self.viewEnterPswd.isHidden = false
        }
    }
}
extension ForgotPswdVC: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        if enterOTPView.isHidden == true{
            return false
        }else{
            return true
        }
    }
    
    func enteredOTP(otp otpString: String) {
        otpCode = otpString
        print("OTPString: \(otpString)")
    }
}
