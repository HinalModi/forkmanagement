//
//  ToLoginVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 11/11/22.
//

import UIKit
import FirebaseAuth

class ToLoginVC: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        btnRegister.rounded(cornerRadius: 10)
        btnLogin.rounded(cornerRadius: 10)
//        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
//            backBtn.setImage(UIImage(named: "image 187"), for: .normal)
//        }else{
//            backBtn.setImage(UIImage(named: "rightArrow"), for: .normal)
//        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    @IBAction func btnAsGuest(_ sender: UIButton) {
        asGuest()
       
    }
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnToLogin(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnToRegister(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: - APi Call
    func asGuest(){
        let params : Parameters = [:]
        let request : URLRequest!
        if userInfo == nil {
            request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .identifier, baseURL: .base, setToken: "")
        }else{
            request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .identifier, baseURL: .base, setToken: "Bearer " + userInfo.token)
        }
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, guest in
            DispatchQueue.main.async {                
                if isSuccess{
                    print(message)
                    UserDefaults.standard.removeObject(forKey: "identifier")
                    do {
                        try Auth.auth().signOut()
                    }catch{
                        print(String(describing: error))
                    }
                    userInfo = nil
                    UserInfoManager.removeUserInfo()
                    if let bundleID = Bundle.main.bundleIdentifier {
                        UserDefaults.standard.removePersistentDomain(forName: bundleID)
                    }
                    let guestData = guest["data"] as! [String : Any]
                    let identifier = guestData["identifier"] as? String ?? ""
                    UserDefaults.standard.set(identifier, forKey: "identifier")
                    print(UserDefaults.standard.string(forKey: "identifier"))
                    let storyBoard = UIStoryboard(name: "Guide", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "GuideMenuVC") as! GuideMenuVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    print(message)
                    
                }
            }
        }
    }
}
