//
//  RegisterVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 14/11/22.
//

import UIKit
import OTPFieldView
import FirebaseAuth
import CountryPickerView

class RegisterVC: UIViewController {

    @IBOutlet weak var activityInc: UIActivityIndicatorView!
    @IBOutlet weak var btnPswd: VCLanguageButton!
    @IBOutlet weak var btnConfirmPswd: VCLanguageButton!
    @IBOutlet weak var lblTimer: VCLanguageLabel!
    @IBOutlet weak var txtEmail: VCLanguageTextField!
    @IBOutlet weak var backBtn: VCLanguageButton!
    @IBOutlet weak var btnCloseRegister: UIButton!
    @IBOutlet weak var otpImageView: UIImageView!
    @IBOutlet weak var otpStackView: UIStackView!
    @IBOutlet weak var otpBgImage: UIImageView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var otpSuccessView: UIView!
    @IBOutlet weak var enterOTPView: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var otpTextFieldView: OTPFieldView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var txtConfirmPswd: FloatLabelTextField!
    @IBOutlet weak var txtPswd: VCLanguageTextField!
    @IBOutlet weak var txtMobile: VCLanguageTextField!
    @IBOutlet weak var txtUserName: FloatLabelTextField!
    var loginData: PhoneNumberLogin!
    var dicUserDetail = [String : AnyObject]()
    var otpVal = ""
    var totalTime = 0
    var remainingTime = ""
    var countdownTimer: Timer!
    var phone_Code = "+91"
    var currentVerificationId = ""
    var otpCode = String()
    var idVerifyToken = ""
    var varifyId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPswd.cornerRadius = 2
        txtMobile.cornerRadius = 2
        txtUserName.cornerRadius = 2
        txtConfirmPswd.cornerRadius = 2
        txtEmail.cornerRadius = 2
        btnRegister.rounded(cornerRadius: 10)        
        btnBack.rounded(cornerRadius: 10)
        btnDone.rounded(cornerRadius: 10)
        btnSubmit.rounded(cornerRadius: 10)
        enterOTPView.isHidden = true
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            backBtn.setImage(UIImage(named: "image 187"), for: .normal)
            txtPswd.setLeftPaddingPoints(10)
            txtMobile.setLeftPaddingPoints(10)
            txtUserName.setLeftPaddingPoints(10)
            txtConfirmPswd.setLeftPaddingPoints(10)
            txtEmail.setLeftPaddingPoints(10)
            txtPswd.setRightPaddingPoints(60)
            txtMobile.setRightPaddingPoints(60)
            txtUserName.setRightPaddingPoints(60)
            txtConfirmPswd.setRightPaddingPoints(60)
            txtEmail.setRightPaddingPoints(60)
        }else{
            backBtn.setImage(UIImage(named: "rightArrow"), for: .normal)
            txtPswd.setLeftPaddingPoints(60)
            txtMobile.setLeftPaddingPoints(60)
            txtUserName.setLeftPaddingPoints(60)
            txtConfirmPswd.setLeftPaddingPoints(60)
            txtEmail.setLeftPaddingPoints(60)
            txtPswd.setRightPaddingPoints(10)
            txtMobile.setRightPaddingPoints(10)
            txtUserName.setRightPaddingPoints(10)
            txtConfirmPswd.setRightPaddingPoints(10)
            txtEmail.setRightPaddingPoints(10)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidLayoutSubviews() {
        if otpStackView.isHidden == false{            
            setupOtpView()
        }
    }
    //MARK: -Firebase Auth login
    func getOtp(){
        activityInc.startAnimating()
        Auth.auth().languageCode = "en"
        let phoneNumber = "+91" + (txtMobile.text ?? "")
        
        // Step 4: Request SMS
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            self.activityInc.stopAnimating()
            if let error = error {
                print(error.localizedDescription)
                return
            }
            self.varifyId = verificationID ?? ""
            
            // Either received APNs or user has passed the reCAPTCHA
            // Step 5: Verification ID is saved for later use for verifying OTP with phone number
            self.currentVerificationId = verificationID!
        }
    }
    func loginThroughPhone(){
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: varifyId, verificationCode: self.otpCode)

            Auth.auth().signIn(with: credential) { (authResult, error) in
                if let error = error {
                    let authError = error as NSError
                    print(authError.description)
                    return
                }
                
                // User has signed in successfully and currentUser object is valid
                let currentUserInstance = Auth.auth().currentUser
                currentUserInstance?.getIDTokenForcingRefresh(true) { idToken, error in
                    if let error = error {
                        // Handle error
                        return;
                    }
                    self.idVerifyToken = idToken ?? ""
                    self.loginApi()
                }
            }
        }
    //MARK: - Api call
    func loginApi(){
        activityInc.startAnimating()
        let parameters : Parameters = [
            .contact : txtMobile.text ?? "",
            .password: txtPswd.text ?? "",
            .name : txtUserName.text ?? "",
            .email : txtEmail.text ?? "",
            .c_password : txtConfirmPswd.text ?? "",
            .token : idVerifyToken,
            .identifier : UserDefaults.standard.string(forKey: "identifier") ?? "",
        ]
        print(parameters)

        let request = APIManager.shared.createDataRequest(with: .post, params: parameters, api: .register, baseURL: .base)
        URLSession.shared.userLogin(with: request) { success, msg , userdata in
            DispatchQueue.main.async { [self] in
                if success{
                    activityInc.stopAnimating()
//                    UserDefaults.standard.removeObject(forKey: "identifier")
                    let storyBoard = UIStoryboard(name: "Guide", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "GuideMenuVC") as! GuideMenuVC
                    navigationController?.pushViewController(vc, animated: true)
                }else{
                    self.otpStackView.isHidden = true
                    self.otpImageView.isHidden = true
                    self.otpBgImage.isHidden = true
                    self.btnCloseRegister.isHidden = true
                    activityInc.stopAnimating()
                    print(userdata)
                    self.view.makeToast(msg)
                }
            }
        }
    }
    //MARK: - Actions
    
    var selPswd = 0
    var selConFirmpswd = 0
    @IBAction func showPswd(_ sender: UIButton) {
        if selPswd == 0 {
            txtPswd.isSecureTextEntry = false
            selPswd = 1
            btnPswd.setImage(UIImage(named:"visible-eye"), for: .normal)
        }else{
            txtPswd.isSecureTextEntry = true
            selPswd = 0
            btnPswd.setImage(UIImage(named:"eye-close-line"), for: .normal)
        }
    }
    @IBAction func showConfirmPswd(_ sender: UIButton) {
        if selConFirmpswd == 0 {
            txtConfirmPswd.isSecureTextEntry = false
            selConFirmpswd = 1
            btnConfirmPswd.setImage(UIImage(named:"visible-eye"), for: .normal)
        }else{
            txtConfirmPswd.isSecureTextEntry = true
            selConFirmpswd = 0
            btnConfirmPswd.setImage(UIImage(named:"eye-close-line"), for: .normal)
        }
    }
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnPrev(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnClose(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2) {
            self.otpBgImage.isHidden = true
        }
        UIView.animate(withDuration: 0.4) {
            self.otpStackView.isHidden = true
            self.otpImageView.isHidden = true
            self.btnCloseRegister.isHidden = true
        }
    }
    @IBAction func doneOtpAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Guide", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSubmitOtp(_ sender: UIButton) {
        loginThroughPhone()
//        enterOTPView.isHidden = true
//        UIView.animate(withDuration: 0.1) {
//            self.otpSuccessView.isHidden = false
//        }
    }
    
    @IBAction func btnEnterOtp(_ sender: UIButton) {
        
        otpSuccessView.isHidden = true
        UIView.animate(withDuration: 0.1) {
            self.enterOTPView.isHidden = false
        }
    }
    @IBAction func btnRegister(_ sender: UIButton) {
        let pswdlen = txtPswd.text?.count ?? 0
        let confPswdlen = txtPswd.text?.count ?? 0
        if txtPswd.text == "" || txtEmail.text == "" || txtMobile.text == "" || txtUserName.text == "" || txtConfirmPswd.text == ""{
            self.view.makeToast("Please fill all the information!")
        }else if txtPswd.text != txtConfirmPswd.text{
            self.view.makeToast("Password and Confirm Password should be same!")
        }else if pswdlen < 6 || confPswdlen < 6 || pswdlen >= 15 || confPswdlen >= 15 {
            self.view.makeToast("Password should not be less then 6 character and greater then 15 character")
        }else{
            getOtp()
            self.otpStackView.isHidden = false
            self.otpSuccessView.isHidden = true
            UIView.animate(withDuration: 0.1) {
                self.otpBgImage.isHidden = false
            }
            UIView.animate(withDuration: 0.2) {
                self.enterOTPView.isHidden = false
                self.otpImageView.isHidden = false
                self.btnCloseRegister.isHidden = false
            }
        }
    }
    func getCredentials() -> PhoneAuthCredential? {
        if otpVal != "" {
            loginData.otpCode = otpVal
            return PhoneAuthProvider.provider().credential(withVerificationID: loginData.firebaseVerificationID, verificationCode: loginData.otpCode)
        } else {
            return nil
        }
    }
    func authenticateUser() {
//        activityIndicator.startAnimating()
        guard let credential = getCredentials() else {
            print("Enter correct OTP")
//                self.showAlert(message: "Enter correct OTP", title: "")
//            GlobalFunction.showAlert(message: "Enter correct OTP", actionTitle: "Ok", viewcontroller: self)
            return
        }
        Auth.auth().signIn(with: credential) { [self] authResult, error in
            if let error = error {
                print("unable to login:", error.localizedDescription as Any)
//                GlobalFunction.showAlert(message: "Please check otp!", actionTitle: "Ok", viewcontroller: self)
//                activityIndicator.stopAnimating()
            } else {
                if (authResult != nil){
                    activityInc.stopAnimating()
                    userInfo = UserInfoManager.getUserInfoModel()
                    let vc = storyboard?.instantiateViewController(withIdentifier: "GuideMenuVC") as! GuideMenuVC
//                    vc.phoneNo = loginData.phoneNumber
                    navigationController?.pushViewController(vc, animated: true)
                }else{
                    activityInc.stopAnimating()
                }
                
            }
        }
    }
func resendOTP() {
    let phonenumber = loginData.formatedPhoneNumber
    PhoneAuthProvider.provider().verifyPhoneNumber(phonenumber, uiDelegate: nil) {(verificationid, error) in
        if error == nil{
            guard let verify = verificationid else {return}
            self.loginData.firebaseVerificationID = verify
            
        }else{
            print("unable to get verification:", error?.localizedDescription as Any)
        }
    }
}
    func startOTPTimer() {
        loginData.otpCode = ""
        remainingTime = ""
//        self.showAlert(message: "OTP : 123456", title: "Alert")
        startTimer()
    }
    
    func startTimer() {
        totalTime = 120
//        btnTimer.isHidden = true
//        lblMsg.isHidden = true
//        lblTimer.isHidden = !btnTimer.isHidden
        print(countdownTimer)
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        remainingTime = "\(timeFormatted(totalTime))"
        lblTimer.text = "\(remainingTime)"
        if totalTime != 0 {
            totalTime -= 2
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
//        btnTimer.isHidden = false
//        lblTimer.isHidden = !btnTimer.isHidden
//        lblMsg.isHidden = false
        countdownTimer.invalidate()
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    func setupOtpView(){
        self.otpTextFieldView.fieldsCount = 6
        self.otpTextFieldView.fieldBorderWidth = 0.5
        self.otpTextFieldView.filledBorderColor = UIColor.darkGray
        self.otpTextFieldView.cursorColor = UIColor.darkGray
        self.otpTextFieldView.displayType = .square
        self.otpTextFieldView.fieldSize = 40
        self.otpTextFieldView.separatorSpace = 15
        self.otpTextFieldView.shouldAllowIntermediateEditing = false
        self.otpTextFieldView.delegate = self
        self.otpTextFieldView.initializeUI()
        }
}
extension RegisterVC: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        if enterOTPView.isHidden == true{
            return false
        }else{
            return true
        }
    }
    
    func enteredOTP(otp otpString: String) {
        otpCode = otpString
        print("OTPString: \(otpString)")
    }
}
extension RegisterVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField.text == txtMobile.text{
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        return true
    }
}
