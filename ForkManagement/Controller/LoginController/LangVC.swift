//
//  LangVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 11/11/22.
//

import UIKit

class LangVC: UIViewController {

    @IBOutlet weak var btnEng: UIButton!
    @IBOutlet weak var btnArabic: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        btnEng.rounded(cornerRadius: 10)
        btnArabic.rounded(cornerRadius: 10)
        VCLangauageManager.sharedInstance.changeAppLanguage(To: .english)
    }
    
    @IBAction func btnEnglish(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "IntroVC") as! IntroVC
        VCLangauageManager.sharedInstance.changeAppLanguage(To: .english)
        Constant.APPDELEGATE.setRootVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnArabic(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "IntroVC") as! IntroVC
        if VCLangauageManager.sharedInstance.isLanguageEnglish() {
            VCLangauageManager.sharedInstance.changeAppLanguage(To: .arabic)
            Constant.APPDELEGATE.setRootVC()
        }
        navigationController?.pushViewController(vc, animated: true)
        
    }
}
