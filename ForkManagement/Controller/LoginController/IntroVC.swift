//
//  IntroVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 11/11/22.
//

import UIKit

class IntroVC: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var introCV: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    @IBAction func btnSkip(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ToLoginVC") as! ToLoginVC
        navigationController?.pushViewController(vc, animated: true)
    }
}
extension IntroVC : UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCVC", for: indexPath) as! IntroCVC
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageControl.currentPage = indexPath.item
    }
    
}
