//
//  ForgotSrnFaceIdVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 15/11/22.
//

import UIKit
import LocalAuthentication

class ForgotSrnFaceIdVC: UIViewController {

    @IBOutlet weak var backBtn: VCLanguageButton!
    @IBOutlet weak var btnCloseAlert: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var alertImage: UIImageView!
    @IBOutlet weak var alertBgImage: UIImageView!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnDontAllow: UIButton!
    @IBOutlet weak var btnReset: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        btnReset.rounded(cornerRadius: 10)
        btnDontAllow.rounded(cornerRadius: 10)
        btnOk.rounded(cornerRadius: 10)
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            backBtn.setImage(UIImage(named: "image 187"), for: .normal)
        }else{
            backBtn.setImage(UIImage(named: "rightArrow"), for: .normal)
        }
    }
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
   
    @IBAction func btnAllow(_ sender: UIButton) {
        let context : LAContext = LAContext()
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: nil){
            
        }
//        let vc = storyboard?.instantiateViewController(withIdentifier: "SecureLoginVC") as! SecureLoginVC
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnToResetPswd(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2) {
            self.alertView.isHidden = false
            self.alertImage.isHidden = false
            self.alertBgImage.isHidden = false
            self.btnCloseAlert.isHidden = false
        }
        
    }
    @IBAction func btnClose(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2) {
            self.alertView.isHidden = true
            self.alertImage.isHidden = true
            self.alertBgImage.isHidden = true
            self.btnCloseAlert.isHidden = true
        }
    }
}
