//
//  LoginVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 14/11/22.
//

import UIKit
import GoogleSignIn
import FirebaseAuth
import FBSDKLoginKit
import AuthenticationServices

class LoginVC: UIViewController {
    
    @IBOutlet weak var activityInc: UIActivityIndicatorView!
    @IBOutlet weak var btnPswd: VCLanguageButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtPswd: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    let clientID = "691657475825471"
    let redirectURI = "https://subdan.ru/oauth"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtPswd.cornerRadius = 2
        txtMobile.cornerRadius = 2
        btnRegister.rounded(cornerRadius: 10)
        btnLogin.rounded(cornerRadius: 10)
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
//            backBtn.setImage(UIImage(named: "image 187"), for: .normal)
            txtPswd.setLeftPaddingPoints(10)
            txtMobile.setLeftPaddingPoints(10)
            txtPswd.setRightPaddingPoints(60)
            txtMobile.setRightPaddingPoints(60)
        }else{
//            backBtn.setImage(UIImage(named: "rightArrow"), for: .normal)
            txtPswd.setLeftPaddingPoints(60)
            txtMobile.setLeftPaddingPoints(60)
            txtPswd.setRightPaddingPoints(10)
            txtMobile.setRightPaddingPoints(10)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    //MARK: - Api call
    func loginApi(){
        activityInc.startAnimating()
        let parameters : Parameters = [
            .contact : txtMobile.text ?? "",
            .password: txtPswd.text ?? ""
        ]
        let request = APIManager.shared.createDataRequest(with: .post, params: parameters, api: .login, baseURL: .base)
        URLSession.shared.userLogin(with: request) { success, msg , userdata in
            DispatchQueue.main.async { [self] in
                if success{
                    print(msg)
                    activityInc.stopAnimating()
                    let storyBoard = UIStoryboard(name: "Guide", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "GuideMenuVC") as! GuideMenuVC
                    navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    print(msg)
                    activityInc.stopAnimating()
                    GlobalFunction.setAlert(title: "Alert", alertMessage: msg, actionTitle: "Ok", vc: self)
                }
            }
        }
    }
    //MARK: - Action
    var selPswd = 0    
    @IBAction func showPassword(_ sender: UIButton) {
        
        if selPswd == 0 {
            txtPswd.isSecureTextEntry = false
            selPswd = 1
            btnPswd.setImage(UIImage(named:"visible-eye"), for: .normal)
        }else{
            txtPswd.isSecureTextEntry = true
            selPswd = 0
            btnPswd.setImage(UIImage(named:"eye-close-line"), for: .normal)
        }
        
    }
    @IBAction func btnGoogleLogin(_ sender: UIButton) {
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func btnFBSignIn(_ sender: UIButton) {
        fbLoginAction()
    }
    
    @IBAction func btnToDashboard(_ sender: UIButton) {
        if txtMobile.text == "" || txtPswd.text == ""{
            GlobalFunction.setAlert(title: "Alert", alertMessage: "Please fill all the information!", actionTitle: "Ok", vc: self)
        }else{
            loginApi()
        }
            
    }
    
    @IBAction func btnForgotPswd(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotPswdVC") as! ForgotPswdVC
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnToRegister(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - Apple Login
    @available(iOS 13.0, *)
    func signInWithApple(){
      let appleId = ASAuthorizationAppleIDProvider()
        let request = appleId.createRequest()
        request.requestedScopes = [.email,.fullName]
        let authController = ASAuthorizationController(authorizationRequests: [request])
        authController.presentationContextProvider = self
        authController.delegate = self
        authController.performRequests()
    }
    
    @available(iOS 13.0, *)
    @IBAction func btnSignInApple(_ sender: UIButton) {
        signInWithApple()
    }
    //MARK: - FB Login
    func fbLoginAction() {
        let fbLoginManager = LoginManager()
        
        fbLoginManager.logIn(permissions: ["email","public_profile"] , from: self) { (result, error) in
            if (error == nil){
                let fbloginresult = result!
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email")) {
                    self.getFBUserData()
                }
            }
        }
        
    }
    func getFBUserData(){
        if((AccessToken.current) != nil){
            
            GraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email, first_name, last_name, picture.type(large)"]).start(completion: { (connection, result, error) -> Void in
                if error == nil{
                    let response = result! as? [String : Any]
                    let id = response?["id"] as? String
                    print(response)
                    self.userLogin(userId: id ?? "", type: "facebook")
                }else{
                    print("Error")
                }

            })
           
        }
    }
    func userLogin(userId : String , type : String){
        let parameters : Parameters = [
            .id : userId,
            .type : type
        ]
        print(parameters)
        let request = APIManager.shared.createDataRequest(with: .post, params: parameters, api: .social_login, baseURL: .base)
        URLSession.shared.userLogin(with: request) { success, msg , userdata in
            DispatchQueue.main.async { [self] in
                if success{
                    print(msg)
                    print(success)
                    print(userdata)
                    let storyBoard = UIStoryboard(name: "Guide", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "GuideMenuVC") as! GuideMenuVC
                    navigationController?.pushViewController(vc, animated: true)
                }else{
                    print(msg)
                    
                }
            }
        }
    }
}
//MARK: - Google Login
extension LoginVC : GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error == nil{
            print(user.profile.name)
            UserDefaults.standard.set(user.profile.name, forKey: "Username")
            userLogin(userId: user.userID, type: "google")
        }else{
            print("Error")
        }
        
    }
    
}
//MARK: - Apple Login Delegate
@available(iOS 13.0, *)
extension LoginVC : ASAuthorizationControllerDelegate ,ASAuthorizationControllerPresentationContextProviding {
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return view.window!
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("Error :",error.localizedDescription)
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleCredential = authorization.credential as? ASAuthorizationAppleIDCredential{
            
            let userIdentifier = appleCredential.user
            let userEmail = appleCredential.email
            let userFullName = appleCredential.fullName
            
//            UserDefaults.standard.setValue(userEmail, forKey: Constant.UserDefaultkeys.email)
            print("\(userIdentifier)","\(userEmail ?? "")","\(String(describing: userFullName))")
            userLogin(userId: userIdentifier , type: "apple")
            UserDefaults.standard.set(userFullName, forKey: "Username")
//            self.performSegue(withIdentifier:Constant.segueIdentifier.garageVc, sender: self)
        }
    }
    
}


extension LoginVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField.text == txtMobile.text{
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}
