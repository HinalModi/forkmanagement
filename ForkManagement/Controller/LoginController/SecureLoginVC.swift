//
//  SecureLoginVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 15/11/22.
//

import UIKit

class SecureLoginVC: UIViewController {

    @IBOutlet weak var txtPswd: VCLanguageTextField!
    @IBOutlet weak var backBtn: VCLanguageButton!
    @IBOutlet weak var lblFaceId: UILabel!
    @IBOutlet weak var imgDone: UIImageView!
    @IBOutlet weak var blurBgImage: UIImageView!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    var tap = UITapGestureRecognizer()
    override func viewDidLoad() {
        super.viewDidLoad()

        btnLogin.rounded(cornerRadius: 10)
        btnRegister.rounded(cornerRadius: 10)
        tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        blurBgImage.addGestureRecognizer(tap)
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            backBtn.setImage(UIImage(named: "image 187"), for: .normal)
            txtPswd.setLeftPaddingPoints(10)
            txtPswd.setRightPaddingPoints(60)
        }else{
            backBtn.setImage(UIImage(named: "rightArrow"), for: .normal)
            txtPswd.setLeftPaddingPoints(60)
            txtPswd.setRightPaddingPoints(10)
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        UIView.animate(withDuration: 0.1) {
            self.imgDone.isHidden = true
            self.blurBgImage.isHidden = true
            self.lblFaceId.isHidden = true
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        blurBgImage.addBlur()
    }
   
    @IBAction func btnToForgotPswd(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotPswdVC") as! ForgotPswdVC
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnToRegister(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func login(_ sender: UIButton) {
//        UIView.animate(withDuration: 0.1) {
//            self.imgDone.isHidden = false
//            self.blurBgImage.isHidden = false
//            self.lblFaceId.isHidden = false
//        }
        let storyBoard = UIStoryboard(name: "Guide", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "GuideMenuVC") as! GuideMenuVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
protocol Blurable {
    func addBlur(_ alpha: CGFloat)
}

extension Blurable where Self: UIView {
    func addBlur(_ alpha: CGFloat = 0.45) {
        // create effect
        let effect = UIBlurEffect(style: .regular)
        let effectView = UIVisualEffectView(effect: effect)
        
        // set boundry and alpha
        effectView.frame = self.bounds
        effectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        effectView.alpha = alpha
        
        self.addSubview(effectView)
    }
}

// Conformance
extension UIView: Blurable {}
