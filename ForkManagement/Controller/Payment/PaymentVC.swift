//
//  PaymentVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 05/01/23.
//

import UIKit

class PaymentVC: UIViewController {

    @IBOutlet weak var activityInc: UIActivityIndicatorView!
    @IBOutlet weak var progressImg: UIImageView!
    @IBOutlet weak var progressFrntImg: UIImageView!
    @IBOutlet weak var summaryDashImg: UIImageView!
    @IBOutlet weak var btnPay: VCLanguageButton!
    @IBOutlet weak var cashBgView: UIView!
    @IBOutlet weak var outCashImg: UIImageView!
    @IBOutlet weak var inCashImg: UIImageView!
    @IBOutlet weak var inCrdImg: UIImageView!
    @IBOutlet weak var outCrdImg: UIImageView!
    @IBOutlet weak var creditBgView: UIView!
    var orderId = String()
    var paymentType = "cash"
    var totalPrice = String()
    var book_table_id = String()
    var flowType = String()
    var isfromPickUp = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnPay.rounded(cornerRadius: 5)
        cashBgView.rounded(cornerRadius: 2)
        creditBgView.rounded(cornerRadius: 2)
        UIView.animate(withDuration: 0.1) {
            self.summaryDashImg.image = UIImage(named: "greenDash")
            self.progressFrntImg.image = UIImage(named: "greenDash")
            self.progressImg.image = UIImage(named: "complete")
        }
        btnPay.setTitle("Pay-\(totalPrice)", for: .normal)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    @IBAction func btnSelCash(_ sender: UIButton) {
        inCashImg.isHidden = false
        inCrdImg.isHidden = true
        cashBgView.backgroundColor = UIColor(named: "yellow")
        creditBgView.backgroundColor = UIColor(named: "PaymentBg")
        paymentType = "cash"
    }
    
    @IBAction func btnSelCredit(_ sender: UIButton) {
        inCrdImg.isHidden = false
        inCashImg.isHidden = true
        creditBgView.backgroundColor = UIColor(named: "yellow")
        cashBgView.backgroundColor = UIColor(named: "PaymentBg")
        paymentType = "online"
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        if isfromPickUp{
            self.navigationController?.popToViewController(ofClass: BookingTabScrVC.self)
        }else{
            self.navigationController?.popToViewController(ofClass: BookingTabDetailVC.self)
        }
    }
    @IBAction func btnPayment(_ sender: UIButton) {
        payment()
    }
    func payment(){
        activityInc.startAnimating()
        let params : Parameters = [
            .order_id : "\(orderId)",
            .payment_type : paymentType,
            .book_table_id : "\(book_table_id)",
            .type : flowType
        ]
        print(params)
        let request = APIManager.shared.createDataRequestTwo(with: .post, params: params, api: .payment, baseURL: .base, setToken: "Bearer " + userInfo.token)
        print(request)
        URLSession.shared.makeApiCall(with: request) { isSuccess, message, book in
            DispatchQueue.main.async { [self] in
                self.activityInc.stopAnimating()
                if isSuccess{
                    print(message)
                    self.view.makeToast(message)
                    let orderdata = book["data"] as! [String : Any]
                    let order_Id = orderdata["order_id"] as? Int ?? 0
                    let type = orderdata["type"] as? String ?? ""
                    
                    if isfromPickUp {
                        let storyBoard = UIStoryboard(name: "WalkInPickUp", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "PickUpConfirmVC") as! PickUpConfirmVC
                        vc.orderId = order_Id
                        navigationController?.pushViewController(vc, animated: true)
                    }else{
                        let storyBoard = UIStoryboard(name: "Guide", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "ConfirmOrderScrVC") as! ConfirmOrderScrVC
                        vc.orderId = order_Id
                        navigationController?.pushViewController(vc, animated: true)
                    }
                }else{
                    print(message)
                }
            }
        }
    }
}
extension UINavigationController {
  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
      popToViewController(vc, animated: animated)
    }
  }
}
