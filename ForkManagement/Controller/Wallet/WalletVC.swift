//
//  WalletVC.swift
//  ForkManagement
//
//  Created by Ankit Dave on 28/11/22.
//

import UIKit
import LGSideMenuController
class WalletVC: UIViewController {

    @IBOutlet weak var couponTV: UITableView!
    @IBOutlet weak var tabHeight: NSLayoutConstraint!
    @IBOutlet weak var cardView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        cardView.rounded(cornerRadius: 10)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async {
            self.tabHeight.constant = self.couponTV.contentSize.height
            self.view.layoutIfNeeded()
            self.tabHeight.constant = self.couponTV.contentSize.height
        }
    }

    @IBAction func btnMenu(_ sender: UIButton) {
        if VCLangauageManager.sharedInstance.isLanguageEnglish(){
            sideMenuController?.showLeftView()
        }else{
            sideMenuController?.showRightView()
        }
    }
}
extension WalletVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletTVC") as! WalletTVC
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
}
