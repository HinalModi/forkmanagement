//
//  ExtensionHelper.swift
//  ExtensionHelper
//
//  Created by Vikram Chaudhary on 24/02/21.
//  Copyright © 2020 CMExpertise Infotech PVT LTD. All rights reserved.
//

import UIKit

extension UIViewController {
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPopToLoginAction(_ sender: UIButton) {
        self.popToLogin()
    }
    
    func popToLogin() {
        var viewControllers = self.navigationController?.viewControllers
//        if let vc = viewControllers?.first(where: {$0 is LoginVC }) {
//            self.navigationController?.popToViewController(vc, animated: true)
//        } else {
//            viewControllers = self.tabBarController?.navigationController?.viewControllers
//            if let vc = viewControllers?.first(where: {$0 is SplashVC }) {
//                self.tabBarController?.navigationController?.popToViewController(vc, animated: true)
//            }
//        }
    }
    
    func selectDate(minDate: Date?, maxDate: Date?, selectTime: Bool = false, result: @escaping (Date) -> Void) {
        let vc = Constant.StoryBoard.datePicker.instantiateViewController(withIdentifier: "DatePickerVC") as! DatePickerVC
        vc.modalPresentationStyle = .overFullScreen
        vc.selectTime = selectTime
        vc.minDate = minDate
        vc.maxDate = maxDate
        vc.dateBlock = { date in
            result(date)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func selectYear(result: @escaping (String) -> Void) {
        let vc = Constant.StoryBoard.datePicker.instantiateViewController(withIdentifier: "YearPickerVC") as! YearPickerVC
        vc.modalPresentationStyle = .overFullScreen
        vc.dateBlock = { date in
            result(date)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func sharePost(with data: String, image: UIImage = UIImage()) {
        let shareAll = [image] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func shareOnWhatsApp (data:String) {
        let message = data
        let urlStringEncoded = message.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        if let url  = URL(string: "whatsapp://send?text=\(urlStringEncoded!)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    func shareLink (data:String) {
        let message = data
        let dataToShare = [message]
        let activityViewController = UIActivityViewController(activityItems: dataToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        self.present(activityViewController, animated: true, completion: nil)
    }
}



extension Int {
    var ordinal: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .ordinal
        return formatter.string(from: NSNumber(value: self)) ?? ""
    }
}

extension Array {
    
    func filterDuplicates(includeElement: (_ lhs:Element, _ rhs:Element) -> Bool) -> [Element]{
        var results = [Element]()
        
        forEach { (element) in
            let existingElements = results.filter {
                return includeElement(element, $0)
            }
            if existingElements.count == 0 {
                results.append(element)
            }
        }
        
        return results
    }
}
extension String{
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-=().!_")
        return text.filter {okayChars.contains($0) }
    }
}

extension UIView {
    
    func takeScreenshot() -> UIImage {
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image ?? UIImage()
    }
    func roundedCorners(corners: UIRectCorner, radius: CGFloat) {
         let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         layer.mask = mask
     }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}


extension UITextField{
    
    func setPlaceholder() {
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: Constant.Color.gray,NSAttributedString.Key.font:UIFont(name: "Mulish-Regular", size:15.0) ?? .systemFont(ofSize: 15.0)])
    }
}

extension UITextView {
    
    func setPlaceholder(placeholderString:String) {
        
        let placeholderLabel = UILabel()
        placeholderLabel.text = placeholderString
        placeholderLabel.font = UIFont(name:"Mulish-Regular", size: 15.0)
        placeholderLabel.sizeToFit()
        placeholderLabel.tag = 222
        placeholderLabel.frame.origin = CGPoint(x:2.5, y: (self.font?.pointSize)! / 2)
        placeholderLabel.textColor = Constant.Color.gray
        placeholderLabel.isHidden = !self.text.isEmpty
        
        self.addSubview(placeholderLabel)
    }
    
    func checkPlaceholder() {
        let placeholderLabel = self.viewWithTag(222) as! UILabel
        placeholderLabel.isHidden = !self.text.isEmpty
    }
}

extension UIView {
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }
    
    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
}
