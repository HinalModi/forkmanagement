//
//  UIView+Utils.swift
//  UIView+Utils
//
//  Created by Vikram Chaudhary on 24/05/18.
//  Copyright © 2020 CMExpertise Infotech PVT LTD. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addAndFitSubview(view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        let views = ["view": view]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: views))
    }
    
    func roundedUsingWidth() {
        self.rounded(cornerRadius: self.frame.size.width/2)
    }
    
    func roundedUsingHeight() {
        self.rounded(cornerRadius: self.frame.size.height/2)
    }
    
    func rounded(cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius
    }
    
    func corner(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func screenshot() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: self.bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func applyShadow(
        apply: Bool,
        color: UIColor = UIColor.black,
        offset: CGSize = CGSize(width: 0.0, height: 2.0),
        opacity: Float = 0.2, radius: Float = 1.0,
        shadowRect: CGRect? = nil) {
        self.layer.shadowColor = apply ? color.cgColor : UIColor.white.withAlphaComponent(0.0).cgColor
        self.layer.shadowOffset = apply ? offset : CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = apply ? opacity : 0.0
        self.layer.shadowRadius = apply ? CGFloat(radius) : 0.0
        self.layer.masksToBounds = false
        if let shadowRect = shadowRect {
            self.layer.shadowPath = UIBezierPath(rect: shadowRect).cgPath
        }
    }
    
    func applyGlow(apply: Bool, color: UIColor) {
        self.layer.shadowColor = apply ? color.cgColor : UIColor.white.withAlphaComponent(0.0).cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = apply ? 0.4 : 0.0
        self.layer.shadowRadius = apply ? 10.0 : 0.0
        self.layer.masksToBounds = false
    }
    
    var nibName: String {
        return type(of: self).description().components(separatedBy: ".").last! // to remove the module name and get only files name
    }
    
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: self.nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    func setBorder(borderColor: UIColor, borderWidth: CGFloat = 1) {
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
    }
    
    var roundCorner: Bool {
        get {
            return false
        }
        set {
            if newValue {
                self.roundedUsingHeight()
            }
        }
    }
    
    
    var cornerRadius: CGFloat {
        get {
            return 0
        }
        set {
            if !roundCorner {
                self.layer.cornerRadius = newValue
                self.clipsToBounds = true
                self.layer.masksToBounds = true
                //                self.rounded(cornerRadius: newValue)
            }
        }
    }
    
    var leftCornerRadious: CGFloat {
        get {
            return 0
        }
        set {
            if !roundCorner {
                self.setCustomCorner([.bottomLeft, .topLeft], radious: newValue)
            }
        }
    }
    
    var rightCornerRadious: CGFloat {
        get {
            return 0
        }
        set {
            if !roundCorner {
                self.setCustomCorner([.bottomRight, .topRight], radious: newValue)
            }
        }
    }
    
    var topCornerRadius: CGFloat {
        get {
            return 0
        }
        set {
            if !roundCorner && !(cornerRadius > 0) {
                self.layoutIfNeeded()
                self.corner(corners: [.topLeft, .topRight], radius: newValue)
            }
        }
    }
    
    var bottomCornerRadius: CGFloat {
        get {
            return 0
        }
        set {
            if !roundCorner && !(cornerRadius > 0) {
                self.layoutIfNeeded()
                self.corner(corners: [.bottomLeft, .bottomRight], radius: newValue)
            }
        }
    }
    
    fileprivate func setCustomCorner(_ corners: UIRectCorner, radious: CGFloat) {
        DispatchQueue.main.async {
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radious, height: radious))
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
        }
    }
    
    func setShadow(shadowColor: UIColor, shadowOpacity: Float, shadowRadius: CGFloat, shadowOffset: CGSize) {
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOffset = shadowOffset
        self.layer.masksToBounds = false
    }
    
    func setGradiant(with colors: [CGColor], locations: [NSNumber], start: CGPoint, end: CGPoint) {
        removeGradiantBorder()
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = colors
        gradient.locations = locations
        gradient.startPoint = start
        gradient.endPoint = end
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        gradient.cornerRadius = self.roundCorner ? bounds.size.height :  self.cornerRadius
        layer.sublayers?.removeAll(where: {$0.accessibilityLabel == "gradBorderLayer"})
        layer.insertSublayer(gradient, at: 0)
    }
    
    func setGradiantBorder(with colors: [CGColor], locations: [NSNumber], start: CGPoint, end: CGPoint, with color: UIColor, width: CGFloat, corner: CGFloat) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = colors
        gradient.locations = locations
        gradient.startPoint = start
        gradient.endPoint = end
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        gradient.cornerRadius = self.roundCorner ? bounds.size.height :  self.cornerRadius
        gradient.accessibilityLabel = "gradBorderLayer"
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = width
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: corner).cgPath
        gradient.mask = shapeLayer
        
        self.removeGradiantBorder()
        layer.insertSublayer(gradient, at: 0)
    }
    
    func removeGradiantBorder() {
        layer.sublayers?.removeAll(where: {$0.accessibilityLabel == "gradBorderLayer"})
    }
    
    func addDashedBorder(with color: UIColor, width: CGFloat, corner: CGFloat) {
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = width
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [8,8]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: corner).cgPath
        shapeLayer.accessibilityLabel = "dashedBorder"
        
        self.layer.sublayers?.forEach({ (layerWD) in
            if layerWD.accessibilityLabel == "dashedBorder" {
                layerWD.removeFromSuperlayer()
            }
        })
        
        self.layer.addSublayer(shapeLayer)
        
    }
    
    func setCornerRadius(with corners: UIRectCorner, radius: CGFloat = 3.0) {
        DispatchQueue.main.async {
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    }
    
    func setTabShadow() {
        layer.shadowColor = UIColor.black.withAlphaComponent(0.05).cgColor
        layer.shadowOpacity = 1
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: 0, height: -5)
    }
    
    func gradiantDashboardView(colors: [CGColor]) {
        let layer0 = CAGradientLayer()
        layer0.colors = colors
        layer0.locations = [0, 1]
        layer0.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer0.endPoint = CGPoint(x: 0.75, y: 0.5)
        layer0.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 0, b: 1, c: -1, d: 0, tx: 1, ty: 0))
        layer0.bounds = self.bounds.insetBy(dx: -0.5*self.bounds.size.width, dy: -0.5*self.bounds.size.height)
        layer0.position = self.center
        layer0.accessibilityLabel = "gradBorderLayer"
        self.layer.sublayers?.removeAll(where: {$0.accessibilityLabel == "gradBorderLayer"})
        self.layer.addSublayer(layer0)
    }
}

//class VCTextField: UITextField {
//    var insetX: CGFloat = 10
//    var insetY: CGFloat = 0
//
//    var placeHolderColor: UIColor = .black {
//        didSet {
//            self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
//                                                            attributes: [NSAttributedString.Key.foregroundColor: placeHolderColor])
//        }
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//    }
//
//    override func draw(_ rect: CGRect) {
//        super.draw(rect)
//    }
//
//    // placeholder position
//    override func textRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.insetBy(dx: insetX, dy: insetY)
//    }
//
//    // text position
//    override func editingRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.insetBy(dx: insetX, dy: insetY)
//    }
//}
