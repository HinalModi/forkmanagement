//
//  UIMessageCollectionView.swift
//  UIMessageCollectionView
//
//  Created by Vikram Chaudhary on 21/08/20.
//  Copyright © 2020 CMExpertise Infotech PVT LTD. All rights reserved.
//

import UIKit

class UIMessageCollectionView: UICollectionView {
    private let refreshCtrl = UIRefreshControl()
    @IBOutlet var refreshDelegate: UICollectionViewRefreshDelegate?
    
    @IBInspectable var refreshMessage: String = "Loading" {
        didSet {
            refreshCtrl.attributedTitle = NSAttributedString(string: refreshMessage)
        }
    }
    
    @IBInspectable var allowRefresh: Bool = false {
        didSet {
            if allowRefresh {
                refreshCtrl.attributedTitle = NSAttributedString(string: refreshMessage)
                refreshCtrl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
                self.addSubview(refreshCtrl)
            }
        }
    }
    
    @IBInspectable var setNoDataLabel: Bool = false {
        didSet {
            if setNoDataLabel {
                self.alwaysBounceVertical = true
                setNeedsDisplay()
                if self.numberOfItems(inSection: 0) == 0 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        self.setNoDataLabelUI()
                    }
                }
            }
        }
    }
    
    @IBInspectable var noDataImage: UIImage = UIImage() {
        didSet {
            if setNoDataLabel {
                self.alwaysBounceVertical = true
                setNeedsDisplay()
                if self.numberOfItems(inSection: 0) == 0 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        self.setNoDataLabelUI()
                    }
                }
            }
        }
    }
    
    @IBInspectable var noDataMessage: String = "No data available" {
        didSet {
            if setNoDataLabel {
                self.alwaysBounceVertical = true
                setNeedsDisplay()
                if self.numberOfItems(inSection: 0) == 0 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        self.setNoDataLabelUI()
                    }
                }
            }
        }
    }
    
    @IBInspectable var noDataFontSize: CGFloat = 25 {
        didSet {
            if setNoDataLabel {
                self.alwaysBounceVertical = true
                setNeedsDisplay()
                if self.numberOfItems(inSection: 0) == 0 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        self.setNoDataLabelUI()
                    }
                }
            }
        }
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setNeedsDisplay()
    }
    
    override func reloadData() {
        super.reloadData()
        self.endRefreshing()
        self.setNeedsDisplay()
        if self.numberOfItems(inSection: 0) > 0 {
            self.subviews.forEach { (view) in
                if view.accessibilityIdentifier == "NodataLabel" {
                    view.removeFromSuperview()
                }
            }
        }
    }
    
    @objc func refresh(_ sender: AnyObject) {
        if let refDelegate = refreshDelegate {
            refDelegate.refresh?(collectionView: self)
        }
    }
    
    func endRefreshing() {
        refreshCtrl.endRefreshing()
    }
    
    func setNoDataLabelUI() {
        
        self.subviews.forEach { (view) in
            if view.accessibilityIdentifier == "NodataLabel" {
                view.removeFromSuperview()
            }
        }
        let view = UIView(frame: self.bounds)
        //        view.backgroundColor = UIColor.init(named: "bgColor")
        view.accessibilityIdentifier = "NodataLabel"
        
        if noDataImage.size.width > 0 {
            let imageView = UIImageView(image: noDataImage)
            imageView.frame = CGRect(x: 0, y: 0, width: 100, height: 200)
            view.addSubview(imageView)
            imageView.contentMode = .scaleAspectFit
            // you need to turn off autoresizing masks (storyboards do this automatically)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            
            // setup constraints, it is recommended to activate them through `NSLayoutConstraint.activate`
            // instead of `constraint.isActive = true` because of performance reasons
            NSLayoutConstraint.activate([
                imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -((Constant.Screen.Width * 0.3) / 2)),
                imageView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.7),
                imageView.heightAnchor.constraint(equalToConstant: Constant.Screen.Width * 0.7),
            ])
            
            let label = UILabel(frame: view.bounds)
            label.text = noDataMessage
            label.font = UIFont(name: "Rubik-Regular", size: noDataFontSize)
            label.textColor = UIColor(named: "TextDarkGray")
            label.accessibilityIdentifier = "NodataLabel"
            label.textAlignment = .center
            label.numberOfLines = 0
            view.addSubview(label)
            
            label.translatesAutoresizingMaskIntoConstraints = false
            
            NSLayoutConstraint.activate([
                label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                label.heightAnchor.constraint(equalToConstant: 100),
                label.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 30),
                label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
                label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            ])
            
        } else {
            let label = UILabel(frame: view.bounds)
            label.text = "No data available"
            label.font = UIFont(name: "Rubik-Regular", size: noDataFontSize)
            label.textColor = UIColor(named: "TextDarkGray")
            label.accessibilityIdentifier = "NodataLabel"
            label.textAlignment = .center
            view.addSubview(label)
        }
        
        self.addSubview(view)
    }
}
