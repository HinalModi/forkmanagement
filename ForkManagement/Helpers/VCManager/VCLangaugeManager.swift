//
//  VCLanguageManager.swift
//  LanguageSupport
//
//  Created by mac on 17/04/21.
//  Copyright © 2018 CMExpertise Infotech. All rights reserved.
//

import Foundation
import UIKit

enum VCAppLanguages : String{
    case english = "en",
    arabic = "ar-AE"
}

class VCLangauageManager: NSObject {
    static let sharedInstance = VCLangauageManager()
    
    struct Language {
        static let device = NSLocale.current.languageCode
        static let app = Locale.preferredLanguages[0]
    }
    
    func changeAppLanguage(To newLang: VCAppLanguages){
        UserDefaults.standard.set(newLang.rawValue, forKey: "app_language")
        UserDefaults.standard.synchronize()
        
        setAppAppearanceWithCurrentLanguage()
    }
    
    func setAppAppearanceWithCurrentLanguage(){
        let aNewAttr : UISemanticContentAttribute = isLanguageEnglish() ? .forceLeftToRight : .forceRightToLeft
        UIView.appearance().semanticContentAttribute = aNewAttr
    }
    
    func isLanguageEnglish() -> Bool{
        return getCurrentAppLanguage() == VCAppLanguages.english.rawValue
    }
    func isLanguageArabic() -> Bool{
        return getCurrentAppLanguage() == VCAppLanguages.arabic.rawValue
    }
    func getCurrentAppLanguage() -> String{
        if let _ = UserDefaults.standard.string(forKey: "app_language") {} else {
            // we set a default, just in case
            UserDefaults.standard.set(VCAppLanguages.english.rawValue, forKey: "app_language")
            UserDefaults.standard.synchronize()
        }
        
        return UserDefaults.standard.string(forKey: "app_language")!
    }
    
}

extension String {
    
    ///Returns localized version of string
    func localize() -> String{
        
        if let _ = UserDefaults.standard.string(forKey: "app_language") {} else {
            // we set a default, just in case
            UserDefaults.standard.set(VCAppLanguages.english.rawValue, forKey: "app_language")
            UserDefaults.standard.synchronize()
        }
        
        let lang = UserDefaults.standard.string(forKey: "app_language")
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        //        if self.contains("AED"){
        //            return NSLocalizedString("AED", tableName: nil, bundle: bundle!, value: "", comment: "") + " " + self.replacingOccurrences(of: "AED", with: "").replacingOccurrences(of: " ", with: "")
        //        }
        
        let aStrToReturn = NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        
        //        if aStrToReturn.contains("AED"){
        //            return self.replacingOccurrences(of: "AED", with: "") + " " + NSLocalizedString("AED", tableName: nil, bundle: bundle!, value: "", comment: "")
        //        }
        
        
        return aStrToReturn
    }
}
