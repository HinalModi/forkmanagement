//
//  VCLanguageControl.swift
//  TouristoApp
//
//  Created by mac on 17/04/21.
//


import Foundation
import UIKit

class VCLanguageTextView: UITextView {
    
    @IBInspectable var shouldAlignRightOnArabic : Bool = true{
        didSet{
            // if true then it is defult true to not need to set anything
            guard !shouldAlignRightOnArabic else { return }
            textAlignment = .center
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        text = text?.localize()
    }
    
    override var text: String?{
        didSet{
            checkForLanguageDirection()
            
            guard text != text?.localize() else { return }
            text = text?.localize()
        }
    }
    
    func checkForLanguageDirection(){
        guard shouldAlignRightOnArabic else { return }
        
        //        print("\(text) = \(shouldAlignRightOnArabic ? "True" : "False")")
        
        textAlignment = VCLangauageManager.sharedInstance.isLanguageEnglish() ? .left : .right
    }
}

class VCLanguageSearchBar: UISearchBar, UISearchBarDelegate {
    override init(frame: CGRect) {
        super.init(frame: frame)
        UISetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        UISetup()
    }
    
    func UISetup(){
        let searchTextField: UITextField = self.value(forKey: "_searchField") as! UITextField
        if VCLangauageManager.sharedInstance.isLanguageEnglish() {
            searchTextField.textAlignment = .left
        } else {
            searchTextField.textAlignment = .right
        }
        self.placeholder = self.placeholder?.localize()
        setValue("Cancel".localize(), forKey:"_cancelButtonText")
    }
}

class VCLanguageButton: UIButton {
    
    @IBInspectable var shouldAlignRightOnArabic : Bool = true{
        didSet{
            // if true then it is defult true to not need to set anything
            guard shouldAlignRightOnArabic else { return }
            if VCLangauageManager.sharedInstance.isLanguageEnglish() {
                contentHorizontalAlignment = .left
            } else {
                contentHorizontalAlignment = .right
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        UISetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        UISetup()
    }
    
    func UISetup(){
        let aTitle = titleLabel?.text
        
        setTitle(aTitle?.localize(), for: .normal)
    }
    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title, for: state)
        
        guard title != title?.localize() else { return }
        setTitle(title?.localize(), for: state)
    }
}

class VCLanguageImageButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        UISetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        UISetup()
    }
    
    func UISetup(){
        let aTitle = titleLabel?.text
        if !VCLangauageManager.sharedInstance.isLanguageEnglish() {
            transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        setTitle(aTitle?.localize(), for: .normal)
    }
    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title, for: state)
        
        guard title != title?.localize() else { return }
        setTitle(title?.localize(), for: state)
    }
}

class VCLanguageImage: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        UISetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        UISetup()
    }

    func UISetup(){
        if !VCLangauageManager.sharedInstance.isLanguageEnglish() {
            transform = CGAffineTransform(rotationAngle: .pi)
        }
    }
}

class VCLanguageImg: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        UISetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        UISetup()
    }
    
    func UISetup(){
        if !VCLangauageManager.sharedInstance.isLanguageEnglish() {
            transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
}

class VCLanguageLabel: UILabel {
    
    @IBInspectable var shouldAlignRightOnArabic : Bool = true{
        didSet{
            // if true then it is defult true to not need to set anything
            guard !shouldAlignRightOnArabic else { return }
            textAlignment = .center
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        text = text?.localize()
    }
    
    override var text: String?{
        didSet{
            checkForLanguageDirection()
            
            guard text != text?.localize() else { return }
            text = text?.localize()
        }
    }
    
    
    func checkForLanguageDirection(){
        guard shouldAlignRightOnArabic else { return }
        
//                print("\(text) = \(shouldAlignRightOnArabic ? "True" : "False")")
        
        textAlignment = VCLangauageManager.sharedInstance.isLanguageEnglish() ? .left : .right
    }
}

class VCLanguageLabelRight: UILabel {
    
    @IBInspectable var shouldAlignRightOnArabic : Bool = true{
        didSet{
            // if true then it is defult true to not need to set anything
            guard !shouldAlignRightOnArabic else { return }
            textAlignment = .center
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        text = text?.localize()
    }
    
    override var text: String?{
        didSet{
            checkForLanguageDirection()
            
            guard text != text?.localize() else { return }
            text = text?.localize()
        }
    }
    
    
    func checkForLanguageDirection(){
       // guard shouldAlignRightOnArabic else { return }
        
        //                print("\(text) = \(shouldAlignRightOnArabic ? "True" : "False")")
        
        textAlignment = VCLangauageManager.sharedInstance.isLanguageEnglish() ? .right : .left
    }
}

class VCLanguageTextField: UITextField {
    
    @IBInspectable var shouldAlignRightOnArabic : Bool = true{
        didSet{
            // if true then it is defult true to not need to set anything
            guard !shouldAlignRightOnArabic else { return }
            textAlignment = .center
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        text = text?.localize()
    }
    
    override var text: String?{
        didSet{
            checkForLanguageDirection()
            
            guard text != text?.localize() else { return }
            text = text?.localize()
        }
    }
    
    override var placeholder: String? {
        didSet{
            checkForLanguageDirection()
            
            guard placeholder != placeholder?.localize() else { return }
            placeholder = placeholder?.localize()
        }
    }
    
    func checkForLanguageDirection(){
        guard shouldAlignRightOnArabic else { return }
        
        //        print("\(text) = \(shouldAlignRightOnArabic ? "True" : "False")")
        textAlignment = VCLangauageManager.sharedInstance.isLanguageEnglish() ? .left : .right
    }
}

