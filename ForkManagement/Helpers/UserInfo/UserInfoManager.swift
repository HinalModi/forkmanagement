//
//  UserInfoManager.swift
//  UserInfoManager
//
//  Created by Vikram Chaudhary on 08/04/20.
//  Copyright © 2020 CMExpertise Infotech PVT LTD. All rights reserved.
//

import Foundation

let userInfoKey: String = "userInfoModel"

class UserInfoManager: NSObject {
    
    //MARK:- Setter Method
    class func setUserInfo(userInfoModel: UserData!) {
        if let user = userInfoModel {
            let archivedServerModules = NSKeyedArchiver.archivedData(withRootObject: user)
            UserDefaults.standard.setValue(archivedServerModules, forKey: userInfoKey)
            UserDefaults.standard.synchronize()
        }
    }

    //MARK:- Getter Method
    class func getUserInfoModel() -> UserData! {
        let archivedServerModules: AnyObject? = UserDefaults.standard.value(forKey: userInfoKey) as AnyObject?
        
        if archivedServerModules == nil {
            return nil
        }
        if let data = archivedServerModules as? NSData {
            if let userInfoModel = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? UserData {
                return userInfoModel
            } }
        return nil
    }
    
    //MARK:- Remove user's info
    class func removeUserInfo() {
        UserDefaults.standard.removeObject(forKey: userInfoKey)
        UserDefaults.standard.synchronize()
    }
}
