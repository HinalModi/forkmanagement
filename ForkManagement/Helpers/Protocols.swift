//
//  Protocols.swift
//  Protocols
//
//  Created by Vikram Chaudhary on 21/08/20.
//  Copyright © 2020 CMExpertise Infotech PVT LTD. All rights reserved.
//

import UIKit

@objc public protocol UICollectionViewRefreshDelegate : NSObjectProtocol {
    @objc optional func refresh(collectionView: UICollectionView)
}

@objc public protocol UITableViewRefreshDelegate : NSObjectProtocol {
    @objc optional func refresh(tableView: UITableView)
}
