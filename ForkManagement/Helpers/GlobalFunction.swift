//
//  GlobalFunction.swift
//  GlobalFunction
//
//  Created by Vikram Chaudhary on 25/03/20.
//  Copyright © 2020 CMExpertise Infotech PVT LTD. All rights reserved.
//

import UIKit

class GlobalFunction: NSObject {
    
    static let businessType = [
        "Select Business Type",
        "Grocery",
        "Fruits & Vegetables",
        "General Store",
        "Dairy",
        "Pharmacy",
        "Bakery",
        "Apparel",
        "Stationary & Office Supplies",
        "Electrical Supplies",
        "Hardware Store",
        "Laundry",
        "Salon & Spa",
        "Restaurants & Food Joints",
        "Mobile and Accessories",
        "Electronics & Appliances",
        "Home & Kitchen",
        "Electrician",
        "Plumbers",
        "AC Services and Repair",
        "Appliance repair",
        "Painters",
        "Cleaning and Disinfection",
        "Carpenters",
        "Painter",
        "Pest Control",
        "Spa for Women",
        "Massage for Men",
        "Other"
    ]
    
    
    static func getPostString(params: [String: Any]) -> String {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
    static func printRes(data: Any, printDate: Bool = true) {
        var prefix = ""
        if printDate {
            print("\(Constant.appName) - \(Date()) ===========")
            prefix = Constant.appName + " "
        }
        print("\(prefix)\(data)")
    }
    
    static func userLogin(userId : String , type : String){
        let parameters : Parameters = [
            .id : userId,
            .type : type
        ]
        print(parameters)
        let request = APIManager.shared.createDataRequest(with: .post, params: parameters, api: .social_login, baseURL: .base)
        URLSession.shared.userLogin(with: request) { success, msg , userdata in
            DispatchQueue.main.async { [self] in
                if success{
                    print(msg)
                    print(success)
                    print(userdata)
                    
                }else{
                    print(msg)
                }
            }
        }
    }
    static func convertDateFormatter(date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        if let date = dateFormatter.date(from: date) {
            dateFormatter.dateFormat = "hh:mm a ,d MMM yyyy"///this is what you want to convert format
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            let timeStamp = dateFormatter.string(from: date)
            return timeStamp
        } else {
            return ""
        }
    }
    static func removeSpecialCharsFromString(text: String) -> String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-=[]().!_")
        return text.filter {okayChars.contains($0) }
    }
    static func getCountryList() -> NSMutableArray {
        var countryList = NSMutableArray()
        if let path = Bundle.main.path(forResource: "country-calling-codes", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let person = jsonResult["Data"] as? [Any] {
                    
                    countryList = NSMutableArray()
                    let list = NSMutableArray.init(array: person)
                    for i in 0..<list.count {
                        var dict: [String: Any] = list.object(at: i) as! [String: Any]
                        dict["flag"] = self.flag(country: dict["code"] as! String)
                        countryList.add(dict)
                    }
                }
            } catch {
                // handle error
            }
        }
        return countryList
    }
    
    static func flag(country:String) -> String {
        let base : UInt32 = 127397
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
    
    static func getDateInDMY(from dateString: String, shortFormat: Bool = false) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        if shortFormat {
            dateFormatter.dateFormat = "YYYY-MM-dd"
        }
        guard let date = dateFormatter.date(from: dateString) else {
            return dateString
        }
        dateFormatter.dateFormat = "dd MMM YYYY"
        let formatted = dateFormatter.string(from: date)
        return formatted
    }
    
    
    static func resize(_ image: UIImage) -> UIImage {
        var actualHeight = Float(image.size.height)
        var actualWidth = Float(image.size.width)
        let maxHeight: Float = 300.0
        let maxWidth: Float = 400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.5
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!) ?? UIImage()
    }
    
    static func getCurrentTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = ""
        return ""
    }
    
    static func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    static func setAlert(title : String , alertMessage : String ,actionTitle : String , vc : UIViewController){
        let alert = UIAlertController(title: title, message: alertMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: .default))
        vc.present(alert, animated: true)
    }
    static func checkInternet(vc : UIViewController , title : String , alertMessage : String , actionTitle : String){
        setAlert(title: title, alertMessage: alertMessage, actionTitle: actionTitle, vc: vc)
    }
    static func convertDateString(dateString : String!, fromFormat sourceFormat : String!, toFormat desFormat : String!) -> String {
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = sourceFormat
         let date = dateFormatter.date(from: dateString)
         dateFormatter.dateFormat = desFormat
        return dateFormatter.string(from: date ?? Date())
     }
}
