//
//  APIManager.swift
//  APIManager
//
//  Created by Vikram Chaudhary on 08/04/20.
//  Copyright © 2020 CMExpertise Infotech PVT LTD. All rights reserved.
//

import Foundation

public struct HTTPMethod: RawRepresentable, Equatable, Hashable {
    /// `GET` method.
    public static let get = HTTPMethod(rawValue: "GET")
    /// `POST` method.
    public static let post = HTTPMethod(rawValue: "POST")
    /// `DELETE` method.
    public static let delete = HTTPMethod(rawValue: "DELETE")
    /// `PUT` method.
    public static let put = HTTPMethod(rawValue: "PUT")
    
    public let rawValue: String
    
    public init(rawValue: String) {
        self.rawValue = rawValue
    }
}

enum RequestType {
    case form
    case raw
}

public struct Params: RawRepresentable, Equatable, Hashable {
    public static let phone                 = Params(rawValue: "phone")
    public static let password              = Params(rawValue: "password")
    public static let contact               = Params(rawValue: "contact")
    public static let name                  = Params(rawValue: "name")
    public static let email                 = Params(rawValue: "email")
    public static let c_password            = Params(rawValue: "c_password")
    public static let status_id             = Params(rawValue: "status_id")
    public static let token                 = Params(rawValue: "token")
    public static let password_confirmation = Params(rawValue: "password_confirmation")
    public static let gmail_token_id = Params(rawValue: "phone")
    public static let facebook_token_id = Params(rawValue: "phone")
    public static let login_type = Params(rawValue: "phone")
    public static let device_id = Params(rawValue: "phone")
    public static let device_token = Params(rawValue: "phone")
    public static let device_type = Params(rawValue: "phone")
    public static let image = Params(rawValue: "phone")
    public static let type = Params(rawValue: "type")
    public static let id = Params(rawValue: "id")
    public static let service_id = Params(rawValue: "service_id")
    public static let search = Params(rawValue: "search")
    public static let person = Params(rawValue: "person")
    public static let date = Params(rawValue: "date")
    public static let restaurant_id = Params(rawValue: "restaurant_id")
    public static let latitude = Params(rawValue: "latitude")
    public static let longitude = Params(rawValue: "longitude")
    public static let branch_id = Params(rawValue: "branch_id")
    public static let category_id = Params(rawValue: "category_id")
    public static let search_item = Params(rawValue: "search_item")
    public static let item_id = Params(rawValue: "item_id")
    public static let table_id = Params(rawValue: "table_id")
    public static let rules = Params(rawValue: "rules")
    public static let dresscode = Params(rawValue: "dresscode")
    public static let occasion = Params(rawValue: "occasion")
    public static let qty = Params(rawValue: "qty")
    public static let booking_table_id = Params(rawValue: "booking_table_id")
    public static let item_extra = Params(rawValue: "item_extra")
    public static let cart_item_id = Params(rawValue: "cart_item_id")
    public static let order_id = Params(rawValue: "order_id")
    public static let payment_type = Params(rawValue: "payment_type")
    public static let book_table_id = Params(rawValue: "book_table_id")
    public static let message = Params(rawValue: "message")
    public static let area = Params(rawValue: "area")
    public static let identifier = Params(rawValue: "identifier")
    public static let action = Params(rawValue: "action")
    public static let queue_id = Params(rawValue: "queue_id")
    public static let location_id = Params(rawValue: "location_id")
    public static let location = Params(rawValue: "location")
    public static let is_default = Params(rawValue: "is_default")
    public static let waiter = Params(rawValue: "waiter")
    public static let food = Params(rawValue: "food")
    public static let experience = Params(rawValue: "experience")
    public static let review = Params(rawValue: "review")
    public static let action_type = Params(rawValue: "action_type")
    public let rawValue: String
    public init(rawValue: String) {
        self.rawValue = rawValue
    }
}


enum BaseURLRole: String {
    case base = "https://staging.greatly-done.com/fork-mgmt/fork-management/api/v1/"
}

enum API: String {
    
    //Basic API's
    
    case login = "customer/login"
    case register = "customer/register"
    case otp = "/public/saathi/otp"
    case forget_password = "customer/forget-password"
    case reset_password = "customer/reset-password"
    case social_login = "customer/social-login"
    case restaurants = "customer/restaurants"
    case restaurant_details = "customer/restaurant-details"
    case food_list = "customer/food-list"
    case cat_item_list = "customer/category-item-list"
    case item_list = "customer/item-list"
    case booking_table = "customer/booking-table"
    case add_item_cart = "customer/add-item-cart"
    case get_cart_details = "customer/get-cart-details"
    case update_qty = "customer/update-qty"
    case remove_item_cart = "customer/remove-item-cart"
    case create_order = "customer/create-order"
    case get_order_details = "customer/get-order-details"
    case get_order = "customer/get-orders"
    case search_table = "customer/search-table"
    case payment = "customer/payment"
    case cmspage = "customer/cmspage"
    case contact = "customer/contact/save"
    case get_in_queue = "customer/get-in-queue"
    case queue_list = "customer/queue-list"
    case identifier = "customer/identifier"
    case queue_confirmation_action = "customer/queue-confirmation-action"
    case customer_queue_list = "customer/customer-queue-list"
    case location_list = "customer/location-list"
    case setdefault = "customer/location-setdefault"
    case location_delete = "customer/location-delete"
    case location_store = "customer/location-store"
    case add_review = "customer/add-review"
    case action = "customer/action"
    func url(for role: BaseURLRole) -> String {
        return role.rawValue + self.rawValue
    }
    func urltwo(for role: String) -> String {
        return role + self.rawValue
    }
}

struct Response {
    var data: Data?
    var response: URLResponse?
    var error: Error?
    
    var success: Bool = false
    var status: Int = 0
    var message: String = errorMessage
    var dicResponse: [String: Any] = [String: Any]()
    var arrResponse: [[String: Any]] = []
    
    init(with data: Data?, response: URLResponse?, error: Error?) {
        self.data = data
        self.response = response
        self.error = error
        self.success = false
        if error != nil {
            self.success = false
        } else {
            if self.data != nil {
                do {
                    let response = try JSONSerialization.jsonObject(with: self.data!, options: [])
                    print(response)
                    
                    guard let dic = response as? [String: Any] else {
                        return
                    }
                    GlobalFunction.printRes(data: dic)
                    self.dicResponse = dic
                    status = dic["status"] as? Int ?? 0
                    let success = dic["data"] as? Bool ?? false
                    self.message = dic["message"] as? String ?? errorMessage
                    if (status == 200) {
                        self.success = true
                    } else {
                        self.success = false
                    }
                } catch {
                    self.success = false
                    GlobalFunction.printRes(data: error)
                    print(String(describing: error))
                    GlobalFunction.printRes(data: error.localizedDescription)
                }
            }
        }
    }
    
    func getArrayResponse() -> [[String: Any]] {
        return self.dicResponse["data"] as? [[String: Any]] ?? []
    }
}

typealias Parameters = [Params: Any]

protocol UploadProgressDelegate: AnyObject {
    func progress(totalBytesExpectedToSend: Int64, sentFraction: Int64, totalSentFraction: Int64)
}

class APIManager: NSObject {
    
    static let shared = APIManager()
    var task: URLSessionDataTask?
    
    let allowedCharacterSet = (CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] ").inverted)
    var delegate: UploadProgressDelegate?
    
    private func getPostString(params:Parameters) -> String {
        var data = [String]()
        GlobalFunction.printRes(data: "", printDate: false)
        for(key, value) in params
        {
            GlobalFunction.printRes(data: "\(key.rawValue): \(value)", printDate: false)
            data.append(key.rawValue + "=\("\(value)".addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) ?? "")")
        }
        GlobalFunction.printRes(data: "", printDate: false)
        return data.map { String($0) }.joined(separator: "&")
    }
    
    func getPostDic(params:Parameters) -> [String: Any] {
        var data = [String: Any]()
        for(key, value) in params
        {
            data[key.rawValue] = value
        }
        GlobalFunction.printRes(data: data, printDate: false)
        return data
    }
    
    private func getRawParams(from params: Parameters) -> Data? {
        let dic = self.getPostDic(params: params)
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            return jsonData
        } catch {
            return nil
        }
    }
    
    private func getRawParamsDict(from params: Data) {
        do {
            let jsonData = try JSONSerialization.jsonObject(with: params, options: .mutableLeaves)
            print(jsonData)
        } catch {
            print(error)
        }
    }
    
    func createDataRequest(with method: HTTPMethod, params: Parameters, api: API, baseURL: BaseURLRole, requestType: RequestType = .form, includeHeader: Bool = true) -> URLRequest {
        let urlString = api.url(for: baseURL)
        
        var apiReq = URL(string: urlString)!
        GlobalFunction.printRes(data: "API - Base URL :- \(apiReq.absoluteString)")
        
        var request = URLRequest(url: apiReq)
        request.httpMethod = method.rawValue
        if userInfo != nil && api != .otp {
            request.setValue("bearer \(userInfo.token ?? "")", forHTTPHeaderField: "Authorization")
        }
        
        let postString = self.getPostString(params: params)
        if method == .get {
            let reqURL = urlString + "?" + postString
            apiReq = URL(string: reqURL)!
            request.url = apiReq
        } else {
            if requestType == .form {
                request.httpBody = postString.data(using: .utf8)
//                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//                request.setValue("application/json", forHTTPHeaderField: "Accept")
            } else {
                if let rawData = self.getRawParams(from: params) {
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    self.getRawParamsDict(from: rawData)
                    request.httpBody = rawData
                }
            }
        }
        return request
    }
    func createDataRequestTwo(with method: HTTPMethod, params: Parameters, api: API, baseURL: BaseURLRole, requestType: RequestType = .raw, includeHeader: Bool = true ,setToken : String) -> URLRequest {
        let urlString = api.url(for: baseURL)
        
        var apiReq = URL(string: urlString)!
        GlobalFunction.printRes(data: "API - Base URL :- \(apiReq.absoluteString)")
        
        var request = URLRequest(url: apiReq)
        request.httpMethod = method.rawValue

        let postString = self.getPostString(params: params)
        if method == .get {
//            let reqURL = urlString + "?" + postString
//            apiReq = URL(string: reqURL)!
//            request.url = apiReq
            if let rawData = self.getRawParams(from: params) {
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("application/json", forHTTPHeaderField: "Accept")
                request.setValue("\(setToken)", forHTTPHeaderField: "Authorization")
                print(setToken)
                self.getRawParamsDict(from: rawData)
                request.httpBody = rawData
            }
        } else {
            if requestType == .form {
                request.httpBody = postString.data(using: .utf8)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("application/json", forHTTPHeaderField: "Accept")
                request.setValue("\(setToken)", forHTTPHeaderField: "Authorization")
            } else {
                if let rawData = self.getRawParams(from: params) {
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.setValue("application/json", forHTTPHeaderField: "Accept")
                    request.setValue("\(setToken)", forHTTPHeaderField: "Authorization")
                    print(setToken)
                    self.getRawParamsDict(from: rawData)
                    request.httpBody = rawData
                }
            }
        }
        return request
    }
    func makeMultipartURLRequest(with method: HTTPMethod = .post, params: Parameters, mediaImage: [Media], api: API, baseURL: BaseURLRole, complition: @escaping (_ response: Response) -> Void) {
        guard let apiReq = URL(string: api.url(for: baseURL)) else {
            return
        }
        
        GlobalFunction.printRes(data: "API - Base URL :- \(apiReq.absoluteString)")
        
        let boundary = "Boundary-\(UUID().uuidString)"

        var request = URLRequest(url: apiReq)
        request.httpMethod = method.rawValue
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        if userInfo != nil {
            request.setValue("bearer \(userInfo.token ?? "")", forHTTPHeaderField: "Authorization")
        }
        
        let httpBody = NSMutableData()
        
        for (key, value) in params {
            httpBody.appendString(convertFormField(named: key.rawValue, value: "\(value)", using: boundary))
        }

        for media in mediaImage {
            httpBody.append(convertFileData(fieldName: media.key,
                                            fileName: media.filename,
                                            mimeType: media.mimeType,
                                            fileData: media.data,
                                            using: boundary))
        }
        
        httpBody.appendString("--\(boundary)--")

        request.httpBody = httpBody as Data
        
        let config = URLSessionConfiguration.default
        
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        
        let task = session.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
            }
            let responseData = Response(with: data, response: response, error: error)
            complition(responseData)
        }
        
        task.resume()
    }
    
    private func generateBoundary() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    private func convertFormField(named name: String, value: String, using boundary: String) -> String {
        var fieldString = "--\(boundary)\r\n"
        fieldString += "Content-Disposition: form-data; name=\"\(name)\"\r\n"
        fieldString += "\r\n"
        fieldString += "\(value)\r\n"
        let value = name + ":" + value
        print(value)
        return fieldString
    }
    
    private func convertFileData(fieldName: String, fileName: String, mimeType: String, fileData: Data, using boundary: String) -> Data {
        let data = NSMutableData()
        
        data.appendString("--\(boundary)\r\n")
        data.appendString("Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(fileName)\"\r\n")
        data.appendString("Content-Type: \(mimeType)\r\n\r\n")
        data.append(fileData)
        data.appendString("\r\n")
        
        return data as Data
    }
}

extension NSMutableData {
  func appendString(_ string: String) {
    if let data = string.data(using: .utf8) {
      self.append(data)
    }
  }
}


extension URLSession {
    fileprivate func codableTask<T: Codable>(with request: URLRequest, shouldShowMessage: Bool = true, completionHandler: @escaping (T?) -> Void) {
        if APIManager.shared.task != nil {
            if APIManager.shared.task?.originalRequest?.url == request.url {
                APIManager.shared.task?.cancel()
            }
        }
        
        APIManager.shared.task = self.dataTask(with: request) { (data, response, error) in
            let response = Response(with: data, response: response, error: error)
            if response.success {
                DispatchQueue.main.async {
                }
                guard let data = data, error == nil else {
                    completionHandler(nil)
                    return
                }
                do {
                    let model = try JSONDecoder().decode(T.self, from: data)
                    completionHandler(model)
                } catch {
                    completionHandler(nil)
                }
            } else {
                DispatchQueue.main.async {
                    if shouldShowMessage {
                        Constant.window.makeToast(response.message)
                    }
                }
                completionHandler(nil)
            }
        }
        
        APIManager.shared.task?.resume()
    }
    
    fileprivate func codableResponseTask<T: Codable>(with request: URLRequest, completionHandler: @escaping (T?, Response) -> Void) {
        if APIManager.shared.task != nil {
            if APIManager.shared.task?.originalRequest?.url == request.url {
                APIManager.shared.task?.cancel()
            }
        }
        
        APIManager.shared.task = self.dataTask(with: request) { (data, response, error) in
            let response = Response(with: data, response: response, error: error)
            if response.success {
                DispatchQueue.main.async {
                }
                guard let data = data, error == nil else {
                    completionHandler(nil, response)
                    return
                }
                do {
                    let model = try JSONDecoder().decode(T.self, from: data)
                    completionHandler(model, response)
                } catch {
                    completionHandler(nil, response)
                }
            } else {
                DispatchQueue.main.async {
                    Constant.window.makeToast(response.message)
                }
                completionHandler(nil, response)
            }
        }
        
        APIManager.shared.task?.resume()
    }
    
    func normalTask(with request: URLRequest, completionHandler: @escaping (Response) -> Void) {
        if APIManager.shared.task != nil {
            if APIManager.shared.task?.originalRequest?.url == request.url {
                APIManager.shared.task?.cancel()
            }
        }
        
        APIManager.shared.task = self.dataTask(with: request) { (data, response, error) in
            let response = Response(with: data, response: response, error: error)
            if response.success {
                DispatchQueue.main.async {
                }
                completionHandler(response)
            } else {
                DispatchQueue.main.async {
                }
                completionHandler(response)
            }
        }
        
        APIManager.shared.task?.resume()
    }
    func userLogin(with request: URLRequest, result: @escaping (Bool , String , UserData?) -> Void){
        self.normalTask(with: request) { (response) in
            let modal = User(fromDictionary: response.dicResponse)
            if response.success {
                let loginData = response.dicResponse
                if let userData = loginData["data"] {
                    userInfo = UserData(fromDictionary: userData as! [String : Any])
                    UserInfoManager.setUserInfo(userInfoModel: userInfo)
                    result(true , response.message, modal.data)
                } else {
                    result(false , response.message, modal.data)
                }
            } else {
                result(false , response.message, modal.data)
            }
        }
    }
    
    func makeApiCall(with request: URLRequest, result: @escaping (Bool, String , [String : Any]) -> Void){
        self.normalTask(with: request) { (response) in
           
            if response.success {
                result(true, response.message, response.dicResponse)
                
            }else{
                result(false, response.message, response.dicResponse)
            }
        }
   }
}

extension APIManager: URLSessionDataDelegate {
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        guard let delegate = delegate else {
            return
        }
        delegate.progress(totalBytesExpectedToSend: totalBytesExpectedToSend, sentFraction: bytesSent, totalSentFraction: totalBytesSent)
    }
    
}
