//
//  Media.swift
//  Media
//
//  Created by Vikram Chaudhary on 08/04/20.
//  Copyright © 2020 CMExpertise Infotech PVT LTD. All rights reserved.
//

import UIKit

struct Media {
    let key: String
    let filename: String
    let data: Data
    let mimeType: String
    
    init?(withImage image: UIImage, mimeType:String, doc: String, videoURL: URL?, forKey key: String) {
        self.key = key
        if videoURL != nil {
            self.mimeType = mimeType
            self.filename = "\(Date().currentTimeMillis()).mp4"
            let data = try? Data(contentsOf: videoURL!)
            guard let data1 = data else { return nil }
            self.data = data1
        } else if image.size.width > 0 {
            self.mimeType = mimeType
            self.filename = "\(Date().currentTimeMillis()).jpg"
            guard let data = image.jpegData(compressionQuality: 1.0) else { return nil }
            self.data = data
        } else {
            // "application/pdf"
            self.mimeType = mimeType
            self.filename = "\(Date().currentTimeMillis()).pdf"
            let data = try? Data(contentsOf: URL(string: doc)!)
            guard let data1 = data else { return nil }
            self.data = data1
        }
    }
}

extension Date {
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
