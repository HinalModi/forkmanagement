//
//  DatePickerVC.swift
//  DatePickerVC
//
//  Created by Vikram Chaudhary on 08/04/20.
//  Copyright © 2020 CMExpertise Infotech PVT LTD. All rights reserved.
//

import UIKit

class DatePickerVC: UIViewController {

    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var datepicker: UIDatePicker!
    
    var dateBlock: ((Date)->())?
    var minDate: Date?
    var maxDate: Date?
    var selectTime: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datepicker.minimumDate = minDate
        datepicker.maximumDate = maxDate
        datepicker.datePickerMode = selectTime ? .time : .date
    }
    
    @IBAction func cacnelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        self.dateBlock?(datepicker.date)
        self.dismiss(animated: true, completion: nil)
    }
}

class YearPickerVC: UIViewController {

    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var datepicker: UIPickerView!
    
    var dateBlock: ((String)->())?
    var year: String = ""
    var years: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let df = DateFormatter()
        df.dateFormat = "yyyy"
        let intDate = Int(df.string(from: Date())) ?? 2021
        for year in 2000...intDate {
            years.append("\(year)")
        }
        years = years.sorted{$0 > $1}
    }
    
    @IBAction func cacnelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        dateBlock?(year)
        self.dismiss(animated: true, completion: nil)
    }
}

extension YearPickerVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        years.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        years[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        year = years[row]
    }
    
}

