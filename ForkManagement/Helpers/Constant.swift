//
//  Constant.swift
//  Constant
//
//  Created by Vikram Chaudhary on 25/03/20.
//  Copyright © 2020 CMExpertise Infotech PVT LTD. All rights reserved.
//

import UIKit
import CoreLocation


var userInfo: UserData!
var imageBase: String = ""
var contactNumber: String = ""
var contactEmail: String = ""
var privacyURL: String = ""
var accountDetail: [String: Any] = [:]
var isSearchClicked:Bool = false
var totalEarnings = 0.0
var profileImageURl = ""
var profilePercent = 20


class Constant: NSObject {
    
    //Social Media
    static let clientID = ""
    static let AgoraAPPID = ""
    
    static let appName = "Seva Shop"
    
    static var userLocation = CLLocation()
    
    static var APPDELEGATE: AppDelegate!
    
    static var deviceID = UIDevice.current.identifierForVendor?.uuidString ?? "1234567890"
    static var deviceToken = "1234567890"
    
    static var version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    
    struct Screen {
        static let Width         = UIScreen.main.bounds.size.width
        static let Height        = UIScreen.main.bounds.size.height
        static let Max_Length    = max(Width, Height)
        static let Min_Length    = min(Width, Height)
        
        static let IS_IPHONE                                       = UIDevice.current.userInterfaceIdiom == .phone
        static let IS_IPAD                                         = UIDevice.current.userInterfaceIdiom == .pad
        static let g_IS_TV                                         = UIDevice.current.userInterfaceIdiom == .tv
        static let g_IS_CAR_PLAY                                   = UIDevice.current.userInterfaceIdiom == .carPlay
        
        static let g_IS_Notch_Available                            = IS_IPHONE && Max_Length > 736
        
        static let g_IS_4_SCREEN                            = Max_Length == 480.0
        
        static let g_IS_5_SCREEN                            = Max_Length == 568.0
        
        static let g_IS_6_SCREEN                            = Max_Length == 667.0
        static let g_IS_6s_SCREEN                           = Max_Length == 667.0
        static let g_IS_7_SCREEN                            = Max_Length == 667.0
        static let g_IS_8_SCREEN                            = Max_Length == 667.0
        
        static let g_IS_6Plus_SCREEN                        = Max_Length == 736.0
        static let g_IS_6sPlus_SCREEN                       = Max_Length == 736.0
        static let g_IS_7Plus_SCREEN                        = Max_Length == 736.0
        static let g_IS_8Plus_SCREEN                        = Max_Length == 736.0
        
        static let g_IS_X_SCREEN                            = Max_Length == 812.0
        static let g_IS_XS_SCREEN                           = Max_Length == 812.0
        static let g_IS_11_PRO_SCREEN                       = Max_Length == 812.0
        
        static let g_IS_XR_SCREEN                           = Max_Length == 896.0
        static let g_IS_XS_MAX_SCREEN                       = Max_Length == 896.0
        static let g_IS_11_SCREEN                           = Max_Length == 896.0
        static let g_IS_11_PRO_MAX_SCREEN                   = Max_Length == 896.0
        
        static let g_IS_IPAD_9_7                            = Max_Length == 1024.0
        static let g_IS_IPAD_GEN_7                          = Max_Length == 1080.0
        static let g_IS_IPAD_10_5                           = Max_Length == 1112.0
        static let g_IS_IPAD_11                             = Max_Length == 1194.0
        static let g_IS_IPAD_PRO_12_9                       = Max_Length == 1366.0
        
    }
    
    static var window: UIWindow {
        return APPDELEGATE.window!
    }
    
    struct GradiantColors {
        static let whiteClean: [CGColor] = [UIColor.white.cgColor, UIColor(named: "background")!.cgColor]
        
    }
    
    struct StoryBoard {
        static let main = UIStoryboard(name: "Main", bundle: nil)
        static let datePicker = UIStoryboard(name: "DatePicker", bundle: nil)
        static let products = UIStoryboard(name: "MyProducts", bundle: nil)
        
    }
    
    struct SegueId {
        static let toLoginVC = "toLoginVC"
        static let toRegisterVC = "toRegisterVC"
        static let toOTPVC = "toOTPVC"
        static let toChangePassword = "toChangePassword"
        static let toHomeVC = "toHomeVC"
        static let toContactDetails = "toContactDetails"
        static let toSupport = "toSupport"
        static let toTNC = "toTNC"
        static let toPaymentHistory = "toPaymentHistory"
        static let toFAQ = "toFAQ"
        static let toInvitationDetailVC = "toInvitationDetailVC"
        static let toResetPassword = "toResetPassword"
        static let toSuccessRegisterVC = "toSuccessRegisterVC"
        static let toBankAccountVC = "toBankAccountVC"
        static let toAadharCardDetailsVC = "toAadharCardDetailsVC"
        static let toPanCardDetailsVC = "toPanCardDetailsVC"
        static let toTutorialVC = "toTutorialVC"
        static let toCompleteProfile = "toCompleteProfile"
        static let toLMSVC = "toLMSVC"
        
        static let toEditProfileVC = "toEditProfileVC"
        static let toInnovationDetailVC = "toInnovationDetail"
        
    }
    
    struct NotificationName {
        static let updateSideMenuInfo = Notification.init(name: Notification.Name("updateSideMenuInfo"))
        static let updateOrders = Notification.init(name: Notification.Name("updateOrders"))
        static let openChatOnNotification = Notification.init(name: Notification.Name("openChatOnNotification"))
        static let reloadTab = Notification.init(name: Notification.Name("reloadTab"))

    }
    
    struct Color {
        static let theme: UIColor = UIColor(named: "Orange")!
        static let border: UIColor = UIColor(named: "Border")!
        static let dark: UIColor = UIColor(named: "Dark")!
        static let gray: UIColor = UIColor(named: "Gray")!
        static let light: UIColor = UIColor(named: "Light")!
        static let backGray: UIColor = UIColor(named: "Back Gray")!
    }
}

