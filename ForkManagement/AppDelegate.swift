//
//  AppDelegate.swift
//  ForkManagement
//
//  Created by Ankit Dave on 11/11/22.
//

import UIKit
import LGSideMenuController
import FirebaseCore
import GoogleSignIn
import FBSDKLoginKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var DELEGATE : AppDelegate!
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        Constant.APPDELEGATE = self
        VCLangauageManager.sharedInstance.changeAppLanguage(To: VCLangauageManager.sharedInstance.isLanguageArabic() ? .arabic : .english)
        userInfo = UserInfoManager.getUserInfoModel()
        GIDSignIn.sharedInstance()?.clientID = "42002515698-d0equ0de69caofkcafs10of2vn8vc56b.apps.googleusercontent.com"
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        window?.makeKeyAndVisible()
        setRootVC()
        if #available(iOS 13.0, *) {
            IQKeyboardManager.shared.toolbarTintColor = .link
        }
        return true
    }
    func showSideMenu(viewController : UIViewController){
        let leftView = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LeftSideMenuVC") as! LeftSideMenuVC
        let rightView = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "RightSideMenuVC") as! RightSideMenuVC

        let nav = UINavigationController(rootViewController: viewController)
        nav.navigationBar.isHidden = true
        let lgMenu = LGSideMenuController(rootViewController: nav, leftViewController: leftView, rightViewController: rightView)
        lgMenu.isRightViewSwipeGestureEnabled = false
        lgMenu.isLeftViewSwipeGestureEnabled = false
        lgMenu.leftViewWidth = UIScreen.main.bounds.width / 1.4
        lgMenu.rightViewWidth = UIScreen.main.bounds.width / 1.4
        lgMenu.leftViewPresentationStyle = .slideAbove
        lgMenu.rightViewPresentationStyle = .slideAbove
        lgMenu.rootViewCoverBlurEffect = UIBlurEffect(style: .dark)
        if #available(iOS 11.0, *) {
            lgMenu.rootViewCoverColor = UIColor(named: "blackTop")?.withAlphaComponent(0.75)
        }
        Constant.APPDELEGATE.window?.rootViewController = lgMenu
    }
    func setRootVC(){
        if (userInfo != nil){
            let vc = UIStoryboard(name: "Guide", bundle: nil).instantiateViewController(withIdentifier: "GuideMenuVC") as! GuideMenuVC
            let nav = UINavigationController(rootViewController: vc)
            showSideMenu(viewController: vc)
            nav.navigationBar.isHidden = true
        }else{
            let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "IntroVC") as! IntroVC
            let nav = UINavigationController(rootViewController: vc)
            showSideMenu(viewController: vc)
            nav.navigationBar.isHidden = true
        }
    }
    func setInitialViewController() {
        let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LangVC") as! LangVC
        let nav = UINavigationController(rootViewController: vc)
        showSideMenu(viewController: vc)
        nav.navigationBar.isHidden = true
        resetDefaults()
    }
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let fb = ApplicationDelegate.shared.application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        let google = (GIDSignIn.sharedInstance()?.handle(url))
        return fb || (google != nil)
    }
   
}

