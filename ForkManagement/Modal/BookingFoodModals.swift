//
//  BookingFoodModals.swift
//  ForkManagement
//
//  Created by Ankit Dave on 02/01/23.
//

import Foundation
struct FoodList{
    internal init(name: String, price: Int, image: String, id: Int, category_id: Int) {
        self.name = name
        self.price = price
        self.image = image
        self.id = id
        self.category_id = category_id
    }
    var name : String
    var price : Int
    var image : String
    var id : Int
    var category_id : Int
}
struct FoodCategory{
     init(name: String, id: Int, branch_id: String, slug: String, status_id: Int , defaultVal : Int) {
         self.name = name
         self.id = id
         self.branch_id = branch_id
         self.slug = slug
         self.status_id = status_id
         self.defaultVal = defaultVal
    }
    var name : String
    var id : Int
    var branch_id : String
    var slug : String
    var status_id : Int
    var defaultVal : Int
}
struct ItemList {
     init(id: Int, item_Id: Int, name: String, price: String, description: String, status_id: Int, image: String) {
        self.id = id
        self.item_Id = item_Id
        self.name = name
        self.price = price
        self.description = description
        self.status_id = status_id
        self.image = image
    }
    let id : Int
    let item_Id : Int
    let name : String
    let price : String
    let description : String
    let status_id : Int
    let image : String
}
struct Cart_item{
    internal init(cart_item_id: Int, category_id: Int, cart_item_name: String, cart_item_price: Int, cart_item_image: String, extra_item_details: [[String : Any]],qty : Int,cart_Id : Int) {
        self.cart_item_id = cart_item_id
        self.category_id = category_id
        self.cart_item_name = cart_item_name
        self.cart_item_price = cart_item_price
        self.cart_item_image = cart_item_image
        self.extra_item_details = extra_item_details
        self.qty = qty
        self.cart_Id = cart_Id
    }
    
    let cart_item_id : Int
    let category_id : Int
    let cart_item_name : String
    let cart_item_price : Int
    let cart_item_image : String
    let extra_item_details : [[String : Any]]
    let qty : Int
    let cart_Id : Int
}
