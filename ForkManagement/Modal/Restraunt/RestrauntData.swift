//
//	RestrauntData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class RestrauntData {

	var address : String!
	var block : String!
	var city : String!
	var contact : String!
	var countryId : String!
	var id : Int!
	var noOfStaff : String!
	var parentId : String!
	var pincode : String!
	var restBranch : String!
	var restName : String!
	var services : [RestrauntService]!
	var shopNumber : String!
	var stateId : String!
	var statusId : Int!
	var street : String!
	var currentPage : Int!
	var data : [RestrauntData]!
	var firstPageUrl : String!
	var from : Int!
	var lastPage : Int!
	var lastPageUrl : String!
	var links : [RestrauntLink]!
	var nextPageUrl : AnyObject!
	var path : String!
	var perPage : Int!
	var prevPageUrl : AnyObject!
	var to : Int!
	var total : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		address = dictionary["address"] as? String
		block = dictionary["block"] as? String
		city = dictionary["city"] as? String
		contact = dictionary["contact"] as? String
		countryId = dictionary["country_id"] as? String
		id = dictionary["id"] as? Int
		noOfStaff = dictionary["no_of_staff"] as? String
		parentId = dictionary["parent_id"] as? String
		pincode = dictionary["pincode"] as? String
		restBranch = dictionary["rest_branch"] as? String
		restName = dictionary["rest_name"] as? String
		services = [RestrauntService]()
		if let servicesArray = dictionary["services"] as? [[String:Any]]{
			for dic in servicesArray{
				let value = RestrauntService(fromDictionary: dic)
				services.append(value)
			}
		}
		shopNumber = dictionary["shop_number"] as? String
		stateId = dictionary["state_id"] as? String
		statusId = dictionary["status_id"] as? Int
		street = dictionary["street"] as? String
		currentPage = dictionary["current_page"] as? Int
		data = [RestrauntData]()
		if let dataArray = dictionary["data"] as? [[String:Any]]{
			for dic in dataArray{
				let value = RestrauntData(fromDictionary: dic)
				data.append(value)
			}
		}
		firstPageUrl = dictionary["first_page_url"] as? String
		from = dictionary["from"] as? Int
		lastPage = dictionary["last_page"] as? Int
		lastPageUrl = dictionary["last_page_url"] as? String
		links = [RestrauntLink]()
		if let linksArray = dictionary["links"] as? [[String:Any]]{
			for dic in linksArray{
				let value = RestrauntLink(fromDictionary: dic)
				links.append(value)
			}
		}
		nextPageUrl = dictionary["next_page_url"] as? AnyObject
		path = dictionary["path"] as? String
		perPage = dictionary["per_page"] as? Int
		prevPageUrl = dictionary["prev_page_url"] as? AnyObject
		to = dictionary["to"] as? Int
		total = dictionary["total"] as? Int
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if address != nil{
			dictionary["address"] = address
		}
		if block != nil{
			dictionary["block"] = block
		}
		if city != nil{
			dictionary["city"] = city
		}
		if contact != nil{
			dictionary["contact"] = contact
		}
		if countryId != nil{
			dictionary["country_id"] = countryId
		}
		if id != nil{
			dictionary["id"] = id
		}
		if noOfStaff != nil{
			dictionary["no_of_staff"] = noOfStaff
		}
		if parentId != nil{
			dictionary["parent_id"] = parentId
		}
		if pincode != nil{
			dictionary["pincode"] = pincode
		}
		if restBranch != nil{
			dictionary["rest_branch"] = restBranch
		}
		if restName != nil{
			dictionary["rest_name"] = restName
		}
		if services != nil{
			var dictionaryElements = [[String:Any]]()
			for servicesElement in services {
				dictionaryElements.append(servicesElement.toDictionary())
			}
			dictionary["services"] = dictionaryElements
		}
		if shopNumber != nil{
			dictionary["shop_number"] = shopNumber
		}
		if stateId != nil{
			dictionary["state_id"] = stateId
		}
		if statusId != nil{
			dictionary["status_id"] = statusId
		}
		if street != nil{
			dictionary["street"] = street
		}
		if currentPage != nil{
			dictionary["current_page"] = currentPage
		}
		if data != nil{
			var dictionaryElements = [[String:Any]]()
			for dataElement in data {
				dictionaryElements.append(dataElement.toDictionary())
			}
			dictionary["data"] = dictionaryElements
		}
		if firstPageUrl != nil{
			dictionary["first_page_url"] = firstPageUrl
		}
		if from != nil{
			dictionary["from"] = from
		}
		if lastPage != nil{
			dictionary["last_page"] = lastPage
		}
		if lastPageUrl != nil{
			dictionary["last_page_url"] = lastPageUrl
		}
		if links != nil{
			var dictionaryElements = [[String:Any]]()
			for linksElement in links {
				dictionaryElements.append(linksElement.toDictionary())
			}
			dictionary["links"] = dictionaryElements
		}
		if nextPageUrl != nil{
			dictionary["next_page_url"] = nextPageUrl
		}
		if path != nil{
			dictionary["path"] = path
		}
		if perPage != nil{
			dictionary["per_page"] = perPage
		}
		if prevPageUrl != nil{
			dictionary["prev_page_url"] = prevPageUrl
		}
		if to != nil{
			dictionary["to"] = to
		}
		if total != nil{
			dictionary["total"] = total
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         block = aDecoder.decodeObject(forKey: "block") as? String
         city = aDecoder.decodeObject(forKey: "city") as? String
         contact = aDecoder.decodeObject(forKey: "contact") as? String
         countryId = aDecoder.decodeObject(forKey: "country_id") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         noOfStaff = aDecoder.decodeObject(forKey: "no_of_staff") as? String
         parentId = aDecoder.decodeObject(forKey: "parent_id") as? String
         pincode = aDecoder.decodeObject(forKey: "pincode") as? String
         restBranch = aDecoder.decodeObject(forKey: "rest_branch") as? String
         restName = aDecoder.decodeObject(forKey: "rest_name") as? String
         services = aDecoder.decodeObject(forKey :"services") as? [RestrauntService]
         shopNumber = aDecoder.decodeObject(forKey: "shop_number") as? String
         stateId = aDecoder.decodeObject(forKey: "state_id") as? String
         statusId = aDecoder.decodeObject(forKey: "status_id") as? Int
         street = aDecoder.decodeObject(forKey: "street") as? String
         currentPage = aDecoder.decodeObject(forKey: "current_page") as? Int
         data = aDecoder.decodeObject(forKey :"data") as? [RestrauntData]
         firstPageUrl = aDecoder.decodeObject(forKey: "first_page_url") as? String
         from = aDecoder.decodeObject(forKey: "from") as? Int
         lastPage = aDecoder.decodeObject(forKey: "last_page") as? Int
         lastPageUrl = aDecoder.decodeObject(forKey: "last_page_url") as? String
         links = aDecoder.decodeObject(forKey :"links") as? [RestrauntLink]
         nextPageUrl = aDecoder.decodeObject(forKey: "next_page_url") as? AnyObject
         path = aDecoder.decodeObject(forKey: "path") as? String
         perPage = aDecoder.decodeObject(forKey: "per_page") as? Int
         prevPageUrl = aDecoder.decodeObject(forKey: "prev_page_url") as? AnyObject
         to = aDecoder.decodeObject(forKey: "to") as? Int
         total = aDecoder.decodeObject(forKey: "total") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if block != nil{
			aCoder.encode(block, forKey: "block")
		}
		if city != nil{
			aCoder.encode(city, forKey: "city")
		}
		if contact != nil{
			aCoder.encode(contact, forKey: "contact")
		}
		if countryId != nil{
			aCoder.encode(countryId, forKey: "country_id")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if noOfStaff != nil{
			aCoder.encode(noOfStaff, forKey: "no_of_staff")
		}
		if parentId != nil{
			aCoder.encode(parentId, forKey: "parent_id")
		}
		if pincode != nil{
			aCoder.encode(pincode, forKey: "pincode")
		}
		if restBranch != nil{
			aCoder.encode(restBranch, forKey: "rest_branch")
		}
		if restName != nil{
			aCoder.encode(restName, forKey: "rest_name")
		}
		if services != nil{
			aCoder.encode(services, forKey: "services")
		}
		if shopNumber != nil{
			aCoder.encode(shopNumber, forKey: "shop_number")
		}
		if stateId != nil{
			aCoder.encode(stateId, forKey: "state_id")
		}
		if statusId != nil{
			aCoder.encode(statusId, forKey: "status_id")
		}
		if street != nil{
			aCoder.encode(street, forKey: "street")
		}
		if currentPage != nil{
			aCoder.encode(currentPage, forKey: "current_page")
		}
		if data != nil{
			aCoder.encode(data, forKey: "data")
		}
		if firstPageUrl != nil{
			aCoder.encode(firstPageUrl, forKey: "first_page_url")
		}
		if from != nil{
			aCoder.encode(from, forKey: "from")
		}
		if lastPage != nil{
			aCoder.encode(lastPage, forKey: "last_page")
		}
		if lastPageUrl != nil{
			aCoder.encode(lastPageUrl, forKey: "last_page_url")
		}
		if links != nil{
			aCoder.encode(links, forKey: "links")
		}
		if nextPageUrl != nil{
			aCoder.encode(nextPageUrl, forKey: "next_page_url")
		}
		if path != nil{
			aCoder.encode(path, forKey: "path")
		}
		if perPage != nil{
			aCoder.encode(perPage, forKey: "per_page")
		}
		if prevPageUrl != nil{
			aCoder.encode(prevPageUrl, forKey: "prev_page_url")
		}
		if to != nil{
			aCoder.encode(to, forKey: "to")
		}
		if total != nil{
			aCoder.encode(total, forKey: "total")
		}

	}

}
