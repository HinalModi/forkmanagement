//
//	RestrauntLink.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class RestrauntLink : NSObject, NSCoding{

	var active : Bool!
	var label : String!
	var url : AnyObject!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		active = dictionary["active"] as? Bool
		label = dictionary["label"] as? String
		url = dictionary["url"] as? AnyObject
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if active != nil{
			dictionary["active"] = active
		}
		if label != nil{
			dictionary["label"] = label
		}
		if url != nil{
			dictionary["url"] = url
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         active = aDecoder.decodeObject(forKey: "active") as? Bool
         label = aDecoder.decodeObject(forKey: "label") as? String
         url = aDecoder.decodeObject(forKey: "url") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if active != nil{
			aCoder.encode(active, forKey: "active")
		}
		if label != nil{
			aCoder.encode(label, forKey: "label")
		}
		if url != nil{
			aCoder.encode(url, forKey: "url")
		}

	}

}