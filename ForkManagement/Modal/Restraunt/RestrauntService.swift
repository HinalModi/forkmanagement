//
//	RestrauntService.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class RestrauntService : NSObject, NSCoding{

	var id : Int!
	var restaurantId : String!
	var serviceId : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		id = dictionary["id"] as? Int
		restaurantId = dictionary["restaurant_id"] as? String
		serviceId = dictionary["service_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if id != nil{
			dictionary["id"] = id
		}
		if restaurantId != nil{
			dictionary["restaurant_id"] = restaurantId
		}
		if serviceId != nil{
			dictionary["service_id"] = serviceId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? Int
         restaurantId = aDecoder.decodeObject(forKey: "restaurant_id") as? String
         serviceId = aDecoder.decodeObject(forKey: "service_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if restaurantId != nil{
			aCoder.encode(restaurantId, forKey: "restaurant_id")
		}
		if serviceId != nil{
			aCoder.encode(serviceId, forKey: "service_id")
		}

	}

}