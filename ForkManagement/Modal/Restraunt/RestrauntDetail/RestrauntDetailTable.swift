//
//	RestrauntDetailTable.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class RestrauntDetailTable : NSObject, NSCoding{

	var areaId : Int!
	var createdAt : String!
	var deletedAt : AnyObject!
	var describtion : AnyObject!
	var dressCode : AnyObject!
	var floorId : Int!
	var id : Int!
	var numberOfPerson : Int!
	var restaurantId : Int!
	var statusId : Int!
	var tableNo : Int!
	var type : String!
	var updatedAt : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		areaId = dictionary["area_id"] as? Int
		createdAt = dictionary["created_at"] as? String
		deletedAt = dictionary["deleted_at"] as? AnyObject
		describtion = dictionary["describtion"] as? AnyObject
		dressCode = dictionary["dress_code"] as? AnyObject
		floorId = dictionary["floor_id"] as? Int
		id = dictionary["id"] as? Int
		numberOfPerson = dictionary["number_of_person"] as? Int
		restaurantId = dictionary["restaurant_id"] as? Int
		statusId = dictionary["status_id"] as? Int
		tableNo = dictionary["table_no"] as? Int
		type = dictionary["type"] as? String
		updatedAt = dictionary["updated_at"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if areaId != nil{
			dictionary["area_id"] = areaId
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if deletedAt != nil{
			dictionary["deleted_at"] = deletedAt
		}
		if describtion != nil{
			dictionary["describtion"] = describtion
		}
		if dressCode != nil{
			dictionary["dress_code"] = dressCode
		}
		if floorId != nil{
			dictionary["floor_id"] = floorId
		}
		if id != nil{
			dictionary["id"] = id
		}
		if numberOfPerson != nil{
			dictionary["number_of_person"] = numberOfPerson
		}
		if restaurantId != nil{
			dictionary["restaurant_id"] = restaurantId
		}
		if statusId != nil{
			dictionary["status_id"] = statusId
		}
		if tableNo != nil{
			dictionary["table_no"] = tableNo
		}
		if type != nil{
			dictionary["type"] = type
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         areaId = aDecoder.decodeObject(forKey: "area_id") as? Int
         createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
         deletedAt = aDecoder.decodeObject(forKey: "deleted_at") as? AnyObject
         describtion = aDecoder.decodeObject(forKey: "describtion") as? AnyObject
         dressCode = aDecoder.decodeObject(forKey: "dress_code") as? AnyObject
         floorId = aDecoder.decodeObject(forKey: "floor_id") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? Int
         numberOfPerson = aDecoder.decodeObject(forKey: "number_of_person") as? Int
         restaurantId = aDecoder.decodeObject(forKey: "restaurant_id") as? Int
         statusId = aDecoder.decodeObject(forKey: "status_id") as? Int
         tableNo = aDecoder.decodeObject(forKey: "table_no") as? Int
         type = aDecoder.decodeObject(forKey: "type") as? String
         updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if areaId != nil{
			aCoder.encode(areaId, forKey: "area_id")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if deletedAt != nil{
			aCoder.encode(deletedAt, forKey: "deleted_at")
		}
		if describtion != nil{
			aCoder.encode(describtion, forKey: "describtion")
		}
		if dressCode != nil{
			aCoder.encode(dressCode, forKey: "dress_code")
		}
		if floorId != nil{
			aCoder.encode(floorId, forKey: "floor_id")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if numberOfPerson != nil{
			aCoder.encode(numberOfPerson, forKey: "number_of_person")
		}
		if restaurantId != nil{
			aCoder.encode(restaurantId, forKey: "restaurant_id")
		}
		if statusId != nil{
			aCoder.encode(statusId, forKey: "status_id")
		}
		if tableNo != nil{
			aCoder.encode(tableNo, forKey: "table_no")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}

	}

}