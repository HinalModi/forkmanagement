//
//  OrderModal.swift
//  ForkManagement
//
//  Created by Ankit Dave on 04/01/23.
//

import Foundation
struct OrderList{
     init(orderid: Int,review : [String : Any] ,userId: Int, totalOrder: String, payment_status: String, order_status: String, timing: [String : Any], restaurant: [String : Any] ,opendays : String , orderDate : String) {
         self.orderid = orderid
         self.userId = userId
         self.totalOrder = totalOrder
         self.payment_status = payment_status
         self.order_status = order_status
         self.timing = timing
         self.restaurant = restaurant
         self.opendays = opendays
         self.review = review
         self.orderDate = orderDate
    }
    var orderid : Int
    var userId : Int
    var totalOrder : String
    var payment_status : String
    var order_status : String
    var timing : [String : Any]
    var restaurant : [String : Any]
    var opendays : String
    var review : [String : Any]
    var orderDate : String
}
struct OrdeConfirm{
    init(order_item_name: String, order_item_image: String, order_item_price: Int) {
        self.order_item_name = order_item_name
        self.order_item_image = order_item_image
        self.order_item_price = order_item_price
    }
    var order_item_name : String
    var order_item_image : String
    var order_item_price : Int
}
struct Review {
     init(exp: Int, food: Int, waiter: Int, id: Int, order_id: Int) {
        self.exp = exp
        self.food = food
        self.waiter = waiter
        self.id = id
        self.order_id = order_id
    }
    
    var exp : Int
    var food : Int
    var waiter : Int
    var id : Int
    var order_id : Int
}
