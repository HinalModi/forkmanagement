//
//  LocationModal.swift
//  ForkManagement
//
//  Created by Ankit Dave on 23/01/23.
//

import Foundation
struct Location{
    init(lat: String, long: String, type: String, location: String, is_default: Int, id: Int) {
        self.lat = lat
        self.long = long
        self.type = type
        self.location = location
        self.is_default = is_default
        self.id = id
    }
    var lat : String
    var long : String
    var type : String
    var location : String
    var is_default : Int
    var id : Int
}
