//
//	ForgotPswdData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ForgotPswdData : NSObject, NSCoding{

	var contact : String!
	var token : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		contact = dictionary["contact"] as? String
		token = dictionary["token"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if contact != nil{
			dictionary["contact"] = contact
		}
		if token != nil{
			dictionary["token"] = token
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         contact = aDecoder.decodeObject(forKey: "contact") as? String
         token = aDecoder.decodeObject(forKey: "token") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if contact != nil{
			aCoder.encode(contact, forKey: "contact")
		}
		if token != nil{
			aCoder.encode(token, forKey: "token")
		}

	}

}