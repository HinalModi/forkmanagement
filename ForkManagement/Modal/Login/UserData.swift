//
//	UserData.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import CountryPickerView


class UserData : NSObject, NSCoding{
    var contact : String!
    var email : String!
    var lastLogin : String!
    var name : String!
    var token : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        contact = dictionary["contact"] as? String
        email = dictionary["email"] as? String
        lastLogin = dictionary["last_login"] as? String
        name = dictionary["name"] as? String
        token = dictionary["token"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if contact != nil{
            dictionary["contact"] = contact
        }
        if email != nil{
            dictionary["email"] = email
        }
        if lastLogin != nil{
            dictionary["last_login"] = lastLogin
        }
        if name != nil{
            dictionary["name"] = name
        }
        if token != nil{
            dictionary["token"] = token
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        contact = aDecoder.decodeObject(forKey: "contact") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        lastLogin = aDecoder.decodeObject(forKey: "last_login") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        token = aDecoder.decodeObject(forKey: "token") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if contact != nil{
            aCoder.encode(contact, forKey: "contact")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if lastLogin != nil{
            aCoder.encode(lastLogin, forKey: "last_login")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if token != nil{
            aCoder.encode(token, forKey: "token")
        }
        
    }
    
}
class PhoneNumberLogin {
    //+973 3719 9887
    var phoneCode: String = ""
    var phoneNumber: String = ""
    var selectedCountry: Country? = nil
    var otpCode: String = ""
    var firebaseVerificationID: String = ""
    
    var formatedPhoneNumber: String {
        print((phoneCode) + phoneNumber)
        return (phoneCode) + phoneNumber
    }
    //, selectedCountry: Country
    init(phoneCode: String, phoneNumber: String, selectedCountry: Country) {
        self.phoneCode = phoneCode
        self.phoneNumber = phoneNumber
        self.selectedCountry = selectedCountry
    }
    
}
